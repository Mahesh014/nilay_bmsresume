<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>My Profile: Contribution</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/icon" href="assets/images/favicon.ico"/>
    <!-- Font Awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">    
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css"/> 
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="assets/css/jquery.fancybox.css" type="text/css" media="screen" /> 
    <!-- Animate css -->
    <link rel="stylesheet" type="text/css" href="assets/css/animate.css"/> 
    <!-- Bootstrap progressbar  --> 
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-progressbar-3.3.4.css"/> 
     <!-- Theme color -->
      <link id="switcher" href="assets/css/theme-color/lite-blue-theme.css" rel="stylesheet">


    <!-- Main Style -->
    <link href="assets/css/style.css" rel="stylesheet">

    <!-- Fonts -->

    <!-- Open Sans for body font -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- Lato for Title -->
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'> 
   
    <script type="text/javascript">
function show(e1Id)
{
for (var i=1; i<=4; i++) {
 document.getElementById('1'+i).className = 'hide';
 }
 document.getElementById('1'+e1Id).className = 'show';
}
</script>
</head>
  <body>     

  <!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

 
  <!-- End header -->
 
      
  <!-- BEGIN MENU -->
  <section id="menu-area">      
    <nav class="navbar navbar-default" role="navigation">  
            <jsp:include page="/jsp/pagelinks.jsp"></jsp:include>    
     
    </nav>
  </section>
  <!-- END MENU -->  
  
  <!-- Start single page header -->
  <section id="single-page-header">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>My Contributions</h2>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="<%=request.getContextPath()%>/home.do?action=list">Home</a></li>
                <li class="active">My Contributions</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  
  <!-- Start blog archive -->
  <section id="blog-archive">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="blog-archive-area">
            <div class="row">
              <div class="col-md-3 hidden-sm hidden-xs">
                <aside class="blog-side-bar">
                  <!-- Start sidebar widget -->
                  <div class="sidebar-widget">
                    <!-- Start blog search -->
                    
                    <!-- End blog search -->                                
                  </div>
                  
                   
                </aside>
              </div>
              <div class="col-md-12">
                <div class="blog-archive-left">
                  <!-- Start blog news single -->
                  <article class="blog-news-single">
                   <div id="11" class="show">	
                    <div class="blog-news-title">
                      <h2>Contributions as Principal, BMSCE</h2>
                      
                    </div>
                    <div class="blog-news-details blog-single-details" style="min-height:300px">
                      

                    <!--<ul class="sports">
                        
                        <li>Introduced innovative educational practices thereby creating an effective learning environment
                        <ul class="contribution">
                        <li>Choice Based Credit System (CBCS)</li>
                        <li>Cluster learning concept</li>
                        <li>Course reclassification into three main categories namely, Regular, Integrated and Comprehensive courses</li>
                        <li>Course based laboratories</li>
                        <li>Pedagogy comprising delivery components like Lecture, Tutorials, Practical and Self-study</li>
                        <li>Impetus on continuous learning through equal weightage to CIE & SEE</li>
                        <li>Workshops / lectures by international experts</li>
                        <li>Learning through observation and experience —Industrial / site visits</li>
                        </ul>
                    </li>
                     
                       
                      </ul>
                      --><logic:notEmpty name="roleList">
            <logic:iterate id="userrole" name="roleList" indexId="userid">
            	<ul class="sports">
            	<li><bean:write name="userrole" property="title" />
	            	<ul class="contribution">
		            	<bean:define id="abc" name="userrole" property="contributionasprincipal" type="java.lang.String"/>   
	            <%
        		String[] temp;
        		String delimiter = "~@~";
        		String s = abc;
        		int counter = 0;
        		for( int i=0; i<s.length(); i++ ) {
        		    if( s.contains("~@~") ) {
        		        counter++;
        		    } 
        		}
        		 temp = abc.split(delimiter);
        		  /* print substrings */
        		  if(counter>0){
        		  for(int i =0; i < temp.length ; i++)
        		  {
        			
        		    //System.out.println("preinting***************************:"+temp.length);%>
              
          		            	<li><%=temp[i]%></li>
          
        		 	 <% }}%>
	            	</ul>
	            </li>
            	</ul>
            	

            </logic:iterate>
            </logic:notEmpty>
                      
                    </div>
                    </div><!--11-->
                    
                   
                 
              <div class="col-md-4 visible-sm visible-xs">
                <aside class="blog-side-bar">
                  <!-- Start sidebar widget -->
                 
                
                </aside>
              </div>             
            </div>
          </div>
        </div>
      </div>
    </div>  
  </section>
  <!-- End blog archive -->

 

  <!-- Start footer -->
  <footer id="footer">
    <jsp:include page="/jsp/Commanfooter.jsp"></jsp:include>
  </footer>
  <!-- End footer -->

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>    
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <!-- Bootstrap -->
  <script src="<%=request.getContextPath()%>/assets/js/bootstrap.js"></script>
  <!-- Slick Slider -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/slick.js"></script>    
  <!-- mixit slider -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->        
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/jquery.fancybox.pack.js"></script>
 <!-- counter -->
  <script src="<%=request.getContextPath()%>/assets/js/waypoints.js"></script>
  <script src="<%=request.getContextPath()%>/assets/js/jquery.counterup.js"></script>
  <!-- Wow animation -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/wow.js"></script> 
  <!-- progress bar   -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/bootstrap-progressbar.js"></script>  
  
 
  <!-- Custom js -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/custom.js"></script>
    
  </body>
</html>