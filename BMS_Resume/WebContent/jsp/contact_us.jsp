<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>My Profile: Contact</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/icon" href="<%=request.getContextPath()%>/assets/images/favicon.ico"/>
    <!-- Font Awesome -->
    <link href="<%=request.getContextPath()%>/assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="<%=request.getContextPath()%>/assets/css/bootstrap.css" rel="stylesheet">    
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/assets/css/slick.css"/> 
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/jquery.fancybox.css" type="text/css" media="screen" /> 
    <!-- Animate css -->
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/assets/css/animate.css"/> 
    <!-- Bootstrap progressbar  --> 
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/assets/css/bootstrap-progressbar-3.3.4.css"/> 
     <!-- Theme color -->
      <link id="switcher" href="<%=request.getContextPath()%>/assets/css/theme-color/lite-blue-theme.css" rel="stylesheet">


    <!-- Main Style -->
    <link href="<%=request.getContextPath()%>/assets/css/style.css" rel="stylesheet">

    <!-- Fonts -->

    <!-- Open Sans for body font -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- Lato for Title -->
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'> 
   
    <script type="text/javascript">
function show(e1Id)
{
for (var i=1; i<=4; i++) {
 document.getElementById('1'+i).className = 'hide';
 }
 document.getElementById('1'+e1Id).className = 'show';
}
</script>  

 <script type="text/javascript">
 
 function validate(){
    if ((document.contactform.fname.value=="")){
        //alert("Please Enter your name!");
        return false;
    }
     if ((document.contactform.email.value=="")){
        //alert("Please Enter your email id!");
        return false;
    }
     
    
    else
        return true;
}
 
 
 </script>



  </head>
  <body>     

  <!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

 
  <!-- End header -->
 
      
  <!-- BEGIN MENU -->
  <section id="menu-area">      
    <nav class="navbar navbar-default" role="navigation">  
            <jsp:include page="/jsp/pagelinks.jsp"></jsp:include>    
     
    </nav>
  </section>
  <!-- END MENU -->  
  
  <!-- Start single page header -->
  <section id="single-page-header">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Contact</h2>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="<%=request.getContextPath()%>/home.do?action=list">Home</a></li>
                <li class="active">Contact</li>
              </ol>
            </div>
          </div>
        </div>
      </div> 
    </div>
  </section>
  <!-- End single page header -->
  
  <!-- Start blog archive -->
  <section id="contact">
     <div class="container">
       <div class="row">
         <div class="col-md-12">
           <div class="title-area">
              <h2 class="title">Get in touch</h2>
              <span class="line"></span>
              
            </div>
         </div>
         <div class="col-md-12">
           <div class="cotact-area">
             <div class="row">
               <div class="col-md-4">
                 <div class="contact-area-left">
                   <h4>Contact Info</h4>
                   <p>Dr. K. MALLIKHARJUNA BABU, PhD</p>
                   <p>Principal, BMS College of Engineering</p>
                   <address class="single-address">
                     <p>Bull Temple Road, Bengaluru.</p>
                    
                   </address> 
                   <div class="footer-right contact-social">
                      <a href="https://www.facebook.com/drkmbabu" target="_blank"><i class="fa fa-facebook"></i></a>
    
            <!--<a href="#"><i class="fa fa-google-plus"></i></a-->
            <a href="http://in.linkedin.com/in/drkmbabu"  target="_blank"><i class="fa fa-linkedin"></i></a>
                    </div>                
                 </div>
               </div>
               <div class="col-md-8">
                 <div class="contact-area-right">
                
                   <form method="post" action="mail.jsp" class="comments-form contact-form" name="contactform" id="contactform" onsubmit="return validate()" >
                    <div class="form-group">                        
                      <input type="text" class="form-control" placeholder="Your Name" name="fname">
                    </div>
                    <div class="form-group">                        
                      <input type="email" class="form-control" placeholder="Email" name="email">
                    </div>
                     <div class="form-group">                        
                      <input type="text" class="form-control" placeholder="Subject" name="subject">
                    </div>
                    <div class="form-group">                        
                      <textarea placeholder="Message" rows="3" class="form-control" name="message"></textarea>
                    </div>
                    <button class="comment-btn">Submit</button>
                  </form>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
  </section>
  <!-- End blog archive -->

 

  <!-- Start footer -->
  <footer id="footer">
    <jsp:include page="/jsp/Commanfooter.jsp"></jsp:include>
  </footer>
  <!-- End footer -->

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>    
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <!-- Bootstrap -->
  <script src="<%=request.getContextPath()%>/assets/js/bootstrap.js"></script>
  <!-- Slick Slider -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/slick.js"></script>    
  <!-- mixit slider -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->        
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/jquery.fancybox.pack.js"></script>
 <!-- counter -->
  <script src="<%=request.getContextPath()%>/assets/js/waypoints.js"></script>
  <script src="<%=request.getContextPath()%>/assets/js/jquery.counterup.js"></script>
  <!-- Wow animation -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/wow.js"></script> 
  <!-- progress bar   -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/bootstrap-progressbar.js"></script>  
  
 
  <!-- Custom js -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/custom.js"></script>
    
  </body>
</html>