<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>My Profile: Research</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/icon" href="<%=request.getContextPath()%>/assets/images/favicon.ico"/>
    <!-- Font Awesome -->
    <link href="<%=request.getContextPath()%>/assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="<%=request.getContextPath()%>/assets/css/bootstrap.css" rel="stylesheet">    
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/assets/css/slick.css"/> 
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/jquery.fancybox.css" type="text/css" media="screen" /> 
    <!-- Animate css -->
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/assets/css/animate.css"/> 
    <!-- Bootstrap progressbar  --> 
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/assets/css/bootstrap-progressbar-3.3.4.css"/> 
     <!-- Theme color -->
      <link id="switcher" href="<%=request.getContextPath()%>/assets/css/theme-color/lite-blue-theme.css" rel="stylesheet">


    <!-- Main Style -->
    <link href="assets/css/style.css" rel="stylesheet">

    <!-- Fonts -->

    <!-- Open Sans for body font -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- Lato for Title -->
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'> 
   
    <script type="text/javascript">
function show(e1Id)
{
for (var i=1; i<=4; i++) {
 document.getElementById('1'+i).className = 'hide';
 }
 document.getElementById('1'+e1Id).className = 'show';
}
</script>  
  </head>
  <body>     

  <!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

 
  <!-- End header -->
 
      
  <!-- BEGIN MENU -->
  <section id="menu-area">      
    <nav class="navbar navbar-default" role="navigation">  
            <jsp:include page="/jsp/pagelinks.jsp"></jsp:include>    
     
    </nav>
  </section>
  <!-- END MENU -->  
  
  <!-- Start single page header -->
  <section id="single-page-header">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Research</h2>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="<%=request.getContextPath()%>/home.do?action=list">Home</a></li>
                <li class="active">Research</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  
  <!-- Start blog archive -->
  <section id="blog-archive">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="blog-archive-area">
            <div class="row">
              <div class="col-md-3 hidden-sm hidden-xs">
                <aside class="blog-side-bar">
                  <!-- Start sidebar widget -->
                  <div class="sidebar-widget">
                    <!-- Start blog search -->
                    
                    <!-- End blog search -->                                
                  </div>
                  
                   
                </aside>
              </div>
              <div class="col-md-12">
                <div class="blog-archive-left">
                  <!-- Start blog news single -->
                  <article class="blog-news-single">
                    
                   <logic:notEmpty name="roleList">
            <logic:iterate id="userrole" name="roleList" indexId="userid">
                    <div class="blog-news-title">
                      <h2><bean:write name="userrole" property="heading" /></h2>
                      
                    </div>
                    <div class="blog-news-details blog-single-details">
                    <ul class="sports">
                    <bean:define id="abc" name="userrole" property="research_work" type="java.lang.String"/>
                    <%	String[] temp;
                        String delimiter = "~@~";
                        String s = abc;
                        int counter = 0;
                        for( int i=0; i<s.length(); i++ ) {
                        	if( s.contains("~@~") ) {
                        		counter++;
                        		}
                        	}
        		 temp = abc.split(delimiter);
        		  /* print substrings */
        		  if(counter>0){
        		  for(int i =0; i < temp.length ; i++)
        		  {
        			
        		    //System.out.println("preinting***************************:"+temp.length);%>
                      <li><%=temp[i]%></li>
                     <% }}%>
                      </ul>
                      </div>
                      </logic:iterate>
            </logic:notEmpty>
                      
                      
                      
                    </div>
                  </article>
              </div>             
            </div>
          </div>
        </div>
      </div>
    </div>  
  </section>
  <!-- End blog archive -->

 

  <!-- Start footer -->
  <footer id="footer">
    <jsp:include page="/jsp/Commanfooter.jsp"></jsp:include>
  </footer>
  <!-- End footer -->

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>    
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <!-- Bootstrap -->
  <script src="<%=request.getContextPath()%>/assets/js/bootstrap.js"></script>
  <!-- Slick Slider -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/slick.js"></script>    
  <!-- mixit slider -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->        
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/jquery.fancybox.pack.js"></script>
 <!-- counter -->
  <script src="<%=request.getContextPath()%>/assets/js/waypoints.js"></script>
  <script src="<%=request.getContextPath()%>/assets/js/jquery.counterup.js"></script>
  <!-- Wow animation -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/wow.js"></script> 
  <!-- progress bar   -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/bootstrap-progressbar.js"></script>  
  
 
  <!-- Custom js -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/custom.js"></script>
    
  </body>
</html>