<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div class="container">
        <div class="navbar-header">
          <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
                         
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul id="top-menu" class="nav navbar-nav  main-nav">
            <li class="active"><a href="<%=request.getContextPath()%>/home.do?action=list">Home</a></li>
         
         <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Contributions <span class="fa fa-angle-down"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<%=request.getContextPath()%>/jsp/books_authored.jsp">Books Authored</a></li>                
                <li><a href="<%=request.getContextPath()%>/jsp/exposure.jsp">International Exposure</a></li>
                <li><a href="<%=request.getContextPath()%>/asprincipalui.do?action=list">As A Principal, BMSCE</a></li>
                <li><a href="<%=request.getContextPath()%>/jsp/outcomes.jsp">Outcomes</a></li>           
              </ul
            ></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Recognitions <span class="fa fa-angle-down"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<%=request.getContextPath()%>/jsp/awards.jsp">Awards</a></li> 
                 <li><a href="<%=request.getContextPath()%>/jsp/important_recognitions.jsp">Important Recognitions</a></li>                            
                <li><a href="<%=request.getContextPath()%>/jsp/institutional_ recognitions.jsp">Institutional Recognitions</a></li>
                  
              </ul>
            </li>
            
            <li ><a href="<%=request.getContextPath()%>/researchworkui.do?action=list">Research</a></li>               
            <li class="dropdown ">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Innovations <span class="fa fa-angle-down"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<%=request.getContextPath()%>/jsp/academic_ innovations.jsp">Academic Innovations</a></li>                
                <li><a href="<%=request.getContextPath()%>/jsp/administrative_ practices.jsp">Administrative Practices</a></li>
                <li><a href="<%=request.getContextPath()%>/jsp/established_labs.jsp">Established Labs</a></li>
                <li><a href="<%=request.getContextPath()%>/jsp/best_practices.jsp">Best Practices at BMSCE</a></li>           
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Collaboration <span class="fa fa-angle-down"></span></a>
              <ul class="dropdown-menu" role="menu">
              <li><a href="<%=request.getContextPath()%>/engineeringorganizationui.do?action=list">Engineering Organizations </a></li>
                <li><a href="<%=request.getContextPath()%>/educationorganizationui.do?action=list">Educational Organizations</a></li>
                <li><a href="<%=request.getContextPath()%>/jsp/benefits.jsp">Benefits</a></li>
                       
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Activities <span class="fa fa-angle-down"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<%=request.getContextPath()%>/teqipui.do?action=list">TEQIP</a></li>                
                       
              </ul>
            </li>
            
              <li><a href="<%=request.getContextPath()%>/jsp/papers_published.jsp">Publications</a></li>      
               <li><a href="<%=request.getContextPath()%>/jsp/contact_us.jsp">Contact </a></li>        
          </ul>                     
        </div><!--/.nav-collapse -->
       
      </div> 
</body>
</html>