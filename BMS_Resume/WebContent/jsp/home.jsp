<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>My Profile: Home</title>
    <!-- Favicon -->
   <link rel="shortcut icon"  type="image/icon" href="assets/images/favicon5.png"/>
    <!-- Font Awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">    
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css"/> 
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="assets/css/jquery.fancybox.css" type="text/css" media="screen" /> 
    <!-- Animate css -->
    <link rel="stylesheet" type="text/css" href="assets/css/animate.css"/> 
    <!-- Progress bar  -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-progressbar-3.3.4.css"/> 
     <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/lite-blue-theme.css" rel="stylesheet">

    <!-- Main Style -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
	<!--//fonts-->
		<link href="assets/css/bootstrapn.css" rel="stylesheet" type="text/css" media="all" />
		<link href="assets/css/stylen.css" rel="stylesheet" type="text/css" media="all" />

    <!-- Fonts -->

    <!-- Open Sans for body font -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- Lato for Title -->
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>    
    
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- //for-mobile-apps -->
	<!-- js -->
		<script type="text/javascript" src="assets/js/jquery.min.js"></script>
	<!-- js -->
	<!-- start-smooth-scrolling -->
		<script type="text/javascript" src="assets/js/move-top.js"></script>
		<script type="text/javascript" src="assets/js/easing.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
		</script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


<link
	href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
	rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>

<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/additional-methods.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/additional-methods1.js"></script>

<script type="text/javascript">

$(document).ready(function()
		   {
	   //alert("append value");
		   $.ajax({
			   type: "GET",
			   url:"<%=request.getContextPath()%>/home.do?action=detailsAppend",
			   async:false,
			   success:function(r){				    
					var json= JSON.parse(r);
					$.each(json,function(i,obj){	
											
	 				var a,b;
	 				//alert("---");
	 				var name = obj.name;
	 				$("#name").text(name);
	 				
	 				var designation = obj.designation;
	 				$("#designation").text(designation);
	 				
	 				var worksat = obj.worksat;
	 				$("#worksat").text(worksat);

	 				var photo = obj.photo;
	 				//alert(photo);
	 				$("#image").attr("src","<%=request.getContextPath() %>/Employeeupload/"+photo);
	 				
	 				var aboutme = obj.aboutme;
	 				$("#aboutme").text(aboutme);
  					var total=obj.corecompetencies;  					
  					var value= total.split("~@~");
  					var total1=obj.mystrength;  					
  					var value1= total1.split("~@~");
  					
  					//alert(value);
  					//alert(value1);
  					
  					var l= value.length;
  					var l1= value1.length;

  					var onepageresume = obj.onepageresume;
	 				$("#onepageresume").attr("href","<%=request.getContextPath() %>/Employeeupload/"+onepageresume);

	 				var detailresume = obj.detailresume;
	 				$("#detailresume").attr("href","<%=request.getContextPath() %>/Employeeupload/"+detailresume);
	 					
  					for(a= 0;a<l;a++){
  						var markup = "<tr><td style='text-align:center'>"+value[a]+"</td></tr>";
  						
	     		            $("#corecompetencies").append(markup);
	     				
	     					}
  					for(b= 0;b<l1;b++){
  						var markup1 = "<tr><td>"+value1[b]+"</td></tr>";
  						
	     		            $("#mystrength").append(markup1);
	     				
	     					}
  					
  					});
			   }
		   });		   
	});

</script>
    
    
    
  </head>
  <body>
 
   
    <!-- header bottom -->
    
  <!-- End header -->
   <!-- BEGIN MENU -->
  <section id="menu-area">      
    <nav class="navbar navbar-default" role="navigation">  
            <jsp:include page="/jsp/pagelinks.jsp"></jsp:include>    
     
    </nav>
  </section>
  <!-- END MENU -->  
  <div class="banner" id="home">
	<div class="container"> 
		<div class="top-header row">
			<script src="js/classie.js"></script>
            <div class="banner-info">
             <div class="col-md-6">
			<div class="banner-left">
				<img id="image" class="img-responsive" src="" alt="" style="border-radius:100%; border:1px solid #999999; width: 250px;
height: 250px;margin-top: 10px; margin-bottom: 6px;"/>
			</div>
            </div>
             <div class="col-md-6">
			<div class="banner-right">
				<h1 id="name"></h1>
                <h3 id="designation"></h3>
                <h3 id="worksat"></h3>
               
				<!--<div class="border"></div>
				<h2>WEB MASTER</h2>-->
			<!--	<a href="#">DOWNLOAD MY RESUME</a>-->
			</div>
            </div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

 
      
 
 
  <section id="feature" style="background-color: #fff;"> 
    <div class="container"> 
      <div class="row" > 
        <div class="col-md-12" >
          <div class="title-area">
            <h2 class="title">About Me</h2>
            <span class="line"></span><br><br>
            
            	<p id="aboutme"></p>
            
            <br><br>
          </div>
        </div>
                              
      </div> 
    </div> 
  </section> 
  <!-- End About Me -->
  
  <section id="about"> 
    <div class="container"> 
      <div class="row"> 
        <div class="col-md-12">
          <div class="title-area" style="margin-bottom: -30px;">
            <h2 class="title1">Core Competencies</h2>
            <span class="line"></span>
           
          </div>
        </div>
        <div class="col-md-12"> 
          <div class="feature-content"> 
                  <center> 
                  	<table class="table table-bordered table-responsive table-condensed" id="corecompetencies">

                  
                    </table>
                </center>
                
            
          </div> 
        </div> 
      </div> 
    </div> 
  </section> 
  
  
  <!-- Start Feature --> 
  <section id="about" style="background-color: #fff;"> 
    <div class="container"> 
      <div class="row"> 
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title1">My Strengths </h2>
            <span class="line"></span>
           
          </div>
        </div>
        <div class="col-md-12"> 
          <div class="feature-content"> 
            <div class="row"> 
              <div class="col-md-3"></div>
              <div class="col-md-6 col-sm-12">
               
                  <p>
                  <table class="table table-bordered table-responsive table-condensed" style="height: 157px;" id="mystrength">
                  	
                    </table>
                    </p>
                
              </div>
              <div class="col-md-3"></div>
            </div> 
          </div> 
        </div> 
      </div> 
    </div> 
  </section> 
  <!-- End Feature -->

  <!-- Start about  -->
  <section id="feature">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">Social Standing</h2>
            <span class="line"></span>
            
          </div>
        </div>
        <div class="col-md-12">
          <div class="about-content">
            <div class="row">
              <div class="col-md-3"></div>
              <div class="col-md-6 col-sm-12">
                <div class="why-choose-us">
                <div class="single-feature wow zoomIn">
                
                  <h3 align="left">Positions / Committees currently involved</h3>
                  <div class="panel-group why-choose-group" id="accordion">
                    
                    
                    <logic:notEmpty name="roleList">
            <logic:iterate id="userrole" name="roleList" indexId="userid">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                           <bean:write name="userrole" property="title" /><span class="fa fa-minus-square"></span>
                          </a>
                        </h4>
                      </div>
                      <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                        <bean:define id="abc" name="userrole" property="socialstanding" type="java.lang.String"/>
                        <%	String[] temp;
                        String delimiter = "~@~";
                        String s = abc;
                        int counter = 0;
                        for( int i=0; i<s.length(); i++ ) {
                        	if( s.contains("~@~") ) {
                        		counter++;
                        		}
                        	}
        		 temp = abc.split(delimiter);
        		  /* print substrings */
        		  if(counter>0){
        		  for(int i =0; i < temp.length ; i++)
        		  {
        			
        		    //System.out.println("preinting***************************:"+temp.length);%>
                        
                        
                         <p><%=temp[i]%></li>
                         <% }}%>
                         </ul>
                         </p>
                        </div>
                        
                      </div>
                    </div>
                    </logic:iterate>
            </logic:notEmpty>
                    
                    
                    
                    
                    
                  </div>
                  </div>
                </div>
              </div>
              <div class="col-md-3"></div>
              <div class="col-md-6">
                <div class="our-skill">
                <!--<div class="single-feature wow zoomIn">
                  <h3 align="left">Other Responsibilities at Institution Level</h3>                  
                  <div class="our-skill-content">
               
                    <ul class="index"><li>BOD member of Melton Foundation, USA</li>
                         <li>Chairman of Academic Council</li>
                         <li>Programme Coordinator, TEQIP (Phase l)</li>
                         <li>Chief Coordinator for conduct of theory examinations</li>
                         <li>Coordinator for national/international education fairs</li>
                         <li>Chief coordinator for the college Accreditation committee</li>
                         <li>Honorary Director of BMS Consultancy </li>
                         <li>Campus Coordinator-Melton Foundation </li>
                         <li>Convener of Annual Academic Performance Monitoring Committee</li>
                         
                         </ul>
                  
                  
                  
                  
                   
                  </div> 
                  </div>                 
                --></div>
              </div>              
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- end about -->

 
  <!-- Start subscribe us -->
  <section id="subscribe"> 
    <div class="subscribe-overlay">
      <div class="container">
        <div class="row" align="center">
         
            <div class="subscribe-area">
            <div class="col-md-1"></div>
            <div class="col-md-4">
            <h3 class="wow fadeInUp">Download My Resume :</h3></div> 
            <div class="col-md-6">
             <ul style="list-style:none; color:#FFFFFF">
             <li style="float:left"><h3 class="wow fadeInUp"><a href="" target="_blank" style="color:#FFFFFF" id="onepageresume">One Page Profile</a></h3></li> 
             <li style="float:left"><h3>|</h3></li>
            <li style="float:left"><h3 class="wow fadeInUp"><a href="" target="_blank" style="color:#FFFFFF" id="detailresume">Detailed Profile </a></h3></li>
            </ul>
            </div>
              <div class="col-md-1"></div>
             </div>
            </div>
            </div>
           
         
        
      </div>
    </div>
  </section> 
  <!-- End subscribe us -->

  <!-- Start footer -->
  <footer id="footer">
    <jsp:include page="/jsp/Commanfooter.jsp"></jsp:include>
    
  </footer>
  <!-- End footer -->
<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>    
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <!-- Bootstrap -->
  <script src="<%=request.getContextPath()%>/assets/js/bootstrap.js"></script>
  <!-- Slick Slider -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/slick.js"></script>    
  <!-- mixit slider -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->        
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/jquery.fancybox.pack.js"></script>
 <!-- counter -->
  <script src="<%=request.getContextPath()%>/assets/js/waypoints.js"></script>
  <script src="<%=request.getContextPath()%>/assets/js/jquery.counterup.js"></script>
  <!-- Wow animation -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/wow.js"></script> 
  <!-- progress bar   -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/bootstrap-progressbar.js"></script>  
  
 
  <!-- Custom js -->
  <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/custom.js"></script>
  
  </body>
</html>