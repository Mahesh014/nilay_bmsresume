z<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Engineering organizations</title>

    <!-- Bootstrap Core CSS -->
    <link href="<%=request.getContextPath()%>/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<%=request.getContextPath()%>/css/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="<%=request.getContextPath()%>/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<%=request.getContextPath()%>/css/startmin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<%=request.getContextPath()%>/css/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<%=request.getContextPath()%>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<style>
.image-preview-input {
    position: relative;
	overflow: hidden;
	margin: 0px;    
    color: #333;
    background-color: #fff;
    border-color: #ccc;    
}
.image-preview-input input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	font-size: 20px;
	cursor: pointer;
	opacity: 0;
	filter: alpha(opacity=0);
}
.image-preview-input-title {
    margin-left:2px;
}
</style>

<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.9.1.js"></script>
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/common.validation.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/additional-methods.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/additional-methods1.js"></script>

<script>
function PreviewImage(no) {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("uploadImage"+no).files[0]);
    oFReader.onload = function (oFREvent) {
        document.getElementById("uploadPreview"+no).src = oFREvent.target.result;
    };
}
</script>

<script>
	$(document).ready(function() {
		$('#myform').validate( { // initialize the plugin
					rules : {
						name : {
							required : true,
							letterswithbasicpunc : true
						},
						heading : {
							required : true,
							lettersonly : true
						}
					},
					messages : {
						name : {
							required : "PLEASE ENTER THE EDUCATIONAL COLLABORATION DETAILS",
							letterswithbasicpunc : "ENTER ONLY LETTERS WITH BASIC PUNCTIONS"
						},
						heading : {
							required : "PLEASE ENTER THE EDUCATIONAL COLLABORATION HEADING",
							lettersonly : "ENTER ONLY LETTERS WITH BASIC PUNCTIONS"
						}
					}
				});
	});
</script>

<script type="text/javascript">

    

   function edit(id,heading,userphoto,name)
{ 
	//alert(id);
	$("#id").val(id);
    		
	$("#heading").val(heading);
	$("#uploadPreview11").attr("src","/BMS_Resume/Employeeupload/"+userphoto);
	$("#filename").val(userphoto);
	$("#textarea1").val(name);

	
	
	$("#submit1").val("update");	
	$("#myform").attr('action',"engineeringcollaboration.do?action=update"); 
}


</script>

</head>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Admin</a>
        </div>

        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        

        <!-- Top Navigation: Right Menu -->
        <ul class="nav navbar-right navbar-top-links">
           <li><a href="<%=request.getContextPath() %>/logout.do?action=AdminLogoutAction"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
        </ul>
        <!-- Sidebar -->
        <jsp:include page="/masterdata/sidebar.jsp"></jsp:include>
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
      <div class="container-fluid">
         <div class="row"><div class="col-lg-12"><h3 class="page-header">Engineering Collaboration</h3></div></div>
					 
                      
                      <html:form method="post" action="engineeringcollaboration.do?action=add" styleId="myform" enctype="multipart/form-data">
                      
                      <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                         <div class="form-group">
                          <label>Heading </label>
               			 	 <input type="text" class="form-control" id="heading" name="heading">
               			 	 <input type="hidden" name="id" id="id"></input>
                   			</div></div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                             <div class="form-group">
                          <label style="display:block;">Enter Engineering Organization's Picture </label>
               			 	<img id="uploadPreview11" src="" style="height:100px;width:100px;"> <!-- don't give a name === doesn't send on POST/GET -->
                			<span class="input-group-btn">
                         	<!-- image-preview-clear button -->
                    		<button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        	<span class="glyphicon glyphicon-remove"></span> Clear
                    		</button>
                    		<!-- image-preview-input -->
                    		
                    		<div class="btn btn-default image-preview-input" style="margin-top: 2%;">
							<span class="glyphicon glyphicon-folder-open"></span>
                        	<span class="image-preview-input-title">Browse</span>
                        	<input type="file" accept="image/png, image/jpeg, image/gif" id="uploadImage11" type="file" name="photo1" onchange="PreviewImage(11)"/> <!-- rename it -->
                   			<input type="hidden" name="filename" id="filename"></input>
                   			</div>
			                </span>
            				</div></div></div>
                      <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group">
                          <label>Enter Engineering Collaboration Details</label>
                           <textarea class="form-control" rows="3" id="textarea1" name="name"></textarea>
                           </div> </div>  
                      </div>
                      
                    
                      
                      
                      <div class="row">
                         <div class="col-lg-12">
                          <input type="submit" class="btn btn-primary" value="submit" id="submit1">
                           <button type="cancel" class="btn btn-primary">Cancel</button>
                           </div>
                      </div>
                      
                    </html:form>
                   
                   
                    <hr>
<!--        --------------------------------------------------------------------------------------------------------------------------            -->
                   <div class="table-responsive">
                      <table class="table table-bordered">
                        <tr>
                        	<th>Sr.no</th>
                            <th>Heading</th>
                            <th>Image</th>
                            <th>Description</th>                            
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        <tbody>
                        <logic:notEmpty name="roleList">
								<logic:iterate id="userrole" name="roleList" indexId="index">
                        <tr>
                        	<td><%=index + 1%></td>
                            <td><bean:write name="userrole" property="heading" /></td>
                            <td><bean:write name="userrole" property="userphoto" /></td>
                            <td><bean:write name="userrole" property="name"/></td>
                          
                            <td><logic:equal name="userrole" property="active" value="true">
					<font color="green">Active</font>
				</logic:equal> 
				<logic:equal name="userrole" property="active" value="false"><font color="red">Inactive</font>
				</logic:equal></td>
                            <td>                          
                               
                               <logic:equal name="userrole" property="active" value="true">
           <a href='engineeringcollaboration.do?action=changestatus&amp;userid=<bean:write name="userrole" property="userid"/>&amp;id=<bean:write name="userrole" property="id"/>&amp;active=false'>
           <input type="button" value="Remove"  class="btn btn-info"/></a>
           							&nbsp;                             
                            <a href='#'
						onclick='edit(
						"<bean:write name="userrole" property="id" />",
						"<bean:write name="userrole" property="heading" />",
						"<bean:write name="userrole" property="userphoto" />",
						"<bean:write name="userrole" property="name"/>")'>				
					<button type="button" class="btn btn-info" name="userrole"
						value="Edit">Edit</button>
										

					</a> 
					</logic:equal> 
					
					<logic:equal name="userrole" property="active" value="false">

					<a href='engineeringcollaboration.do?action=changestatus&amp;userid=<bean:write name="userrole" property="userid"/>&amp;id=<bean:write name="userrole" property="id"/>&amp;active=true'><input
						type="button" value="Activate"  class="btn btn-info"/></a>

				</logic:equal>
					         
					</td>
                        </tr>
                        </logic:iterate>
									</logic:notEmpty>
                        </tbody>
                      </table>
					</div>
</div>
</div>
</div>


<!-- Bootstrap Core JavaScript -->
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<%=request.getContextPath()%>/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<%=request.getContextPath()%>/js/startmin.js"></script>
<script src="<%=request.getContextPath()%>/js/image-preview.js"></script>

</body>
</html>