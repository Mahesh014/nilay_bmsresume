<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Resume-Admin Dashboard</title>

    <!-- Bootstrap Core CSS -->
    <link href="<%=request.getContextPath()%>/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<%=request.getContextPath()%>/css/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="<%=request.getContextPath()%>/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<%=request.getContextPath()%>/css/startmin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<%=request.getContextPath()%>/css/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<%=request.getContextPath()%>/css/font-awesome.min.css" rel="stylesheet" type="text/css">


<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript"
        src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>
<script type="text/javascript"
        src="<%=request.getContextPath()%>/js/validation/common.validation.js"></script>
<script type="text/javascript"
        src="<%=request.getContextPath()%>/js/validation/additional-methods.js"></script>
<script type="text/javascript"
        src="<%=request.getContextPath()%>/js/validation/additional-methods1.js"></script>


<script type="text/javascript">

$(document).ready(function(){

	//alert();
				   $.ajax({
					   type: "GET",
					   url:"<%=request.getContextPath()%>/resume.do?action=detailsAppend",
					   async:false,
					   success:function(r){
						    
							var json= JSON.parse(r);
		 					$.each(json,function(i,obj){

			 					var onepage=obj.onepageresume;
		     					var detail=obj.detailresume;

		     					//alert(onepage);
		     					
		     					$("#mydoc").append(onepage);
		     					$("#mypdf").append(detail);
		     					

		     				    
		     					
		     					if(obj!=null){
				     				
			     					$("#submit").val("Update");
			     					 $("#myform").attr('action','<%=request.getContextPath()%>/resume.do?action=update');
				     				}
		     					
		     					
		     				});

		     				

					   }


				   });

				   
			});

</script>
 
</head>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Admin</a>
        </div>

        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        

        <!-- Top Navigation: Right Menu -->
        <ul class="nav navbar-right navbar-top-links">
           <li><a href="<%=request.getContextPath() %>/logout.do?action=AdminLogoutAction"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
        </ul>

        <!-- Sidebar -->
        <jsp:include page="/masterdata/sidebar.jsp"></jsp:include>
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
      <div class="container-fluid">
         <div class="row"><div class="col-lg-12"><h3 class="page-header">Resume</h3></div></div>
					 
                      
                      <html:form method="post" action="resume.do?action=details" enctype="multipart/form-data" styleId="myform">
                      
                      <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group">
                            <label> One Page Profile </label>
                            <input type="file" id="doc" name="doc" accept=".doc" class="mydoc" ></input>
                            <label for="mydoc" id="mydoc"></label>
                   </div>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group">
                          <label>Detailed Profile </label>
                          <input type="file" id="pdf" name="pdf" accept=".pdf" class="mydoc" >
                          <label for="mypdf" id="mypdf"></label>
                      </div>
                      
                      <div class="row">
                         <div class="col-lg-12">
                          <input type="submit" class="btn btn-primary" value="save" id="submit">
                           <button type="cancel" class="btn btn-primary">Cancel</button>
                           </div>
                      </div>
                      
                    </html:form>
                   
                   
                    
</div>
</div>
</div>

<!-- jQuery -->
<script src="js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="js/startmin.js"></script>

</body>
</html>