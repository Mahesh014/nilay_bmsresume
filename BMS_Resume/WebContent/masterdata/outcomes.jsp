<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Resume-Admin Dashboard</title>

    <!-- Bootstrap Core CSS -->
    <link href="<%=request.getContextPath()%>/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<%=request.getContextPath()%>/css/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="<%=request.getContextPath()%>/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<%=request.getContextPath()%>/css/startmin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<%=request.getContextPath()%>/css/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<%=request.getContextPath()%>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<style>
.image-preview-input {
    position: relative;
	overflow: hidden;
	margin: 0px;    
    color: #333;
    background-color: #fff;
    border-color: #ccc;    
}
.image-preview-input input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	font-size: 20px;
	cursor: pointer;
	opacity: 0;
	filter: alpha(opacity=0);
}
.image-preview-input-title {
    margin-left:2px;
}
</style>

<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/common.validation.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/additional-methods.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/additional-methods1.js"></script>

<script type="text/javascript">

    $(document).ready(function(){
    	 var i=1;
        $("#addrow").click(function(){
           
            var markup = "<tr><td><input type='checkbox' name='record'></td><td><input type='text' class='form-control' name='outcomeyear' id='outcomeyear"+i+"' required></td><td><input type='text' class='form-control' name='outcome' id='outcome"+i+"' required></td></tr>";
            $("table tbody").append(markup);
            i++;
        });
    });  
    
       $(document).ready(function()
    		   {
		   //alert();
			   $.ajax({
				   type: "GET",
				   url:"<%=request.getContextPath()%>/outcomes.do?action=detailsAppend",
				   async:false,
				   success:function(r){
					    
						var json= JSON.parse(r);
	 					$.each(json,function(i,obj){

		 					var a;
	     					var total=obj.outcomes;
	     					var value= total.split("~@~");
	     					var total1=obj.outcomesyear;
	     					var value1= total1.split("~@~");
	     					var l= value.length;
	     					$("#outcomeyear0").val(value1[0]);
	     					$("#outcome0").val(value[0]);
	     					for(a= 1;a<l;a++){
	     						var markup = "<tr><td><input type='checkbox' name='record'></td><td><input type='text' class='form-control' name='outcomeyear' id='outcomeyear"+i+"' value='"+value1[a]+"'></td><td><input type='text' class='form-control' name='outcome' id='outcome"+i+"' value='"+value[a]+"'></td></tr>";
		     		            $("table tbody").append(markup);
		     					i++;
		     					}
	     					});

				   }


			   });

			   
				   });
       
</script>

<script>
	$(document).ready(function() {
		
		$('#myform').validate( { // initialize the plugin
					rules : {
						outcomeyear : {
							required : true,
							maxlength : 4,
							minlength : 4,
							integer : true
						},
						outcome : {
							required : true,
							letterswithbasicpunc : true
						}
					},
					messages : {
						outcomeyear : {
							required : "PLEASE ENTER YOUR STRENGTH",
							maxlength : "PLEASE ENTER VALID YEAR NUMBER",
							minlength : "PLEASE ENTER VALID YEAR NUMBER",
							integer : "ENTER NUMBER ONLY"
						},
						outcome : {
							required : "PLEASE ENTER YOUR OUTCOMES",
							letterswithbasicpunc : "ENTER ONLY LETTERS WITH BASIC PUNCTIONS"
						}					
					}
				});
	});
</script>

<script type="text/javascript">
$(document).ready(function(){
	
	// Find and remove selected table rows
	$("#deleterow").on("click", function () {
		var tablenum = $('#myform tr').length-1;
		//alert(tablenum);
		$("table tbody tr").find('input[name="record"]').each(function(){
        	if($(this).is(":checked")){
            	if(tablenum>1){
                $(this).parents("tr").remove();
                tablenum--;
            }
        	}
        });
  	    });
});
</script>

</head>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Admin</a>
        </div>

        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        

        <!-- Top Navigation: Right Menu -->
        <ul class="nav navbar-right navbar-top-links">
           <li><a href="<%=request.getContextPath() %>/logout.do?action=AdminLogoutAction"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
        </ul>

        <!-- Sidebar -->
        <jsp:include page="/masterdata/sidebar.jsp"></jsp:include>
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
      <div class="container-fluid">
         <div class="row"><div class="col-lg-12"><h3 class="page-header">Outcomes</h3></div></div>
         	
         	
         	<html:form action="outcomes.do?action=details" method="post" styleId="myform">
         		<div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                       
                      <table class="table table-bordered table-hover" id="tab_logic">
						<thead>
							<tr><th class="text-center">Select</th>
							<th class="text-center">Enter Year</th>
							<th class="text-center">Enter the your Outcome</th>
							</tr>
						</thead>
						<tbody>
							<tr id='addr0'>
							<td><input type="checkbox" name="record"></td>
							<td><input type="text" class="form-control" name="outcomeyear" id="outcomeyear0" required></td>
							<td><input type="text" class="form-control" name=outcome id="outcome0" required></td>
							</tr>
						</tbody>
					</table></div></div>
					<div class="row">
                         <div class="col-lg-12">
            				<a id="addrow" class="btn btn-default ">Add Row</a>
                            <a id="deleterow" class="btn btn-default">Delete Row</a>
                          </div>
                          </div>

<br>

	<div class="row">
	<div class="col-lg-12">
	<button type="submit" class="btn btn-primary">Save</button>
	<button type="reset" class="btn btn-primary">Cancel</button>
	</div>
	</div>

</html:form>
</div>
</div>
</div>



<!-- Bootstrap Core JavaScript -->
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<%=request.getContextPath()%>/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<%=request.getContextPath()%>/js/startmin.js"></script>
<script src="<%=request.getContextPath()%>/js/image-preview.js"></script>

</body>
</html>