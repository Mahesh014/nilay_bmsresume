<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>LOGIN</title>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript"
        src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>
<script type="text/javascript"
        src="<%=request.getContextPath()%>/js/validation/common.validation.js"></script>
<script type="text/javascript"
        src="<%=request.getContextPath()%>/js/validation/additional-methods.js"></script>
<script type="text/javascript"
        src="<%=request.getContextPath()%>/js/validation/additional-methods1.js"></script>
<script>
	$(document).ready(function() {
		//alert();
		$('#myform').validate( { // initialize the plugin
					rules : {
						newpassword : {
							required : true
						},
						password : {
							required : true
						},
						confirmpassword : {
							required : true,
							equalTo : "#newpassword"
						}
					},
					messages : {
						newpassword : {
							required : "PLEASE ENTER THR EMAIL_ID"
						},
						password : {
							required : "PLEASE ENTER THE PASSWORD"
						},
						confirmpassword : {
							required : "PLEASE ENTER THE PASSWORD",
							equalTo : "PLEASE ENTER SAME PASSWORD AS NEW PASSWORD"
						}
					}
				});
	});
</script>
</head>
<body>

<logic:notEmpty name="status">
	<font color="red"><bean:write name="status"></bean:write></font>
</logic:notEmpty>

<form method="post" action="<%=request.getContextPath() %>/login.do?action=changepassword" id="myform">
<table>
	<tr>
		<td>OLD PASSWORD</td>
		<td><input type="password" name="password"
			placeholder="Enter old password"></td>
	</tr>
	<tr>
		<td>NEW PASSWORD</td>
		<td><input type="password" name="newpassword" id="newpassword"
			placeholder="Enter new password"></td>
	</tr>
	<tr>
		<td>CONFIRM PASSWORD</td>
		<td><input type="password" name="confirmpassword"
			placeholder="Enter confirm password"></td>
	</tr>
	<tr>
		<td><input type="submit" value="CHANGE"></td>
		<td><input type="reset" value="RESET"></td>
	</tr>
</table>
</form>
</body>
</html>