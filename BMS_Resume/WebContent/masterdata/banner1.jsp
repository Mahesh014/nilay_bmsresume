<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Home page Banner</title>

    <!-- Bootstrap Core CSS -->
    <link href="<%=request.getContextPath()%>/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<%=request.getContextPath()%>/css/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="<%=request.getContextPath()%>/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<%=request.getContextPath()%>/css/startmin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<%=request.getContextPath()%>/css/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<%=request.getContextPath()%>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<style>
.image-preview-input {
    position: relative;
	overflow: hidden;
	margin: 0px;    
    color: #333;
    background-color: #fff;
    border-color: #ccc;    
}
.image-preview-input input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	font-size: 20px;
	cursor: pointer;
	opacity: 0;
	filter: alpha(opacity=0);
}
.image-preview-input-title {
    margin-left:2px;
}
</style>
  
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript"
        src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>
<script type="text/javascript"
        src="<%=request.getContextPath()%>/js/validation/common.validation.js"></script>
<script type="text/javascript"
        src="<%=request.getContextPath()%>/js/validation/additional-methods.js"></script>
<script type="text/javascript"
        src="<%=request.getContextPath()%>/js/validation/additional-methods1.js"></script>


<script>
	$(document).ready(function() {
		alert();
		$('#myform').validate( { // initialize the plugin
					rules : {
						name : {
							required : true,
							letterswithbasicpunc : true
						},
						designation : {
							required : true,
							lettersonly : true
						},
						worksat : {
							required : true,
							letterswithbasicpunc : true
						}
					},
					messages : {
						name : {
							required : "PLEASE ENTER THE NAME",
							letterswithbasicpunc : "ENTER ONLY LETTERS WITH BASIC PUNCTIONS"
						},
						designation : {
							required : "PLEASE ENTER THE DESIGNATION",
							lettersonly : " ENTER THE ONLY LETTER"
						},
						worksat : {
							required : "PLEASE ENTER THE WORKSAT",
							letterswithbasicpunc : "ENTER ONLY LETTERS WITH BASIC PUNCTIONS"
						}
					}
				});
	});
</script>


<!--<script>
  var loadFile = function(event) {
		var output = document.getElementById('output');
    	output.src = URL.createObjectURL(event.target.files[0]);
  };
</script>
--><script type="text/javascript">

       $(document).ready(function(){
//alert();
			   $.ajax({
				   type: "GET",
				   url:"<%=request.getContextPath()%>/banner.do?action=detailsAppend",
				   async:false,
				   success:function(r){
					    
						var json= JSON.parse(r);
	 					$.each(json,function(i,obj){

		 					var d=obj.designation;
	     					var n=obj.name;
	     					var w=obj.worksat;
	     					var p=obj.photo;
	     					$("#name").val(n);
	     					$("#designation").val(d);
	     					$("#worksat").val(w);
	     					$("#uploadPreview11").attr("src","<%=request.getContextPath() %>/Employeeupload/"+p);
	     					
	     				});

				   }


			   });

			   
		});

       function PreviewImage(no) {
    	    var oFReader = new FileReader();
    	    oFReader.readAsDataURL(document.getElementById("uploadImage"+no).files[0]);
    	    oFReader.onload = function (oFREvent) {
    	        document.getElementById("uploadPreview"+no).src = oFREvent.target.result;
    	    };
    	}  
       
       
       </script>

</head>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Admin</a>
        </div>

        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        

        <!-- Top Navigation: Right Menu -->
        <ul class="nav navbar-right navbar-top-links">
           <li><a href="<%=request.getContextPath() %>/logout.do?action=AdminLogoutAction"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
        </ul>

        <!-- Sidebar -->
        <jsp:include page="/masterdata/sidebar.jsp"></jsp:include>
    </nav>

    <!---------------------------------------------- Page Content ----------------------------------------------->
    <div id="page-wrapper">
      <div class="container-fluid">
         <div class="row"><div class="col-lg-12"><h3 class="page-header">Banner and Title</h3></div></div>
					 
                      
                      <html:form method="post" action="banner.do?action=details" enctype="multipart/form-data" styleId="myform">
                      
                      <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                         
                          <div class="form-group">
                          
                          <label style="display:block;">Select Profile Picture </label>
               			 	<img id="uploadPreview11" src="images/no_image.jpg" style="height:100px;width:100px;"> <!-- don't give a name === doesn't send on POST/GET -->
                			<span class="input-group-btn">
                         	<!-- image-preview-clear button -->
                    		<button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        	<span class="glyphicon glyphicon-remove"></span> Clear
                    		</button>
                    		<!-- image-preview-input -->
                    		
                    		<div class="btn btn-default image-preview-input" style="margin-top: 2%;">
							<span class="glyphicon glyphicon-folder-open"></span>
                        	<span class="image-preview-input-title">Browse</span>
                        	<input type="file" accept="image/png, image/jpeg, image/gif" id="uploadImage11" type="file" name="photo1" onchange="PreviewImage(11)"/> <!-- rename it -->
                   			</div>
			                </span>
            				</div></div><!-- /input-group image-preview [TO HERE]--> 
                      
                      
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group">
                          <label>Name</label>
                          <input type="text" class="form-control" name="name" id="name" required>
                           </div> </div>  
                      </div>
                      
                      <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                         <div class="form-group">
                          <label>Designation</label>
                          <input type="text" class="form-control" name="designation" id="designation" required>
                           </div>
               			 	
                      </div>
                      
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group">
                          <label>Works at</label>
                          <input type="text" class="form-control" name="worksat" id="worksat" required>
                           </div> </div>  
                      </div>
                      
                      
                      <div class="row">
                         <div class="col-lg-12">
                          <button type="submit" class="btn btn-primary">Save</button>
                           <button type="reset" class="btn btn-primary">Cancel</button>
                           </div>
                      </div>
                      
                    </html:form>
                   
                   
                   
</div>
</div>
</div>



<!-- Bootstrap Core JavaScript -->
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<%=request.getContextPath()%>/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<%=request.getContextPath()%>/js/startmin.js"></script>
<script src="<%=request.getContextPath()%>/js/image-preview.js"></script>

</body>
</html>