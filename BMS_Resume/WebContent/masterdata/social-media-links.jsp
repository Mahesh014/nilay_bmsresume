<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Resume-Admin Dashboard</title>

    <!-- Bootstrap Core CSS -->
    <link href="<%=request.getContextPath() %>/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<%=request.getContextPath() %>/css/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="<%=request.getContextPath() %>/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<%=request.getContextPath() %>/css/startmin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<%=request.getContextPath() %>/css/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<%=request.getContextPath() %>/css/font-awesome.min.css" rel="stylesheet" type="text/css">




<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript"
        src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>
<script type="text/javascript"
        src="<%=request.getContextPath()%>/js/validation/common.validation.js"></script>
<script type="text/javascript"
        src="<%=request.getContextPath()%>/js/validation/additional-methods.js"></script>
<script type="text/javascript"
        src="<%=request.getContextPath()%>/js/validation/additional-methods1.js"></script>
<script>
	$(document).ready(function() {
		
		$('#myform').validate( { // initialize the plugin
					rules : {
						flink : {
							required : true,
							url : true
						},
						tlink : {
							required : true,
							url : true
						},
						ylink : {
							required : true,
							url : true
						},
						glink : {
							required : true,
							url : true
						}
					},
					messages : {
						flink : {
							required : "PLEASE ENTER URL",
							url : "PLEASE ENTER VALID URL LINK"
						},
						tlink : {
							required : "PLEASE ENTER URL",
							url : "PLEASE ENTER VALID URL LINK"
						},
						ylink : {
							required : "PLEASE ENTER URL",
							url : "PLEASE ENTER VALID URL LINK"
						},
						glink : {
							required : "PLEASE ENTER URL",
							url : "PLEASE ENTER VALID URL LINK"
						}
					}
				});
	});
</script>
<script type="text/javascript">
       $(document).ready(function()
    		   {
		   
			   $.ajax({
				   type: "GET",
				   url:"<%=request.getContextPath()%>/socialmedia.do?action=detailsAppend",
				   async:false,
				   success:function(r){
					    
						var json= JSON.parse(r);
	 					$.each(json,function(i,obj){

	     					var facebooklink=obj.facebooklink;
	     					$("#flink").val(facebooklink);
	     					
	     					var twitterlink=obj.twitterlink;
	     					$("#tlink").val(twitterlink);
	     					
	     					var youtubelink=obj.youtubelink;
	     					$("#ylink").val(youtubelink);
	     					
	     					var googlelink=obj.googlelink;
	     					$("#glink").val(googlelink);
	     					
	     					
	     					if(obj!=null){
	    	 					$("#submit").val("update");
	    	 					$("#myform").attr('action',"<%=request.getContextPath()%>/socialmedia.do?action=update");
	         					}
	     					});
     					
				   }


			   });

			   
				   });

    		  
       
       
       </script>




</head>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Admin</a>
        </div>

        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        

        <!-- Top Navigation: Right Menu -->
        <ul class="nav navbar-right navbar-top-links">
           <li><a href="<%=request.getContextPath() %>/logout.do?action=AdminLogoutAction"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
        </ul>

        <!-- Sidebar -->
        <jsp:include page="/masterdata/sidebar.jsp"></jsp:include>
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
      <div class="container-fluid">
         <div class="row"><div class="col-lg-12"><h3 class="page-header">Social Media</h3></div></div>
					 
                      
                      <html:form styleId="myform" action="socialmedia.do?action=details" method="post">
                      
                      <div class="row">
                      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                          <div class="form-group">
                            <label>Facebook Link </label>
                            <input type="text" class="form-control" id="flink" name="flink">
                          </div>
                      </div>
                      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <div class="form-group">
                            <label>Twitter Link </label>
                            <input type="text" class="form-control" id="tlink" name="tlink">
                          </div></div>
                       <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                       <div class="form-group">
                            <label>Youtube Link </label>
                            <input type="text" class="form-control" id="ylink" name="ylink">
                          </div>
                      </div>
                      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <div class="form-group">
                            <label>Google Plus Link </label>
                            <input type="text" class="form-control" id="glink" name="glink">
                          </div></div>  
                      </div>
                      
                      <div class="row">
                         <div class="col-lg-12">
                          
                          <input type="submit" class="btn btn-primary" id="submit" value="submit">
                           <button type="reset" class="btn btn-primary">Cancel</button>
                           </div>
                      </div>
                      
                    </html:form>
                   
                   
                    
</div>
</div>
</div>

<!-- jQuery -->
<script src="js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="js/startmin.js"></script>

</body>
</html>