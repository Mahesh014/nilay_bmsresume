<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Resume-Admin Dashboard</title>

    <!-- Bootstrap Core CSS -->
    <link href="<%=request.getContextPath()%>/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<%=request.getContextPath()%>/css/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="<%=request.getContextPath()%>/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<%=request.getContextPath()%>/css/startmin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<%=request.getContextPath()%>/css/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<%=request.getContextPath()%>/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/common.validation.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/additional-methods.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/additional-methods1.js"></script>

<script type="text/javascript">

    $(document).ready(function(){
    	 var i=1;
    	 
        $("#addrow").click(function(){
            var markup = "<tr><td><input type='checkbox' name='record'></td><td><input type='text' class='form-control' name='contributionasprincipal' id='contributionasprincipal"+i+"' required/></td></tr>";
            $("#tbody1").append(markup);
            i++;
        });
    });  

function edit(id,title,contributionasprincipal)
{ 
	$("#hid").val(id);
	$("#title").val(title);
	//alert(id);
	var valT = $("#title").val();
	//alert(valT);
	var ss=contributionasprincipal.split("~@~");
	
	
	var L = ss.length;
	//alert(L+"value of length is");
	$("#contributionasprincipal0").val(ss[0]);
	if(L>1){
		
	for(var a=1;a<L;a++){
		//alert(a);
		var markup1 ="<tr><td><input type='checkbox' name='record'></td><td><input type='text' class='form-control' name='contributionasprincipal' id=contributionasprincipal"+a+"' value='"+ss[a]+"'></td></tr>";
        $("#tbody1").append(markup1);
		}
	}
	$("#submit").val("update");
	$("#myform").attr('action',"contributionasprincipal.do?action=update"); 
}
</script>

<script>
	$(document).ready(function() {
		
		$('#myform').validate( { // initialize the plugin
					rules : {
						title : {
							required : true,
							letterswithbasicpunc : true
						},
						contributionasprincipal : {
							required : true,
							letterswithbasicpunc : true
						}
					},
					messages : {
						title : {
							required : "PLEASE ENTER YOUR CONTRIBUTION HEADING",
							letterswithbasicpunc : "ENTER ONLY LETTERS WITH BASIC PUNCTIONS"
						},
						contributionasprincipal : {
							required : "PLEASE ENTER YOUR CONTRIBUTION",
							letterswithbasicpunc : "ENTER ONLY LETTERS WITH BASIC PUNCTIONS"
						}
					}
				});
	});
</script>

<script type="text/javascript">
$(document).ready(function(){
	
	// Find and remove selected table rows
	$("#deleterow").on("click", function () {
		var tablenum = $('#myform tr').length-1;
		//alert(tablenum);
		$("#tbody1 tr").find('input[name="record"]').each(function(){
        	if($(this).is(":checked")){
            	if(tablenum>1){
                $(this).parents("tr").remove();
                tablenum--;
            }
        	}
        });
  	    });
});
</script>

</head>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Admin</a>
        </div>

        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        

        <!-- Top Navigation: Right Menu -->
        <ul class="nav navbar-right navbar-top-links">
           <li><a href="<%=request.getContextPath() %>/logout.do?action=AdminLogoutAction"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
        </ul>

        <!-- Sidebar -->
        <jsp:include page="/masterdata/sidebar.jsp"></jsp:include>
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
      <div class="container-fluid">
         <div class="row"><div class="col-lg-12"><h3 class="page-header">Contribution As Principal</h3></div></div>
					 
<!--  Form starts..............................................................................................  -->

<html:form action="contributionasprincipal.do?action=add" styleId="myform" method="post">

	<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
	<div class="form-group">
		<label>Heading </label>
		<input type="text" class="form-control" id="title" name="title">
		<input type="hidden" id="hid" name="id">
	</div>
	
	<table class="table table-bordered table-hover" id="tab_logic">
		<thead>
			<tr>
				<th class="text-center">select</th>
				<th class="text-center">Enter your Contributions</th>
			</tr>
		</thead>
		<tbody id="tbody1">
			<tr id='addr0'>
				<td><input type='checkbox' name='record'></td>
				<td><input type="text" class="form-control"	name="contributionasprincipal" id="contributionasprincipal0"/></td>
			</tr>
		</tbody>
	</table>
	</div>
	</div>
	<div class="row">
	<div class="col-lg-12">
		<a id="addrow" class="btn btn-default ">Add Row</a>
		<a id='deleterow' class="btn btn-default">Delete Row</a>
	</div>
	</div>
	<br>


	<div class="row">
	<div class="col-lg-12"><input type="submit"
		class="btn btn-primary" id="submit">
	<button type="reset" class="btn btn-primary">Reset</button>
	</div>
	</div>

</html:form>
                   
                   
                    <hr>
<!-- ------------------------------------------Form Ends----------------------------------------------------------- -->
<div class="table-responsive">
<table class="table table-bordered">
	<thead>
		<tr><th>Heading</th>
			<th>Contributions As a Principal</th>
			<th>Status</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
	<logic:notEmpty name="roleList">
				<logic:iterate id="userrole" name="roleList" indexId="userid">
				
		<tr>		<td><bean:write name="userrole" property="title" /></td>
					<td><bean:write name="userrole" property="contributionasprincipal"/></td>


					<td class="no-print">
				<logic:equal name="userrole" property="active" value="true">
					<font color="green">Active</font>
				</logic:equal> 
				<logic:equal name="userrole" property="active" value="false"><font color="red">Inactive</font>
				</logic:equal></td>  

					<td>                          
                               
                               <logic:equal name="userrole" property="active" value="true">
           <a href='contributionasprincipal.do?action=changestatus&amp;userid=<bean:write name="userrole" property="userid"/>&amp;id=<bean:write name="userrole" property="id"/>&amp;active=false'>
           <input type="button" value="Remove"  class="btn btn-info"/></a>
           							&nbsp;                             
                            <a href='#'
						onclick='edit(
						"<bean:write name="userrole" property="id"/>",
          				"<bean:write name="userrole" property="title" />",
						"<bean:write name="userrole" property="contributionasprincipal"/>")'><button type="button" class="btn btn-info" name="userrole"
						value="Edit">Edit</button>
										

					</a> 
					</logic:equal> 
					
					<logic:equal name="userrole" property="active" value="false">

					<a href='contributionasprincipal.do?action=changestatus&amp;userid=<bean:write name="userrole" property="userid"/>&amp;id=<bean:write name="userrole" property="id"/>&amp;active=true'><input
						type="button" value="Activate"  class="btn btn-info"/></a>

				</logic:equal>
					         
					</td>
		</tr>
		</logic:iterate>
		</logic:notEmpty>
	</tbody>

</table>
</div>
</div>
</div>
</div>

<!-- jQuery -->


<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="js/startmin.js"></script>
<script src="js/add-row.js"></script>
</body>
</html>
