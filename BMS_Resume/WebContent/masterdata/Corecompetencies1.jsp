<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Resume-Admin Dashboard</title>

    

<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript"
        src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>
<script type="text/javascript"
        src="<%=request.getContextPath()%>/js/validation/common.validation.js"></script>
<script type="text/javascript"
        src="<%=request.getContextPath()%>/js/validation/additional-methods.js"></script>
<script type="text/javascript"
        src="<%=request.getContextPath()%>/js/validation/additional-methods1.js"></script>
<script>
	$(document).ready(function() {
		$('#myform').validate( { // initialize the plugin
					rules : {
						corecompetenices : {
							required : true,
							letterswithbasicpunc : true
						}
					},
					messages : {
						corecompetenices : {
							required : "PLEASE ENTER YOUR CORE COMPETENICES",
							letterswithbasicpunc : "ENTER ONLY LETTERS WITH BASIC PUNCTIONS"
						}
					}
				});
	});
</script>
<!--<script>
function validateForm() {
    var x = document.forms["myForm"]["corecompetenices"].value;
   
    if (x == "") {
        alert("Name must be filled out");
        return false;
    }
}
</script>
-->

<script type="text/javascript">
var tablenum=1;
$(document).ready(function(){
	//alert("hiii");	
	
	$.ajax({
		   type: "GET",
		   url:"<%=request.getContextPath() %>/corecompetencies.do?action=detailsAppend",
		   async:false,
		   success:function(r){
			    
			var json= JSON.parse(r);
			$.each(json,function(i,obj){
	
				var a;
				var total=obj.corecompetencies;
				var value= total.split("~@~");
				var l= value.length;
				$("#corecompetenices00").val(value[0]);
				for(a= 1;a<l;a++){
					var markup = "<tr><td><input type='checkbox' name='record'></input></td><td><input type='text' name='corecompetenices' class='form-control' id='corecompetenices0'"+tablenum+"' value='"+value[a]+"' required><span style='color:red;' class='empresumeerror0'"+tablenum+"'></td></tr>";
		            $("table tbody").append(markup);
					tablenum++;
					//alert(tablenum);
					}
				if(obj!=null){
  					$("#submit").val("Update");
  					 
						}
				});
	
		   }
	
	});
	
	$("#addrow").click(function(){
		//alert("Add row");
		var test=0;
		var value= $("#corecompetenices0"+j).val();
		for(var j=0 ; j<tablenum ; j++){
			if($("#corecompetenices0"+j).val()=="")

			{
				//$(".empresumeerror0"+j).html("Please enter the details");
				//alert("Please Enter the data in all field first...");
				test++;
			}
		}

		
			if(test==0)
			{
			$('table tbody').append('<tr><td><input type="checkbox" name="record"></input></td><td><input type="text" name="corecompetenices" class="form-control" id="corecompetenices0'+tablenum+'" required><span style="color:red;" class="empresumeerror0'+tablenum+'"></td></tr>');
			
			////alert($("#corecompetenices00").append("corecompetenices0"+tablenum));
			tablenum++;
			//alert(tablenum);
			}
		
	});
    
});
</script>
<script type="text/javascript">
$(document).ready(function(){
	var tablenum = $('#myform tr').length-1;
	// Find and remove selected table rows
	$("#deleterow").on("click", function () {
		
		//alert(tablenum);
		$("table tbody tr").find('input[name="record"]').each(function(){
        	if($(this).is(":checked")){
            	if(tablenum>1){
                $(this).parents("tr").remove();
                tablenum--;
            }
        	}
        });
  	    });
});
</script>
<!-- Bootstrap Core CSS -->
    <link href="<%=request.getContextPath() %>/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<%=request.getContextPath() %>/css/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="<%=request.getContextPath() %>/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<%=request.getContextPath() %>/css/startmin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<%=request.getContextPath()%>/css/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<%=request.getContextPath() %>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Admin</a>
        </div>

        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        

        <!-- Top Navigation: Right Menu -->
        <ul class="nav navbar-right navbar-top-links">
           <li><a href="<%=request.getContextPath() %>/logout.do?action=AdminLogoutAction"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
        </ul>

        <!-- Sidebar -->
        <jsp:include page="/masterdata/sidebar.jsp"></jsp:include>
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
      <div class="container-fluid">
         <div class="row"><div class="col-lg-12"><h3 class="page-header">Core Competencies</h3></div></div>
					 
                      
                     <html:form action="corecompetencies.do?action=details" styleId="myform" method="post" onsubmit='return validateForm()'>
                      
                      <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <table class="table table-bordered table-hover" id="tab_logic">
						<thead>
							<tr><th class="text-center">select</th>
							<th class="text-center">Enter Your Core Competencies</th>
							</tr>
						</thead>
						<tbody>
							<tr id='addr0'>
							<td><input type="checkbox" name="record"></td>
							<td><input type="text" name="corecompetenices" id="corecompetenices00" class="form-control" required/><span style="color:red;" class="empresumeerror00"></span></td>
							</tr>
						</tbody>
					</table></div></div>
                    <div class="row">
                         <div class="col-lg-12">
            				<a id="addrow" class="btn btn-default ">Add Row</a>
                            <a id='deleterow' class="btn btn-default">Delete Row</a>
                          </div>
                          </div>
                    <br>

                      
                      <div class="row">
                         <div class="col-lg-12">
                          <input type="submit" class="btn btn-primary" id="submit" value="submit">
                           <button type="cancel" class="btn btn-primary">Cancel</button>
                           </div>
                      </div>
                      
                    </html:form>
                   
                   
                   </div>
</div>
</div>




<<!-- Bootstrap Core JavaScript -->
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<%=request.getContextPath()%>/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<%=request.getContextPath()%>/js/startmin.js"></script>
</body>
</html>
