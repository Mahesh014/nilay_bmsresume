<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">

                <ol class="nav" id="side-menu">
                    <li><a href="<%=request.getContextPath() %>/masterdata/dashboard.jsp"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a></li>
                    <li><a href="#"><i class="fa fa-sitemap fa-fw"></i> Menu<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="#">HOME PAGE<span class="fa arrow"></a>
	                            <ul class="nav nav-second-level">
		                            <li><a href="<%=request.getContextPath() %>/masterdata/banner1.jsp">Banner</a></li>
		                            <li><a href="<%=request.getContextPath() %>/masterdata/Aboutme.jsp">Aboutme</a></li>
		                            <li><a href="<%=request.getContextPath() %>/masterdata/Corecompetencies1.jsp">Corecompetencies</a></li>
		                            <li><a href="<%=request.getContextPath() %>/masterdata/my-strengths1.jsp">my-strengths</a></li>
		                            <li><a href="<%=request.getContextPath() %>/socialstanding.do?action=list">social standing</a></li>
		                            <li><a href="<%=request.getContextPath() %>/masterdata/resume-upload.jsp">Resume</a></li>
	                            </ul>
                            </li>
                            <li><a href="#">CONTRIBUTION<span class="fa arrow"></a>
	                            <ul class="nav nav-second-level">
		                            <li><a href="<%=request.getContextPath() %>/masterdata/book-authored1.jsp">book-authored</a></li>
		                            <li><a href="<%=request.getContextPath() %>/masterdata/International-exposure.jsp">International-exposure</a></li>
		                            <li><a href="<%=request.getContextPath() %>/contributionasprincipal.do?action=list">Contribution as principal</a></li>
		                            <li><a href="<%=request.getContextPath() %>/masterdata/outcomes.jsp">outcomes</a></li>
	                            </ul>
                            </li>
                            <li><a href="#">RECOGNITIONS<span class="fa arrow"></a>
	                            <ul class="nav nav-second-level">
		                            <li><a href="<%=request.getContextPath() %>/masterdata/awards.jsp">awards</a></li>
		                            <li><a href="<%=request.getContextPath() %>/masterdata/important_institutional_recognitions.jsp">important_institutional_recognitions</a></li>
		                            <li><a href="<%=request.getContextPath() %>/masterdata/Institutional_Recognitions.jsp">Institutional_Recognitions</a></li>
	                            </ul>
                            </li>
                            <li><a href="<%=request.getContextPath() %>/reserchwork.do?action=list">RESEARCH</a></li>
                            <li><a href="#">INNOVATIONS<span class="fa arrow"></a>
	                            <ul class="nav nav-second-level">
		                            <li><a href="<%=request.getContextPath() %>/masterdata/academic_innovations.jsp">Academic_innovations</a></li>
		                            <li><a href="<%=request.getContextPath() %>/masterdata/administrative_practices1.jsp">administrative_practices</a></li>
		                            <li><a href="<%=request.getContextPath() %>/masterdata/established_labs.jsp">established_labs</a></li>
		                            <li><a href="<%=request.getContextPath() %>/masterdata/best_practices_at_BMS1.jsp">best_practices_at_BMS</a></li>
	                            </ul>
                            </li>
                            <li><a href="#">COLLABORATION<span class="fa arrow"></a>
	                            <ul class="nav nav-second-level">
	                            	<li><a href="<%=request.getContextPath() %>/engineeringcollaboration.do?action=list">engineering_organizations</a></li>
		                            <li><a href="<%=request.getContextPath() %>/educational.do?action=list">Educational Organizations</a></li>
		                            <li><a href="<%=request.getContextPath() %>/masterdata/benefits1.jsp">benefits</a></li>
	                            </ul>
                            </li>
                            <li><a href="<%=request.getContextPath() %>/activities.do?action=list">ACTIVITY</a></li>
                            <li><a href="<%=request.getContextPath() %>/masterdata/publications.jsp">PUBLICATIONS</a></li>
                            <li><a href="<%=request.getContextPath() %>/masterdata/contact.jsp">CONTACT ME</a></li>
                            <li><a href="<%=request.getContextPath() %>/masterdata/social-media-links.jsp">SOCIAL MEDIA LINK</a></li>
                        </ul>
                    </li>
                    <li><a href="<%=request.getContextPath() %>/masterdata/changepassword.jsp"><i class="fa fa-dashboard fa-fw"></i>CHANGE PASSWORD</a></li>
                </ol>

            </div>
        </div>
</body>
</html>