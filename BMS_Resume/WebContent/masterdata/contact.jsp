<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Resume-Admin Dashboard</title>

    <!-- Bootstrap Core CSS -->
    <link href="<%=request.getContextPath() %>/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<%=request.getContextPath() %>/css/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="<%=request.getContextPath() %>/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<%=request.getContextPath() %>/css/startmin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<%=request.getContextPath() %>/css/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<%=request.getContextPath() %>/css/font-awesome.min.css" rel="stylesheet" type="text/css">



<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.9.1.js"></script>
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/jquery.validate.min.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/common.validation.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/additional-methods.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/validation/additional-methods1.js"></script>
	
<script>
	$(document).ready(function() {
		
		$('#myform').validate( { // initialize the plugin
					rules : {
						name : {
							required : true,
							letterswithbasicpunc : true
						},
						designationcompany : {
							required : true,
							letterswithbasicpunc : true
						},
						address : {
							required : true,
							letterswithbasicpunc : true
						},
						citypincode : {
							required : true
						},
						emailid : {
							required : true,
							email : true
						}
					},
					messages : {
						name : {
							required : "PLEASE ENTER NAME",
							letterswithbasicpunc : "ENTER ONLY LETTERS WITH BASIC PUNCTIONS"
						},
						designationcompany : {
							required : "PLEASE ENTER DESIGNATION & COMPANY NAME",
							letterswithbasicpunc : "ENTER ONLY LETTERS WITH BASIC PUNCTIONS"
						},
						address : {
							required : "PLEASE ENTER ADDRESS",
							letterswithbasicpunc : "ENTER ONLY LETTERS WITH BASIC PUNCTIONS"
						},
						citypincode : {
							required : "PLEASE ENTER CITY AND PINCODE"
						},
						emailid : {
							required : "PLEASE ENTER EMAIL-ID",
							email : "PLEASE ENTER EMAIL IN VALID FORMATE"
						}
					}
				});
	});
</script>
<script type="text/javascript">
       $(document).ready(function()
    		   {
		   
			   $.ajax({
				   type: "GET",
				   url:"<%=request.getContextPath()%>/contactme.do?action=detailsAppend",
				   async:false,
				   success:function(r){
					    
						var json= JSON.parse(r);
	 					$.each(json,function(i,obj){

	     					var name=obj.name;
	     					$("#name").val(name);
	     					
	     					var designationcompany=obj.designationcompany;
	     					$("#designationcompany").val(designationcompany);
	     					
	     					var address=obj.address;
	     					$("#address").val(address);
	     					
	     					var citypincode=obj.citypincode;
	     					$("#citypincode").val(citypincode);
	     					
	     					var emailid=obj.emailid;
	     					$("#emailid").val(emailid);

	     					if(obj!=null){
	    	 					$("#submit").val("update");
	    	 					$("#myform").attr('action',"contactme.do?action=update");
	         					}
	     					});
     					
				   }


			   });

			   
				   });

    		  
       
       
       </script>



  
</head>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Admin</a>
        </div>

        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        

        <!-- Top Navigation: Right Menu -->
        <ul class="nav navbar-right navbar-top-links">
           <li><a href="<%=request.getContextPath() %>/logout.do?action=AdminLogoutAction"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
        </ul>

        <!-- Sidebar -->
        <jsp:include page="/masterdata/sidebar.jsp"></jsp:include>
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
      <div class="container-fluid">
         <div class="row"><div class="col-lg-12"><h3 class="page-header">Contact Me</h3></div></div>
					 
                      
                      <html:form styleId="myform" action="contactme.do?action=details" method="post">
                      
                      <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                          <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" id="name" name="name">
                          </div>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                      
                      	 <div class="form-group">
                            <label>Designation and Company</label>
                            <input type="text" class="form-control" id="designationcompany" name="designationcompany">
                          </div>
                      
                      </div>  
                       <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                      
                      	 <div class="form-group">
                            <label> Address</label>
                             <textarea class="form-control" rows="3" id="address" name="address"></textarea>
                          </div>
                      
                      </div>  
                      </div>
                      
                       <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                          <div class="form-group">
                            <label> City and Pincode</label>
                            <input type="text" class="form-control" id="citypincode" name="citypincode">
                          </div>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                          <div class="form-group">
                            <label> Email ID</label>
                            <input type="text" class="form-control" id="emailid" name="emailid">
                          </div>
                      </div>
                      
                       
                      </div>
                      
                      <div class="row">
                         <div class="col-lg-12">
                          
                          <input type="submit" class="btn btn-primary" value="save" id="submit">
                           <button type="reset" class="btn btn-primary">Cancel</button>
                           </div>
                      </div>
                      
                    </html:form>
                   
                   
                    
</div>
</div>
</div>

<!-- Bootstrap Core JavaScript -->
<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<%=request.getContextPath()%>/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<%=request.getContextPath()%>/js/startmin.js"></script>
<script src="<%=request.getContextPath()%>/js/image-preview.js"></script>

</body>
</html>
