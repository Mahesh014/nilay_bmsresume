 
function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null;};

$(document).ready(function () {
//alert("sfdsf");
	debugger;
	
	$(function() {
		$.validator.addMethod("greaterThan", 
			    			function(value, element, params) {

			    			    if (!/Invalid|NaN/.test(new Date(value))) {
			    			        return new Date(value) > new Date($(params).val());
			    			    }

			    			    return isNaN(value) && isNaN($(params).val()) 
			    			        || (Number(value) > Number($(params).val())); 
		});
		});
	$("#priceForm").validate({
    	rules: {
		priceName: {
		required: true
		//integer:true
		//lettersanddigitswithbasicpunc:true
		},
		fromPrice:{
			
			required: true,
			integer:true
			//maxlength:true
		},
		toPrice:{
			
			required: true,
			integer:true,
			greaterThan:"#fromPrice"
			//maxlength:true
		}
		
			
    	  },
    	  messages: {
    		  priceName: { 
    		  required: "Price required!" ,
    		  integer: "Please Enter digits",
    		  lettersanddigitswithbasicpunc:"Please Enter Only letters,numbers,special characters"
    			  },
    			  fromPrice:{
    					
    					required:"From price required!",
    					integer:"Please enter only numbers"
    					
    				},
    				toPrice:{
    					
    					required:"To price required!",
    					integer:"Please enter only numbers",
    						greaterThan:"To Price Should not be lesser than From Price."
    				}
	      }
	 });
		//****************************************RoleMaster ****************************************************
		
		$("#role-form")
		.validate(
				{

					rules : {
						rolename : {
							required : true,
							minlength : 3,
							lettersWithSpaceonly : true
						}
					},
					messages : {
						rolename : {
							required : "RoleName required!",
							minlength : "Your RoleName must be at least 3 characters    long",
							lettersWithSpaceonly : "Please Enter only alphabets "

						}
					},
					submitHandler : function(form) {
						form.submit();
					}
				});
		$("#fuelForm").validate({
	    	  rules: {
			fuelName: {
			required: true,
			lettersWithSpaceonly:true
			}
			
				
	    	  },
	    	  messages: {
	    		  fuelName: { 
	    		  required: "Fuel Name required!"  ,
	    		  lettersWithSpaceonly: "Please Enter Alphabets"
	    			  }
		      }
		 });
		
		$("#kilometerForm").validate({
	    	  rules: {
			kilometerName: {
			required: true
			//lettersWithSpaceonly:true
			}
			
				
	    	  },
	    	  messages: {
	    		  kilometerName: { 
	    		  required: "Kilometer required!" , 
	    		  lettersWithSpaceonly: "Please Enter Alphabets"
	    			  }
		      }
		 });
	    $("#transmissionForm").validate( {
			rules : {
				
	    	transmissionName : {
					required : true,					
					lettersonly : true,
					minlength : 2
				}
			},
			messages : {
				
				transmissionName : {
					required : "Transmission name required!",
					lettersonly : "Enter only characters",
					minlength : "Transmission name at least 2 character"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});
	    $("#bodyTypeForm").validate( {
			rules : {
				
	    	bodyTypeName : {
					required : true,
					//letterswithspecialchracters : true,
					minlength : 2
				}
			},
			messages : {
				
				bodyTypeName : {
				
				
					required : "Body type required!",
					//letterswithspecialchracters : "Enter only letters and Special characters",
					//lettersonly : "Enter only characters",
					minlength : "Body type at least 2 character"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});
	    $("#colorForm").validate( {
			rules : {
				
	    	colorName : {
					required : true,
					lettersonly : true,
					minlength : 2
				}
			},
			messages : {
				
				colorName : {
					required : "Color name required!",
					lettersonly : "Enter only characters",
					minlength : "Color name at least 2 character"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});
	    $("#numberForm").validate( {
			rules : {
				
	    	number : {
					required : true
				}
			},
			messages : {
				
				number : {
					required : "Number Of Owners required!"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});
		
		$("#ageForm").validate({
	    	  rules: {
			age: {
			required: true
			// integer:true
			}
			
				
	    	  },
	    	  messages: {
	    		  age: { 
	    		  required: "Age required!" ,
	    		 integer: "Please Enter digits"
	    			  }
		      }
		 });
		
		
		
		
		$("#technologyForm").validate( {
			rules : {
				
			technologyName : {
					required : true
					
				}
			},
			messages : {
				
				technologyName : {
					required : "Technology Name required!"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});
		
	    $("#jobchangeForm").validate( {
			rules : {
				
	    	reasonJobChange : {
					required : true,
					lettersonly : true
				}
			},
			messages : {
				
				reasonJobChange : {
					required : "Reasong for job change required!",
					lettersonly : "Enter only characters"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});
	    $("#skillForm").validate( {
			rules : {
				
	    	technologyID : {
					required : true
					
				},
				technologySkills : {
					required : true,
					lettersonly : true
				}
			},
			messages : {
				technologyID:
				{
				required:"Technology required !"
				},
				
				technologySkills : {
					required : "Technology Skill Name required!",
					lettersonly : "Enter only characters"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});
 
	    /*-------------------------Email complete validation Start-----------------------------------*/
	    jQuery.validator.addMethod("checkemailnew", function(value, element){
	    	return this.optional(element) ||  /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/i.test(value);
	    },"Invalid email address.");
	    /*-------------------------Email complete validation End-----------------------------------*/
		$("#userForm").validate( {
			rules : {
				firstname : {
					required : true,
					lettersonly : true,
					maxlength : 25
				},
				
				username : {
					required : true,
					minlength : 3,
					maxlength : 50
				},
				emailid  : {
					required : true,
					checkemailnew : true
				},
				mobileno:{
					required : true,
					integer : true
				},
				password : {
					required : true,
					minlength : 3,
					maxlength : 20
				},
				reenterpassword : {
					required : true,
					minlength : 3,
					maxlength : 20,
					equalTo: "#password"
				},
				roleid:{
					required :true
					
					
				},
				
				agree : "required"
			},
			messages : {
				firstname : {
					required : "First Name required!",
					lettersonly : "Enter only characters"
				},
				roleid : {
					required : "Select Role!",
					min : "Select Role!"
				},
				emailid  : {
					required :"Email Id is required!",
					checkemailnew : "Please Enter Valid email Id"
				},
				mobileno:{
					required : "Mobile Number required!",
					integer : "Please enter numbers"
				},
				username : {
					required : "UserName required!",
					minlength : "3 Letters Minimum",
					maxlength : "50 Letters Maximum"
				},
				password : {
					required : "Password is required!",
					minlength : "3 Letters Minimum",
					maxlength : "20 Letters Maximum"
				},
				reenterpassword : {
					required : "Re-Enter Password is required!",
					minlength : "3 Letter Minimum",
					maxlength : "20 Letter Maximum",
					equalTo:"Please Re-Enter Password as Same as Password "
				},
				roleid:{
					required :"Please Select Role "
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});

	

		$("#cityForm").validate( {
			rules : {
				stateid :{
				required :true
				},
				cityname : {
					required : true,
					lettersonly : true
				},
				agree : "required"
			},
			messages : {
				stateid :{
				required :"Please Select State Name"
				},
				
				cityname : {
					required : "City Name required!",
					lettersonly : "Enter only characters"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});

		$("#areaForm").validate( {
			rules : {
				
			cityid : {
					required : true
					
				},
				areaname : {
					required : true,
					lettersonly : true
				},
				agree : "required"
			},
			messages : {
				cityid:
				{
				required:"Please Select City Name"
				},
				
				areaname : {
					required : "Area Name required!",
					lettersonly : "Enter only characters"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});
		
//********************************************Brand Master Form*****************************************************
		
		
		
		$("#brand-form").validate({
			
			rules:{
			brandName:{
			required: true,
			lettersWithSpaceonly:true,
			minlength : 3
			//maxlength : 25
		},
		brandLogo:{
			extension: "png|jpe?g|gif"
		}
		},
			messages:{
			brandName:{
			required: "Brand name required",
			lettersWithSpaceonly:"Please enter alphabets",
			minlength : "minimum length is 3"
			//maxlength : 25
		},
		brandLogo:{
			extension: "png|jpe?g|gif"
		}
		}
			
		});
		
		
		$("#country-form").validate( {
			rules : {
				countryname : {
					required : true,
					lettersWithSpaceonly : true

				}
			},
			messages : {
				countryname : {
					required : "Country Name required!",
					lettersWithSpaceonly : "Enter only alphabets"

				}
			}
		});
		
		$("#state-form").validate( {
			rules : {
				countryid:{
					required : true
				},
				statename : {
					required : true,
					lettersWithSpaceonly : true

				}

			},
			messages : {	
				countryid:{
					required :"Please Select Country Name"
					},
				statename : {
					required : "State Name required!",
					lettersWithSpaceonly : "Enter Only Characters"
				}
			}
		});

		$("#langForm").validate( {
			rules : {
				
			languageName : {
					required : true,
					lettersonly : true
				},
				agree : "required"
			},
			messages : {
				
				languageName : {
					required : "Language Name required!",
					lettersonly : "Enter only characters"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});
		$("#desForm").validate( {
			rules : {
				
			designationName : {
					required : true,
					lettersonly : true
				},
				agree : "required"
			},
			messages : {
				
				designationName : {
					required : "Designation Name required!",
					lettersonly : "Enter only characters"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});
		$("#eduForm").validate( {
			rules : {
				
			educationname : {
					required : true,
					lettersonly : true
				},
				agree : "required"
			},
			messages : {
				
				
				
				educationname : {
					required : "Education Name required!",
					lettersonly : "Enter only characters"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});
		$("#signUpForm")
		.validate(
				{
					rules : {

					name : {
							required : true,
							minlength : 2,
							lettersonly : true
						},
						email : {
							required : true,
							email : true
						},
						password : {
							required : true,
							minlength : 5
						},
						reenterpassword : {
							required : true,
							minlength : 5,
							equalTo : "#password"
						},
						
						mobileNumber : {
							required : true,
							minlength : 10,
							digits : true
						},
						statename : {
							required : true
							},
							cityname : {
								required : true
								},
							address : {
							required : true,
							maxlength : 500

						},
						postalCode : {
							required : true,
							digits : true
						},
						

						agree : "required"
					},
					messages : {
						name : {
							required : "provide a FirstName",
							minlength : "Your FirstName must be atleast 2 characters long",
							lettersonly : "Enter only alphabets "

						},
						email :
						{
						required:"provide a email address",
						email : "Enter a valid email address"
					},
					password : {
						required : "provide a password",
						minlength : "Your password must be atleast 5 characters long"

					},
					reenterpassword : {
						required : "provide a re-enter password",
						minlength : "Your re-enter password must be atleast 5 characters long",
						equalTo : "re-enter the same password"
					},

					mobileNumber : {
						required : "provide a Mobile Number",
						minlength : "Your Mobile Number must be atleast 10 numbers long",
						digits : "enter only Numbers"
					},
					statename:
					{
						required : "select your state"
					},
					cityname:
					{
						required : "select your state"
					},
					address : {
						required : "provide a address"
					},
						
					postalCode : {
							required : "provide a postal code",
							digits : "Enter only Numbers"
						},

					submitHandler : function(form) {
						form.submit();
					}
					}
				});
		
		$("#eventForm").validate( {
			rules : {
				
			eventName : {
					required : true,
					lettersonly : true
				},
				agree : "required"
			},
			messages : {
				
				
				
				eventName : {
					required : "Event category name required!",
					lettersonly : "Enter only characters"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});
		$.validator.addMethod("greaterThan", 
				function(value, element, params) {

				    if (!/Invalid|NaN/.test(new Date(value))) {
				        return new Date(value) > new Date($(params).val());
				    }

				    return isNaN(value) && isNaN($(params).val()) 
				        || (Number(value) > Number($(params).val())); 
		});
		


		$("#domainForm").validate( {
			rules : {
				
			domainName : {
					required : true,
					lettersonly : true
				},
				agree : "required"
			},
			messages : {
				
				
				
				domainName : {
					required : "Domain Name required!",
					lettersonly : "Enter only characters"
				}
			},
			errorPlacement : function(error, element) {
				var err = $("<br/>").add(error);
				$(element).after(err);
			}
		});
		
		$(document).ready(function(){

		    jQuery.validator.addMethod("lettersWithSpaceonly", function(value, element) {
		    	return this.optional(element) || /^[A-Za-z\s]+$/i.test(value);
		    }, "Letters only please"); 

		    $.validator.addMethod("integer", function(value, element) {
		    	return this.optional(element) || /^-?\d+$/.test(value);
		    }, "A positive or negative non-decimal number please");

		    $("#company-form").validate( {
				rules : {
					
		    	companyname : {
						required : true,
						lettersWithSpaceonly:true
						
					},
					cityid : {
						required : true
						
					}
				/*	areaid : {
						required : true
						
					},
					contactNo1 : {
						required : true,
						integer:true,
						minlength:10,
						maxlength:12
						
					},
					contactNo2 : {
						required : true,
						integer:true,
						minlength:10,
						maxlength:12
					},
					emailId1 : {
						required : true,
						email:true
						
					},
					emailId2 : {
						required : true,
						email:true
						
					},
					contactperson1 : {
						required : true,
						lettersWithSpaceonly:true
						
					},
					contactperson2 : {
						required : true,
						lettersWithSpaceonly:true
						
					}*/
					
				},
				
				messages : {
					companyname:
					{
					required:"Please Enter Company Name",
					lettersWithSpaceonly:"Please Enter Only Alphabets"
					},
					
					cityid : {
						required : "Please Select City Name"
						
					},
					areaid : {
						required : "Please Select Area Name"
						
					},
					contactNo1 : {
						required : "Please Enter Contact Number1",
						integer:"Please Enter Only digits"
						
						
					},
					contactNo2 : {
						required : "Please Enter Contact Number2",
						integer:"Please Enter Only digits"
						
						
					},
					emailId1 : {
						required : "Please Enter Email Id1",
						email:"Please Enter Valid Email Id"
						
					},
					emailId2 : {
						required : "Please Enter Email Id2",
						email:"Please Enter Valid Email Id"
						
					},
					contactperson1 : {
						required : "Please Enter Contact Person Name1",
						lettersWithSpaceonly:"Please Enter Only Alphabets"
						
					},
					contactperson2 : {
						required : "Please Enter Contact Person Name2",
						lettersWithSpaceonly:"Please Enter Only Alphabets"
						
					}
				}
		        });
		    
		    
		  //****************************************Add Cars Form************************************
			
			$("#manufactyearForm").validate( {
				rules : {
					
				manufacturingYear : {
						required : true,
						digits : true,
						minlength:4,
						maxlength:4
					},
					agree : "required"
				},
				messages : {
					
					
					
					manufacturingYear : {
						required : "Manufacturing Year required!",
						digits: "Enter only digits",
						minlength:"Year must be a 4 digits",
						maxlength:"Year should not have more than four digits"
					}
				},
				errorPlacement : function(error, element) {
					var err = $("<br/>").add(error);
					$(element).after(err);
				}
			});
			$("#makeForm").validate( {
				rules : {
					
				makeName : {
						required : true,
						lettersonly : true
					},
					agree : "required"
				},
				messages : {
					
					
					
					makeName : {
						required : "Make Name required!",
						lettersonly : "Enter only characters"
					}
				},
				errorPlacement : function(error, element) {
					var err = $("<br/>").add(error);
					$(element).after(err);
				}
			});
			$("#modelForm").validate( {
				rules : {
				brandId : {
				required : true
				
			},
			
					
			modelName : {
						required : true,
						lettersonly : true
					},
					agree : "required"
				},
				messages : {
					brandId : {
					required : "Select  Brand Name"
					
				},
					
					
				modelName : {
						required : "Model Name required!",
						lettersonly : "Enter only characters"
					}
				},
				errorPlacement : function(error, element) {
					var err = $("<br/>").add(error);
					$(element).after(err);
				}
			});
			$("#versionForm").validate( {
			rules : {
				brandId : {
				required : true
				
			},
			modelid : {
				required : true
				
			},
			versionname : {
						required : true,
						letterswithnuos : true
					}
					
				},
				messages : {
					brandId : {
					required : "Select  Brand"
					
				},
				modelid : {
					required : "Select  model name"
					
				},
					
					
				versionname : {
						required : "Version  required!",
						letterswithnuos : "Please Enter only alphabets and digits "
					}
				},
				errorPlacement : function(error, element) {
					var err = $("<br/>").add(error);
					$(element).after(err);
				}
			});
			
			$("#brandmaster").validate( {
				rules : {
				brandName : {
				required : true
				
			},
			modelName : {
				required : true
				
			}
			},
			messages : {
				brandName : {
					required : "select  a brand Name"
					
				},
				
					modelName : {
						required : "select a model name"
						
					}
				},
				errorPlacement : function(error, element) {
					var err = $("<br/>").add(error);
					$(element).after(err);
				}
			});

			
			//****************alert(*****************Edit Profile Form***********************************
			
			$("#userprof-form").validate({
			rules:{
				username:{
				required: true,
				minlength : 3
				},
				emailid:{
				required: true,
				checkemailnew:true
				},
				
				firstname:{
					required: true,
					lettersWithSpaceonly:true,
					minlength : 3
					},
				lastname:{required: true,
						lettersWithSpaceonly:true,
						minlength : 3
						},
				mobileno:{required: true,
					 digits:true,
					 minlength:10,
					 maxlength:12
					 
						},
				password:{
							required: true
						},
				oldpassword:{
							required: true
				
				},
				confirmpassword:{
					required: true,
					equalTo : "#oldPassword"
						}
			},
				messages:{
				username:{
				required: "Username required!",
				minlength : "minimum length is 3"
			},
				emailid:{
				required: "Email Id required",
				checkemailnew:"Please enter valid email Id"
				},
				
				firstname:{
					required: "First name required",
					lettersWithSpaceonly:"Please Enter alphabets",
					minlength : "minimum length is 3"
					},
				lastname:{
						required: "Last name required",
						lettersWithSpaceonly:"Please Enter alphabets",
						minlength : "minimum length is 3"
						},
				mobileno:{
					 required: "Mobile number required",
					 integer : "Please Enter digits",
					 minlength:"minimum length is 10",
					 maxlength:"maximum length is 12"
					 
						},
						password :{
							required: "Password is required"
						},
						oldpassword :{
							required: "New Password is required"
				},
				confirmpassword:{
					required:"Confirm Password is required",
					
					equalTo:"Password do not match.."
				}
			}
				
			});
			
			//*********************************Feedback Form***********************************
			
			$("#feedbackForm").validate({
				rules:{
					firstname:{
					required: true,
					lettersWithSpaceonly:true
					},
					
					lastname :{
					required: true,
					lettersWithSpaceonly:true

					},
					
					email :{
					required: true,
					checkemailnew:true

					},
					comments:{
					required: true
					}
				},
				messages:{
					firstname:{
					required: "Firstname required!",
					lettersWithSpaceonly:"Please Enter only Alphabets"
					},
					
					lastname :{
						required: "Lastname required!",
						lettersWithSpaceonly:"Please Enter only Alphabets"

					},
					
					email :{
					required: "Email required!",
					checkemailnew:"Please Enter valid email Id"

					},
					comments:{
					required: "Comments required!"
					}
				}
			
				
				
			});
			
			//*********************************Mycar Form***********************************
			//alert("dfdf");
			$("#mycarForm").validate({
				rules:{
				username:{
					required: true,
					lettersWithSpaceonly:true
					},
					
					mobileno :{
					required: true,
					integer:true,
					minlength:10,
					maxlength:12

					},
					
					emailid :{
					required: true,
					checkemailnew:true

					}
				},
				messages:{
					username:{
					required: "Username required!",
					lettersWithSpaceonly:"Please Enter only Alphabets"
					},
					
					mobileno :{
					required: "Mobile No required!",
					integer:"Please Enter digits",
					minlength:"10 is minimum length",
					maxlength:"12 is minimum length"

					},
					
					emailid :{
					required:"Email Id  is required!",
					checkemailnew:"Please Enter valid Email Id"

					}
				}
			
				
				
			});
			
			//***********************************rEGISTRATION fORM**************************************
			$("#reg-form").validate({
				
				rules:{
				firstname:{
				required: true,
				lettersWithSpaceonly:true,
				minlength : 3,
				maxlength : 25
			},
			username:{
				required: true
				
			},
			password:{
				required: true,
				minlength : 4,
				maxlength : 10,
				equalTo : "#pass"
			},
			pswd:{
				required: true,
				
				minlength : 4,
				maxlength : 10
				
			},
			month:{
				required: true
			},
			day:{
				required: true,
				minlength : 1,
				maxlength : 2,
				digits:true
			},
			year:{required: true,
				minlength : 2,
				maxlength : 4,
				digits:true
			},
			mobileno:{
				required: true,
				digits:true,
				minlength : 10,
				maxlength : 12
				
			},
			roleid:{
				required: true
			},
			emailid:{
				required: true,
				checkemailnew:true
			}
			},
			messages:{
				firstname:{
				required: "Please Enter First name",
				lettersWithSpaceonly:"Please Enter Letters only",
				minlength : "First name atleast 3 characters long",
				maxlength : "First name should not cross 25 characters "
			},
			username:{
				required: "Please Enter User name"
				
			},
			password:{
				required: "Please Enter Password",
				minlength : "Password contains minimum 4 characters",
				maxlength : "Password should not cross more than 10 characters",
				equalTo : "Password do not match"
			},
			pswd:{
				required: "Please Enter Password",
				minlength : "Password contains minimum 4 characters",
				maxlength : "Password should not cross more than 10 characters"
				
			},
			month:{
				required: "Please Select a month"
			},
			day:{
				required: "Please enter date",
				minlength : "Date should be atleast one digit",
				maxlength : "Date Should not be more than two digits",
				digits:"Please Numbers only"
			},
			year:{required: "Please enter year",
				minlength : "year should be atleast two digit",
				maxlength : "year Should not be more four two digits",
				digits:"Please Numbers only"
			},
			mobileno:{
				required: "Please enter mobile no",
				digits:"Please Numbers only",
				minlength : "mobile no should be 10 digits long ",
				maxlength : "mobile no should not more than 12 digits"
				
			},
			roleid:{
				required: "Please select role name"
			},
			emailid:{
				required: "Email Id required!",
				checkemailnew:"Please enter valid email Id"
			}
			},
			errorPlacement: function(error, element1) 
	        {
	            if ( element1.is(":radio") ) 
	            {
	                error.appendTo( element1.parents('.container') );
	            }
	            else 
	            { // This is the default behavior 
	                error.insertAfter( element1 );
	            }
	            
	            
	           
	         }
			
			
			
			});
			
			
			
			$("#budgetForm").validate({
				
				rules:{
				budget:{
				required: true
				
			}
			},
			messages:{
				budget:{
				required: "Budget required!"
				
			}
			}
			});
			
			
			

			
//*********************************Features Types Form***********************************
			$.validator.addMethod("letterswithspacesandspecialchracters", function (value, element) {
				  return this.optional(element) || /^[a-z-+&*.,()'\"\s]+$/i.test(value);
				}, "leters With Space & Special Characters"); 
			
			$("#featuresForm").validate({
				rules:{
				features:{
					required: true,
					letterswithspacesandspecialchracters:true
					}
				},
				messages:{
					features:{
					required: "Features required!",
					letterswithspacesandspecialchracters:"Please Enter only Alphabets"
					}
				}
			
				
				
			});
			
			
			
			//**********************************Utility Master Form************************
			$("#utilityform").validate({
				
				rules:{
				utilityname:{
				required: true,
				minlength:3,
				lettersWithSpaceonly:true
			}
			},
			messages:{
				utilityname:{
				required: "Please Enter Utility name",
				minlength:"Utilit Name shobld be atleast 3 characters long",
				lettersWithSpaceonly:"Please letters  only"
			}
			}
			});
			//********video-form***********************************************************
			$("#video-form").validate({
				rules:{
				modelName:{
				required: true
			},
			brandName:{
				required: true
			},
			versionname:{
				required: true
			},
			videoname:{
				required: true,
				lettersWithSpaceonly:true,
				minlength:5
			},
			videofile:{
				required: true,
				extension: "mp4|wmv"
			}
			},
			messages:{
				modelName:{
				required: "Please Select a Model"
			},
			brandName:{
				required: "Please Select a Brand"
			},
			versionname:{
				required: "Please Select a Version"
			},
			videoname:{
				required: "Plesase Enter Video name",
				lettersWithSpaceonly:"Please Enter letters only",
				minlength:"Please Enter Videoname must be 3 characters length"
			},
			videofile:{
				required: "Please Select a Video file",
				extension: "Upload Only mp4 & wmv Files" 
			}
			}
			});









	//*****************************************************Sub Features Form***************************************************
		$("#FeatureSubTypesForm").validate({
			
		rules:{
		featuresId:{
			required:true
		},
		sfname:{
			required:true,
			letterswithspacesandspecialchracters:true
		}
		},
			messages:{
			required:"Please Select a Future type"
				
		},
		sfname:{
			required:"Please Enter Sub Feature Name",
			letterswithspacesandspecialchracters:""
		}
			
		});




		
		//****************************************************onroadprice-form***********************
		
		
		$("#onroadprice-form").validate({
			
			rules:{
			stateid:{
			required:true
		},
		carname:{
			required:true,
			letterswithnuos:true
		},
		exshowroomprice:{
			required:true,
			digits:true
		},
		rto:{
			required:true,
			digits:true
		},
		insurance:{
			required:true,
			digits:true
		},
		brandName:{
			required:true
		}
		},
		messages:{
			stateid:{
			required:"Please Select a State"
		},
		carname:{
			required:"Please Enter car name",
			letterswithnuos:"Please enter only numbers and letters"
		},
		brandName:{
			required:"Please Select a Brandname "
		},
		exshowroomprice:{
			required:"Please Enter exshowroom price"
		},
		rto:{
			required:"Please enter rto price"
		},
		insurance:{
			required:"Please enter insurance price"
		}
		}
			
		});



//******************************************************Dealers Form*************************
		$("#dealer-form").validate({
			
		rules:{
			stateid:{
			required:true
		},
		cityid :{
			required:true
			
		},
		dealerName:{
			required:true,
			lettersWithSpaceonly:true
		},
		mobile:{
			required:true,
			digits:true,
			minlength:10,
			maxlength:12
		},
		emailid:{
			required:true,
			checkemailnew:true
		},
		address:{
			required:true
		},
//		altitude:{
//			required:true,
//			letterswithnumbersforaltitude:true
//		},
//		longitude:{
//			required:true,
//			letterswithnumbersforaltitude:true
//		},
		brandName:{
			required:true
		}
		},
		messages:{
			stateid:{
			required:"Please select a state name",
			lettersWithSpaceonly:"Please Enter letters and space only"
		},
		cityid :{
			required:"Please select a city name"
			
		},
		dealerName:{
			required:"Please enter dealer name",
			lettersWithSpaceonly:"Please enter only letters"
		},
		mobile:{
			required:"Please enter mobile no",
			digits:"Please enter digits only",
			minlength:"Mobile no must be 10 digits logn",
			maxlength:"Mobile no must not be more than 12 digits"
		},
		emailid:{
			required:"Please enter emailid",
			checkemailnew:"Please enter valid emailid"
		},
		address:{
			required:"Please enter address"
		},
		brandName:{
			required:"Please Select a Brandname "
		}
//		altitude:{
//			required:"Please enter altitude",
//			letterswithnumbersforaltitude:"Please enter valid altitude"
//		},
//		longitude:{
//			required:"Please enter longitude",
//			letterswithnumbersforaltitude:"Please enter valid longitude"
//		}
		}
			
		});















//*********************************carnews-form*************************************
			$("#carnews-form").validate({
				rules:{
				modelName:{
				required:true
			},
			brandName:{
				required:true
			},
			newsname:{
				required:true,
				letterswithnumbersforaltitude:true,
				minlength:3
			},
			carimgae:{
				extension:"png|jpe?g|gif"
			}
			},
				messages:{
				modelName:{
				required:"Please Select a Model Name"
			},
			brandName:{
				required:"Please Select a Brand Name"
			},
			newsname:{
				required:"Please Enter news name",
				letterswithnumbersforaltitude:"Please enter letters and numbers only",
				minlength:"Please newsname must be 3 characters long"
			},
			carimgae:{
				extension:"Invalid format images"
			}
			}
			});



	
			//*********************************fuel-form********************************************
			$("#fuel-form").validate({
				
				
				rules:{
				modelName:{
				required:true
			},
			brandName:{
				required:true
			},
			description:{
				required:true,
				lettersWithSpaceonly:true,
				minlength:3
			},
			fuelName:{
				required:true
			},
			kmpl:{
				required:true,
				digits:true
			}
			},
			messages:{
				modelName:{
				required:"Please Select a Model Name"
			},
			brandName:{
				required:"Please Select a Brand Name"
			},
			description:{
				required:"Please Enter news name",
				lettersWithSpaceonly:"Please enter letters  only",
				minlength:"Please newsname must be 3 characters long"
			},
			fuelName:{
				required:"Please Select a Fuel Type"
			},
			kmpl:{
				required:"Please Enter Kilometers",
				digits:"Digits Only"
			}
			}
			});
		
			
			
//*********************************Specifications Sub Types Form***********************************
			
			
			$("#specificationsSubTypesForm").validate({
				rules:{
				specificationId:{
					required: true
					
					},
					
					specificationsSubTypes :{
						required: true,
						letterswithspacesandspecialchracters:true

					}
				},
				messages:{
					specificationId:{
					required: "Please Select Specifications"
					
					},
					
					specificationsSubTypes :{
						required: "Specifications Sub Types required!",
						letterswithspacesandspecialchracters:"Please Enter only Alphabets"

					}
				}
			
				
				
			});
			
//*********************************Specifications Sub Types Form***********************************
			
			
			$("#subVersionForm").validate({
				rules:{
				brandId:{
					 required: true
					
					},
					
					modelId :{
						required: true
						
					},
					versionId :{
						required: true

					},
					subVersionName :{
						required: true
						

					}
				},
				messages:{
					brandId:{
					 required: "Please select Brand"
					
					},
					
					modelId :{
						required: "Please select model name"
						
					},
					versionId :{
						required: "Please select version "

					},
					subVersionName :{
						required: "Subversion  required!"
						

					}
				}
			
				
				
			});
			
						
			
		    
			
			
			

			
		   
		});
		
		
});