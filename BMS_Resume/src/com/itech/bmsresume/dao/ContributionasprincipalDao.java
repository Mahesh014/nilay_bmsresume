package com.itech.bmsresume.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.ContributionasprincipalActionFrom;

public class ContributionasprincipalDao {

	public Object add(ContributionasprincipalActionFrom details, String title,
			DataSource dataSource, String userid, String str) {
		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;
		
		try {
			System.out.println("inside contribution as principal dao");
			con = dataSource.getConnection();
			//String qry = "insert into userdetails (awardyear,award) values(?,?) where email=?";
			String qry = "insert into contribution_as_principal (title,contributionasprincipal,userid,active) values(?,?,?,?)";
			ps = con.prepareStatement(qry);
			// ps.setString(1, formdetails.getFname());
			ps.setString(1, title);
			ps.setString(2, str);
			ps.setString(3, userid);
			ps.setBoolean(4, true);

			update = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if (update > 0) {
			return "success";

		} else {
			return "failure";
		}

	}

	public Object update(ContributionasprincipalActionFrom details, DataSource dataSource, String userid, String str, int id, String titleVal) {
		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;
		
		try {
			System.out.println("inside contribution as principal dao");
			con = dataSource.getConnection();
			//String qry = "insert into userdetails (awardyear,award) values(?,?) where email=?";
			String qry = "update contribution_as_principal set contributionasprincipal=?,title=?,userid=? where id=?";
			ps = con.prepareStatement(qry);
			// ps.setString(1, formdetails.getFname());
			System.out.println(str+"    "+titleVal);
			
			ps.setString(1, str);
			ps.setString(2, titleVal);
			ps.setString(3, userid);
			ps.setInt(4, id);
			
			System.out.println("IDDDDDDDDDDDDDD"+id);

			System.out.println("queryyyyyyy"+qry);
			update = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if (update > 0) {
			return "success";

		} else {
			return "failure";
		}

	}

	public List<ContributionasprincipalActionFrom> list(DataSource dataSource,
			String userid) throws SQLException {
		List<ContributionasprincipalActionFrom>  list= new ArrayList<ContributionasprincipalActionFrom>();
		ContributionasprincipalActionFrom form=null;
		java.sql.Connection con=null;
		java.sql.PreparedStatement pstmt=null;
		ResultSet rs=null;
		
		
		try {
			con= dataSource.getConnection();
			String qry="select title,contributionasprincipal,userid,active,id from contribution_as_principal";
			pstmt=con.prepareStatement(qry);			
			rs=pstmt.executeQuery();	
			while(rs.next()){
				form= new ContributionasprincipalActionFrom();
				form.setTitle(rs.getString("title"));
				form.setContributionasprincipal(rs.getString("contributionasprincipal"));
				form.setUserid(rs.getString("userid"));
				form.setActive(rs.getBoolean("active"));
				form.setId(rs.getInt("id"));
			    list.add(form);
			    
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		finally
		{
			if(con!=null)
			con.close();
			if(pstmt!=null)
			pstmt.close();
			if(rs!=null)
			rs.close();	
		}
		return list;
	
	}
	
	public Object changeStatus(ContributionasprincipalActionFrom details,
			DataSource dataSource) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try	{
			con=dataSource.getConnection();
			String sql="update contribution_as_principal set active = ? where userid=? and id=?";
			pstmt=con.prepareStatement(sql);
			pstmt.setBoolean(1, details.isActive());			
			pstmt.setString(2, details.getUserid());
			pstmt.setInt(3, details.getId());
			
			
			
			pstmt.executeUpdate();
			System.out.println("++++++++++++"+details.isActive());
			System.out.println("++++++++++++"+details.getUserid());

		}
		catch (Exception e){
			System.out.println(e);
		} 
		return null;
	}



}


