package com.itech.bmsresume.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.bmsresume.actionform.BannerActionFrom;

public class BannerDao {

	public Object details(BannerActionFrom details, DataSource dataSource,
			String filename1, String filepath, String userid) {


		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;
		try {
			System.out.println("in banner dao");
			con = dataSource.getConnection();
			//String qry = "insert into userdetails(name,photo,designation,worksat) values(?,?,?,?) where email=?";
			String qry = "update userdetails set name=?,photo=?,designation=?,worksat=? where email=?";
			ps = con.prepareStatement(qry);
			System.out.println(details.getName()+".....");
			System.out.println(details.getDesignation());
			System.out.println(details.getWorksat());
			System.out.println(filename1);
			ps.setString(1, details.getName());
			ps.setString(2, filename1);
			ps.setString(3, details.getDesignation());
			ps.setString(4, details.getWorksat());
			ps.setString(5, userid);
			update = ps.executeUpdate();			
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		if(update>0)
		{
			return "success";
		}
		else
		{
			return "failure";
		}
	}

	
	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
        JSONObject obj=null;
        try {
        	System.out.println("inside banner dao");
			con=dataSource.getConnection();      
			String sql="select name,photo,designation,worksat from userdetails where email=?";
			pstmt=con.prepareStatement(sql);
			//System.out.println(userid);
			pstmt.setString(1,userid);
            //System.out.println("sql for data append is......."+sql);
            
			
			rs=pstmt.executeQuery();	
			while(rs.next())
			{
				obj=new JSONObject();
				//System.out.println("summmmmmmmmmmmmmmm"+rs.getString("subtotal"));

			obj.put("name",rs.getString("name"));
			obj.put("designation",rs.getString("designation"));
			obj.put("worksat",rs.getString("worksat"));
			obj.put("photo",rs.getString("photo"));
			System.out.println("photo is cominnnnnnng"+rs.getString("photo"));
			
			
			jArray.add(obj);
			
		} }catch (Exception e) {
			e.printStackTrace();
		}finally{
			try{
				rs.close();
				pstmt.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return jArray;

	}

		
}
