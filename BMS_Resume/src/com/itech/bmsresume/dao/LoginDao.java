package com.itech.bmsresume.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.RegisterActionForm;

public class LoginDao {

	public List<RegisterActionForm> login(RegisterActionForm formdetails, DataSource dataSource) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean update;
		ArrayList<RegisterActionForm> loginCredentials = new ArrayList<RegisterActionForm>();
		System.out.println("inside login dao");

		try {
			con = dataSource.getConnection();
			String qry = "select email, password from userdetails where email=? and password=?";
			System.out.println(qry);
			ps = con.prepareStatement(qry);
			System.out.println("formdetails.getEmailid()............."+formdetails.getEmail());
			System.out.println("formdetails.getNewpassword().............."+formdetails.getPassword());
			ps.setString(1, formdetails.getEmail());
			ps.setString(2, formdetails.getPassword());
			rs= ps.executeQuery();
			System.out.println(rs.wasNull());
			update = rs.next();
			System.out.println(update);
			if(update){
				formdetails = new RegisterActionForm();
				formdetails.setEmailid(rs.getString("email"));
				formdetails.setNewpassword(rs.getString("password"));
				System.out.println(rs.getString("email")+" "+rs.getString("password"));
				loginCredentials.add(formdetails);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return loginCredentials;
	}

	public String changepassword(
			RegisterActionForm formdetails, DataSource dataSource) {


		Connection con = null;
		PreparedStatement ps = null;
		int update;
		String returnvalue= null;
		System.out.println("inside LoginDao changepassword");

		try {
			con = dataSource.getConnection();
			//String qry = "select email, password from userdetails where email=? and password=?";
			String qry = "update userdetails set password=? where password=?";
			ps = con.prepareStatement(qry);
			System.out.println(formdetails.getPassword()+"............."+formdetails.getNewpassword());
			ps.setString(1, formdetails.getNewpassword());
			ps.setString(2, formdetails.getPassword());
			System.out.println(qry);
			update=ps.executeUpdate();
			System.out.println(update+"update value");
			if(update==1){
				returnvalue = "changedpassword";
				
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return returnvalue;
	}

}
