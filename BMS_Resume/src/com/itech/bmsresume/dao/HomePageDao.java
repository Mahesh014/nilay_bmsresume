package com.itech.bmsresume.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.bmsresume.actionform.HomePageActionForm;

public class HomePageDao {

	public List<HomePageActionForm> list(DataSource dataSource) throws SQLException {


		
		List<HomePageActionForm>  list= new ArrayList<HomePageActionForm>();
		HomePageActionForm form=null;
		java.sql.Connection con=null;
		java.sql.PreparedStatement pstmt=null;
		ResultSet rs=null;
		String[] str1;
		
		try {
			con= dataSource.getConnection();
			String qry="select * from socialstanding where active=1";
			pstmt=con.prepareStatement(qry);
			//pstmt.setString(1, userid);
			rs=pstmt.executeQuery();	
			while(rs.next()){
				form= new HomePageActionForm();
				form.setSocialstanding(rs.getString("socialstanding"));
				form.setTitle(rs.getString("title"));

				
				
				
				
//				for(int i =0; i<str1.length;i++){
//				
//				System.out.println(str1[i]);
//				form.setCorecompetenices(str1[i]);
//				}
				
				
				

				
			    list.add(form);
			    
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		finally
		{
			if(con!=null)
			con.close();
			if(pstmt!=null)
			pstmt.close();
			if(rs!=null)
			rs.close();	
		}
		return list;
	}

	public List<?> list1(DataSource dataSource) throws SQLException {

		List<HomePageActionForm>  list= new ArrayList<HomePageActionForm>();
		HomePageActionForm form=null;
		java.sql.Connection con=null;
		java.sql.PreparedStatement pstmt=null;
		ResultSet rs=null;
		String[] str1;
		
		try {
			con= dataSource.getConnection();
			String qry="select corecompetencies from userdetails";
			pstmt=con.prepareStatement(qry);
			//pstmt.setString(1, userid);
			rs=pstmt.executeQuery();	
			while(rs.next()){
				form= new HomePageActionForm();
				
				str1 =rs.getString("corecompetencies").split(",");
				
				
				
//				for(int i =0; i<str1.length;i++){
//				
//				System.out.println(str1[i]);
//				form.setCorecompetenices(str1[i]);
//				}
				
				
				form.setCorecompetenices(str1);

				
			    list.add(form);
			    
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		finally
		{
			if(con!=null)
			con.close();
			if(pstmt!=null)
			pstmt.close();
			if(rs!=null)
			rs.close();	
		}
		return list;
	}
	
	
	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
        JSONObject obj=null;
        try {
        	//System.out.println("inside detailsappend dao");
			con=dataSource.getConnection();      
			String sql="select us.corecompetencies,us.mystrength,us.aboutme,us.name,us.photo,us.designation,us.worksat,re.* from userdetails us, resume re where us.email=re.userid";

			pstmt=con.prepareStatement(sql);
			//System.out.println(userid);
//			pstmt.setString(1,userid);
            //System.out.println("sql for data append is......."+sql);
            
			
			rs=pstmt.executeQuery();	
			while(rs.next())
			{
				obj=new JSONObject();
				//System.out.println("summmmmmmmmmmmmmmm"+rs.getString("subtotal"));

			obj.put("corecompetencies",rs.getString("corecompetencies"));
			obj.put("mystrength",rs.getString("mystrength"));	
			obj.put("aboutme",rs.getString("aboutme"));
			obj.put("name",rs.getString("name"));
			obj.put("designation",rs.getString("designation"));
			obj.put("worksat",rs.getString("worksat"));
			obj.put("photo",rs.getString("photo"));
			obj.put("onepageresume",rs.getString("onepageresume"));
			obj.put("detailresume",rs.getString("detailresume"));

			jArray.add(obj);
			
		} }catch (Exception e) {
			e.printStackTrace();
		}finally{
			try{
				rs.close();
				pstmt.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return jArray;

	}


}
