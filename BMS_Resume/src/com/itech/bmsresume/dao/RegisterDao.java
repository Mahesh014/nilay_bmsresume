package com.itech.bmsresume.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.RegisterActionForm;

public class RegisterDao {

	public Object register(RegisterActionForm formdetails, DataSource dataSource) {
		

		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;

		try {
			// System.out.println("inside register dao");
			con = dataSource.getConnection();
			String qry = "insert into userdetails (name,email,password,mobile,dob)values(?,?,?,?,?)";
			ps = con.prepareStatement(qry);
			ps.setString(1, formdetails.getName());
			ps.setString(2, formdetails.getEmailid());
			ps.setString(3, formdetails.getNewpassword());
			ps.setInt(4, formdetails.getMobile());
			ps.setString(5, formdetails.getDob());

			update = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if (update > 0) {
			return "success";

		} else {
			return "failure";
		}
	}

}
