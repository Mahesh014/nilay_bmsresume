package com.itech.bmsresume.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.bmsresume.actionform.EngineeringcollaborationActionform;

public class EngineeringcollaborationDao {

	/*public Object details(EngineeringcollaborationActionform details,
			DataSource dataSource, String filepath, String filename1, String userid) {
		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;
		try {
			System.out.println("in banner dao");
			con = dataSource.getConnection();
			//String qry = "insert into userdetails(photo_engicollaboration,engineeringcollaboration) values(?,?) where email=?";
			String qry = "update userdetails set photo_engicollaboration=?,engineeringcollaboration=? where email=?";
			ps = con.prepareStatement(qry);
			System.out.println(filename1);
			ps.setString(1, filename1);
			ps.setString(2, details.getEngineeringcollaboration());
			ps.setString(3, userid);
			update = ps.executeUpdate();			
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		if(update>0)
		{
			return "success";
		}
		else
		{
			return "failure";
		}

	}

	
	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
        JSONObject obj=null;
        try {
        	//System.out.println("inside detailsappend dao");
			con=dataSource.getConnection();      
			String sql="select photo_engicollaboration,engineeringcollaboration from userdetails where email=?";
			pstmt=con.prepareStatement(sql);
			//System.out.println(userid);
			pstmt.setString(1,userid);
            //System.out.println("sql for data append is......."+sql);
            
			
			rs=pstmt.executeQuery();	
			while(rs.next())
			{
				obj=new JSONObject();
				//System.out.println("summmmmmmmmmmmmmmm"+rs.getString("subtotal"));

			obj.put("photo_engicollaboration",rs.getString("photo_engicollaboration"));
			obj.put("engineeringcollaboration",rs.getString("engineeringcollaboration"));
			
			jArray.add(obj);
			
		} }catch (Exception e) {
			e.printStackTrace();
		}finally{
			try{
				rs.close();
				pstmt.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return jArray;

	}*/


	public Object add(String heading, String textarea, DataSource dataSource,
			String userid, String fileName1, String filePath) {
		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;
		
		try {
			System.out.println("inside Educational dao");
			System.out.println("**********"+textarea);
			con = dataSource.getConnection();
			//String qry = "insert into userdetails (awardyear,award) values(?,?) where email=?";
			String qry = "insert into enginerringcollaboration(userid,heading,name,active,userphoto) values(?,?,?,?,?)";

			
			ps = con.prepareStatement(qry);
			// ps.setString(1, formdetails.getFname());
			ps.setString(1, userid);
			ps.setString(2, heading);
			ps.setString(3, textarea);
			ps.setBoolean(4, true);
			ps.setString(5,fileName1);
			
			System.out.println("&&&&&&&&"+heading);
			update = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if (update > 0) {
			return "success";

		} else {
			return "failure";
		}
	}
	
	


	public List<EngineeringcollaborationActionform> list(DataSource dataSource,
			String userid) throws SQLException {
		List<EngineeringcollaborationActionform>  list= new ArrayList<EngineeringcollaborationActionform>();
		EngineeringcollaborationActionform form=null;
		java.sql.Connection con=null;
		java.sql.PreparedStatement pstmt=null;
		ResultSet rs=null;
		
		
		try {
			con= dataSource.getConnection();
			String qry="select heading,name,active,userid,userphoto,id from enginerringcollaboration where userid=?";
			pstmt=con.prepareStatement(qry);
			pstmt.setString(1, userid);
			rs=pstmt.executeQuery();	
			while(rs.next()){
				form= new EngineeringcollaborationActionform();
				form.setId(rs.getInt("id"));
				form.setHeading(rs.getString("heading"));
				form.setName(rs.getString("name"));
				form.setActive(rs.getBoolean("active"));
				form.setUserid(rs.getString("userid"));
				form.setUserphoto(rs.getString("userphoto"));
				
				
			    list.add(form);
			    
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		finally
		{
			if(con!=null)
			con.close();
			if(pstmt!=null)
			pstmt.close();
			if(rs!=null)
			rs.close();	
		}
		return list;
	}


	public String update(EngineeringcollaborationActionform details,
			DataSource dataSource, String userid, String heading,
			String textarea, String filename1, String filepath) {
		
		Connection con = null;
		PreparedStatement pstmt = null;
		try{
			con=dataSource.getConnection();
			String sql;
		
		
					sql="UPDATE enginerringcollaboration SET heading=?,name=?,userphoto=? WHERE id=?";
					System.out.println(sql);
					pstmt=con.prepareStatement(sql);
					pstmt.setString(1, heading);
					System.out.println("***************"+heading);
					pstmt.setString(2, textarea);
					pstmt.setString(3, filename1);
					pstmt.setInt(4, details.getId());
					System.out.println("***************"+textarea);

					pstmt.executeUpdate();			
	
					}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
		
	}


	public Object changeStatus(EngineeringcollaborationActionform details,
			DataSource dataSource) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try	{
			con=dataSource.getConnection();
			String sql="update enginerringcollaboration set active = ? where userid=? and id=?";
			pstmt=con.prepareStatement(sql);
			pstmt.setBoolean(1, details.isActive());			
			pstmt.setString(2, details.getUserid());
			pstmt.setInt(3, details.getId());
			pstmt.executeUpdate();
			System.out.println("++++++++++++"+details.isActive());
			System.out.println("++++++++++++"+details.getUserid());

		}
		catch (Exception e){
			System.out.println(e);
		} 
		return null;
	}

		
}
