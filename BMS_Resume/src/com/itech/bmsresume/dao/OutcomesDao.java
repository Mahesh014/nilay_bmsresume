package com.itech.bmsresume.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.bmsresume.actionform.OutcomesActionForm;

public class OutcomesDao {

	public Object details(OutcomesActionForm details, DataSource dataSource, String userid, String str, String str1) {
		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;

		try {
			System.out.println("inside mystrength dao");
			con = dataSource.getConnection();
			//String qry = "insert into userdetails (outcomes , outcomesyear) values(?,?) where email=?";
			String qry = "update userdetails set outcomes=? , outcomesyear=?  where email=?";
			ps = con.prepareStatement(qry);
			// ps.setString(1, formdetails.getFname());
			
			ps.setString(1, str1);
			ps.setString(2, str);
			ps.setString(3,userid);

			update = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if (update > 0) {
			return "success";

		} else {
			return "failure";
		}

	}
	
	
	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
        JSONObject obj=null;
        try {
        	//System.out.println("inside detailsappend dao");
			con=dataSource.getConnection();      
			String sql="select outcomes,outcomesyear from userdetails where email=?";
			pstmt=con.prepareStatement(sql);
			//System.out.println(userid);
			pstmt.setString(1,userid);
            //System.out.println("sql for data append is......."+sql);
            
			
			rs=pstmt.executeQuery();	
			while(rs.next())
			{
				obj=new JSONObject();
				//System.out.println("summmmmmmmmmmmmmmm"+rs.getString("subtotal"));

			obj.put("outcomes",rs.getString("outcomes"));
			obj.put("outcomesyear",rs.getString("outcomesyear"));
			System.out.println(rs.getString("outcomes")+".............."+rs.getString("outcomesyear"));
			jArray.add(obj);
			
		} }catch (Exception e) {
			e.printStackTrace();
		}finally{
			try{
				rs.close();
				pstmt.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return jArray;

	}

		

}
