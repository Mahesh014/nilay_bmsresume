package com.itech.bmsresume.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.bmsresume.actionform.BookAuthoredActionFrom;

public class BookAuthoredDao {

	public Object details(BookAuthoredActionFrom details, DataSource dataSource, String userid, String str, String str1, String str2) {
		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;

		try {
			System.out.println("inside Administrative Practices dao");
			con = dataSource.getConnection();
			//String qry = "insert into userdetails (booktitle,publisher,publisheryear) values(?,?,?) where email=?";
			String qry = "insert into bookauthored (booktitle,publisher,publisheryear,userid) values(?,?,?,?)";
			ps = con.prepareStatement(qry);
			// ps.setString(1, formdetails.getFname());
			ps.setString(1, str);
			ps.setString(2, str1);
			ps.setString(3, str2);
			ps.setString(4, userid);
			update = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if (update > 0) {
			return "success";

		} else {
			return "failure";
		}
	}

	
	
	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
        JSONObject obj=null;
        try {
        	//System.out.println("inside detailsappend dao");
			con=dataSource.getConnection();      
			String sql="select booktitle,publisher,publisheryear from bookauthored where userid=?";
			pstmt=con.prepareStatement(sql);
			//System.out.println(userid);
			pstmt.setString(1,userid);
            //System.out.println("sql for data append is......."+sql);
            
			
			rs=pstmt.executeQuery();	
			while(rs.next())
			{
				obj=new JSONObject();
				//System.out.println("summmmmmmmmmmmmmmm"+rs.getString("subtotal"));

			obj.put("booktitle",rs.getString("booktitle"));
			obj.put("publisher",rs.getString("publisher"));
			obj.put("publisheryear",rs.getString("publisheryear"));
			
			
			jArray.add(obj);
			
		} }catch (Exception e) {
			e.printStackTrace();
		}finally{
			try{
				rs.close();
				pstmt.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return jArray;

	}



	public Object update(BookAuthoredActionFrom details, DataSource dataSource,
			String userid, String str, String str1, String str2) {
		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;

		try {
			System.out.println("inside Administrative Practices  update dao");
			con = dataSource.getConnection();
			//String qry = "insert into userdetails (booktitle,publisher,publisheryear) values(?,?,?) where email=?";
			String qry = "update bookauthored set booktitle=?,publisher=?, publisheryear=? where userid=?";
			ps = con.prepareStatement(qry);
			// ps.setString(1, formdetails.getFname());
			ps.setString(1, str);
			ps.setString(2, str1);
			ps.setString(3, str2);
			ps.setString(4, userid);
			update = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if (update > 0) {
			return "success";

		} else {
			return "failure";
		}
	}
	}

