package com.itech.bmsresume.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.AsprincipalUIActionForm;

public class AsprincipalUIDao {
	
public List<AsprincipalUIActionForm> list(DataSource dataSource) throws SQLException {


		
		List<AsprincipalUIActionForm>  list= new ArrayList<AsprincipalUIActionForm>();
		AsprincipalUIActionForm form=null;
		java.sql.Connection con=null;
		java.sql.PreparedStatement pstmt=null;
		ResultSet rs=null;
		
		
		try {
			con= dataSource.getConnection();
			String qry="select title,contributionasprincipal from contribution_as_principal where active=1";
			pstmt=con.prepareStatement(qry);
			//pstmt.setString(1, userid);
			rs=pstmt.executeQuery();	
			while(rs.next()){
				form= new AsprincipalUIActionForm();
				form.setTitle(rs.getString("title"));
				form.setContributionasprincipal(rs.getString("contributionasprincipal"));
				
				
				
//				for(int i =0; i<str1.length;i++){
//				
//				System.out.println(str1[i]);
//				form.setCorecompetenices(str1[i]);
//				}
				
				
				

				
			    list.add(form);
			    
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		finally
		{
			if(con!=null)
			con.close();
			if(pstmt!=null)
			pstmt.close();
			if(rs!=null)
			rs.close();	
		}
		return list;
	}

}
