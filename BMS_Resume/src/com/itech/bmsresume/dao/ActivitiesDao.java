package com.itech.bmsresume.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.ActivitiesActionForm;
import com.itech.bmsresume.actionform.EducationalcollaborationActionForm;

public class ActivitiesDao {

/*	public Object details(ActivitiesActionForm details, DataSource dataSource,
			String filepath, String filename1, String userid) {
		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;
		try {
			System.out.println("in banner dao");
			con = dataSource.getConnection();
			//String qry = "insert into userdetails(photo_activities,teqip,teqipi,teqipii) values(?,?,?,?) where email=?";
			String qry = "update userdetails set photo_activities=?,teqip=?,teqipi=?,teqipii=? where email=?";
			ps = con.prepareStatement(qry);
			System.out.println(filename1);
			ps.setString(1, filename1);
			ps.setString(2, details.getTeqip());
			ps.setString(3, details.getTeqipi());
			ps.setString(4, details.getTeqipii());
			ps.setString(5, userid);
			update = ps.executeUpdate();			
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		if(update>0)
		{
			return "success";
		}
		else
		{
			return "failure";
		}

	}

	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
        JSONObject obj=null;
        try {
        	//System.out.println("inside detailsappend dao");
			con=dataSource.getConnection();      
			String sql="select photo_activities,teqip,teqipi,teqipii from userdetails where email=?";
			pstmt=con.prepareStatement(sql);
			//System.out.println(userid);
			pstmt.setString(1,userid);
            //System.out.println("sql for data append is......."+sql);
            
			
			rs=pstmt.executeQuery();	
			while(rs.next())
			{
				obj=new JSONObject();
				//System.out.println("summmmmmmmmmmmmmmm"+rs.getString("subtotal"));

			obj.put("photo_activities",rs.getString("photo_activities"));
			obj.put("teqip",rs.getString("teqip"));
			obj.put("teqipi",rs.getString("teqipi"));
			obj.put("teqipii",rs.getString("teqipii"));
			
			
			jArray.add(obj);
			
		} }catch (Exception e) {
			e.printStackTrace();
		}finally{
			try{
				rs.close();
				pstmt.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return jArray;

	}*/

	public Object add(ActivitiesActionForm details, DataSource dataSource,
			String userid, String filename1, String filepath) {
		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;
		
		try {
			System.out.println("inside activity dao");
			con = dataSource.getConnection();
			//String qry = "insert into userdetails (awardyear,award) values(?,?) where email=?";
			String qry = "insert into activities (userid,heading,photo,description,active) values(?,?,?,?,?)";
			ps = con.prepareStatement(qry);
			// ps.setString(1, formdetails.getFname());
			ps.setString(1, userid);

			ps.setString(2, details.getHeading());
			System.out.println(filename1);
			ps.setString(3, filename1);
			ps.setString(4, details.getActivity());
			ps.setBoolean(5, true);

			update = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if (update > 0) {
			return "success";

		} else {
			return "failure";
		}

	}
	

	public List<ActivitiesActionForm> list(DataSource dataSource,
			String userid) {
		List<ActivitiesActionForm>  list= new ArrayList<ActivitiesActionForm>();
		ActivitiesActionForm form=null;
		java.sql.Connection con=null;
		java.sql.PreparedStatement pstmt=null;
		ResultSet rs=null;
		
		
		try {
			con= dataSource.getConnection();
			String qry="select heading,photo,description,userid,active,id from activities where userid=?";
			pstmt=con.prepareStatement(qry);
			pstmt.setString(1, userid);
			rs=pstmt.executeQuery();	
			while(rs.next()){
				form= new ActivitiesActionForm();
				form.setHeading(rs.getString("heading"));
				form.setActivity(rs.getString("description"));
				form.setFilename(rs.getString("photo"));
				form.setUserid(rs.getString("userid"));
				form.setActive(rs.getBoolean("active"));
				form.setId(rs.getInt("id"));
			    list.add(form);
			    
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		finally
		{

			try {
				if(con!=null)
					con.close();
				if(pstmt!=null)
					pstmt.close();
				if(rs!=null)
					rs.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}

		}
		return list;
	
	}

	public String update(ActivitiesActionForm educationalForm, DataSource dataSource,String heading, String activity, String filepath, String filename1) 
	{
		
		Connection con = null;
		PreparedStatement ps= null;
		try{
			con=dataSource.getConnection();
			String sql;
		
			String qry = "update activities set heading=?,photo=?,description=? where id=?";
			ps = con.prepareStatement(qry);
			// ps.setString(1, formdetails.getFname());
			ps.setString(1, educationalForm.getHeading());
			ps.setString(2, filename1);
			ps.setString(3, activity);
			ps.setInt(4, educationalForm.getId());
			

			ps.executeUpdate();			
			
		}
catch (Exception e) {
e.printStackTrace();
}

return null;
}
	
	
	
	
	public String changeStatus(ActivitiesActionForm educationalForm, DataSource dataSource)
	{
		Connection con = null;
		PreparedStatement pstmt = null;
		try	{
			con=dataSource.getConnection();
			String sql="update activities set active=? where id=?";
			pstmt=con.prepareStatement(sql);
			pstmt.setBoolean(1, educationalForm.isActive());			
			pstmt.setInt(2, educationalForm.getId());
			pstmt.executeUpdate();

		}
		catch (Exception e){
			System.out.println(e);
		} 
		return null;
	}

		
}
