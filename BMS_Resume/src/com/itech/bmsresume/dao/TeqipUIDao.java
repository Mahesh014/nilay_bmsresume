package com.itech.bmsresume.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.TeqipUIActionForm;

public class TeqipUIDao {

public List<TeqipUIActionForm> list(DataSource dataSource) throws SQLException {


		
		List<TeqipUIActionForm>  list= new ArrayList<TeqipUIActionForm>();
		TeqipUIActionForm form=null;
		java.sql.Connection con=null;
		java.sql.PreparedStatement pstmt=null;
		ResultSet rs=null;
		
		
		try {
			con= dataSource.getConnection();
			String qry="select heading,description,photo from activities where active=1";
			pstmt=con.prepareStatement(qry);
			//pstmt.setString(1, userid);
			rs=pstmt.executeQuery();	
			while(rs.next()){
				form= new TeqipUIActionForm();
				form.setHeading(rs.getString("heading"));
				form.setDescription(rs.getString("description"));
				form.setPhoto(rs.getString("photo"));
				
				
				
//				for(int i =0; i<str1.length;i++){
//				
//				System.out.println(str1[i]);
//				form.setCorecompetenices(str1[i]);
//				}
				
				
				

				
			    list.add(form);
			    
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		finally
		{
			if(con!=null)
			con.close();
			if(pstmt!=null)
			pstmt.close();
			if(rs!=null)
			rs.close();	
		}
		return list;
	}
}
