package com.itech.bmsresume.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.bmsresume.actionform.ResumeuploadActionForm;

public class ResumeuploadDao {

	public Object details(ResumeuploadActionForm details,
			DataSource dataSource, String filepath, String filename1, String filepath1, String filename11,String userid) {
		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;
		try {
			System.out.println("in banner dao");
			con = dataSource.getConnection();
			//String qry = "insert into userdetails(name,photo,designation,worksat) values(?,?,?,?) where email=?";
			String qry = "insert into resume(onepageresume,detailresume,userid) values(?,?,?)";
			ps = con.prepareStatement(qry);
			
			System.out.println(filename1+"........."+filename11);
			System.out.println(userid);
			ps.setString(1, filename1);
			ps.setString(2, filename11);
			ps.setString(3, userid);
			update = ps.executeUpdate();			
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		if(update>0)
		{
			return "success";
		}
		else
		{
			return "failure";
		}
	}

	public Object update(ResumeuploadActionForm details, DataSource dataSource,
			String filepath, String filename1, String filepath1,
			String filename11, String userid) {
		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;
		try {
			System.out.println("in banner dao");
			con = dataSource.getConnection();
			//String qry = "insert into userdetails(name,photo,designation,worksat) values(?,?,?,?) where email=?";
			//String qry = "insert into resume(onepageresume,detailresume,userid) values(?,?,?)";
			String qry="update resume set onepageresume=?,detailresume=? where userid=?";
			ps = con.prepareStatement(qry);
			
			System.out.println(filename1+"........."+filename11);
			System.out.println(userid);
			ps.setString(1, filename1);
			ps.setString(2, filename11);
			ps.setString(3, userid);
			update = ps.executeUpdate();			
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		if(update>0)
		{
			return "success";
		}
		else
		{
			return "failure";
		}
	}

	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
        JSONObject obj=null;
        try {
        	System.out.println("inside banner dao");
			con=dataSource.getConnection();      
			String sql="select onepageresume,detailresume from resume where userid=?";
			pstmt=con.prepareStatement(sql);
			//System.out.println(userid);
			pstmt.setString(1,userid);
            //System.out.println("sql for data append is......."+sql);
            
			
			rs=pstmt.executeQuery();	
			while(rs.next())
			{
				obj=new JSONObject();
				//System.out.println("summmmmmmmmmmmmmmm"+rs.getString("subtotal"));

			obj.put("onepageresume",rs.getString("onepageresume"));
			obj.put("detailresume",rs.getString("detailresume"));
			
			System.out.println("file is cominnnnnnng"+rs.getString("onepageresume")+"......"+rs.getString("detailresume"));
			
			
			jArray.add(obj);
			
		} }catch (Exception e) {
			e.printStackTrace();
		}finally{
			try{
				rs.close();
				pstmt.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return jArray;

	}
}
