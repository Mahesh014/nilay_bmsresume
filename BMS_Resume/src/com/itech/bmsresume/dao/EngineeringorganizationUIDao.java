package com.itech.bmsresume.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.EngineeringorganizationUIActionForm;

public class EngineeringorganizationUIDao {
	
public List<EngineeringorganizationUIActionForm> list(DataSource dataSource) throws SQLException {


		
		List<EngineeringorganizationUIActionForm>  list= new ArrayList<EngineeringorganizationUIActionForm>();
		EngineeringorganizationUIActionForm form=null;
		java.sql.Connection con=null;
		java.sql.PreparedStatement pstmt=null;
		ResultSet rs=null;
		
		
		try {
			con= dataSource.getConnection();
			String qry="select heading,name,userphoto from enginerringcollaboration where active=1";
			pstmt=con.prepareStatement(qry);
			//pstmt.setString(1, userid);
			rs=pstmt.executeQuery();	
			while(rs.next()){
				form= new EngineeringorganizationUIActionForm();
				form.setHeading(rs.getString("heading"));
				form.setName(rs.getString("name"));
				form.setUserphoto(rs.getString("userphoto"));
				
				
//				for(int i =0; i<str1.length;i++){
//				
//				System.out.println(str1[i]);
//				form.setCorecompetenices(str1[i]);
//				}
				
				
				

				
			    list.add(form);
			    
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		finally
		{
			if(con!=null)
			con.close();
			if(pstmt!=null)
			pstmt.close();
			if(rs!=null)
			rs.close();	
		}
		return list;
	}

}
