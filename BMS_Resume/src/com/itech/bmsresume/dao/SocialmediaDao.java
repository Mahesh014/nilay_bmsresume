package com.itech.bmsresume.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.itech.bmsresume.actionform.ContactActionForm;
import com.itech.bmsresume.actionform.SocialmediaActionForm;

public class SocialmediaDao {

	
	public Object details(SocialmediaActionForm details, DataSource dataSource,
			String userid) {
		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;

		try {
			 System.out.println("inside Contact dao");
			con = dataSource.getConnection();
			//String qry = "insert into userdetails (aboutme) values(?) where emailid=?";
			String qry = "insert into socialmedia(facebooklink ,twitterlink,youtubelink,googlelink,userid) value(?,?,?,?,?)";
			ps = con.prepareStatement(qry);
			//ps.setString(1, formdetails.getFname());
			
			ps.setString(1, details.getFlink());
			ps.setString(2, details.getTlink());
			ps.setString(3, details.getYlink());
			ps.setString(4, details.getGlink());
			ps.setString(5, userid);
			

			update = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if (update > 0) {
			return "success";

		} else {
			return "failure";
		}
	}

	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
        JSONObject obj=null;
        try {
        	//System.out.println("inside detailsappend dao");
			con=dataSource.getConnection();      
			String sql="select * from socialmedia where userid=?";
			pstmt=con.prepareStatement(sql);
			//System.out.println(userid);
			pstmt.setString(1,userid);
            //System.out.println("sql for data append is......."+sql);
            
			
			rs=pstmt.executeQuery();	
			while(rs.next())
			{
				obj=new JSONObject();
				//System.out.println("summmmmmmmmmmmmmmm"+rs.getString("subtotal"));

			obj.put("facebooklink",rs.getString("facebooklink"));
			obj.put("twitterlink",rs.getString("twitterlink"));
			obj.put("youtubelink",rs.getString("youtubelink"));
			obj.put("googlelink",rs.getString("googlelink"));
			
			
			
			
			jArray.add(obj);
			
		} }catch (Exception e) {
			e.printStackTrace();
		}finally{
			try{
				rs.close();
				pstmt.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return jArray;
	}
	
	
	public Object update(SocialmediaActionForm details, DataSource dataSource,
			String userid) {
		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;

		try {
			 System.out.println("inside Contact dao");
			con = dataSource.getConnection();
			//String qry = "insert into userdetails (aboutme) values(?) where emailid=?";
			String qry = "update socialmedia set facebooklink=? ,twitterlink=?,youtubelink=?,googlelink=? where userid=?";
			ps = con.prepareStatement(qry);
			//ps.setString(1, formdetails.getFname());
			ps.setString(1, details.getFlink());
			ps.setString(2, details.getTlink());
			ps.setString(3, details.getYlink());
			ps.setString(4, details.getGlink());
			ps.setString(5, userid);
			

			update = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if (update > 0) {
			return "success";

		} else {
			return "failure";
		}
	}
}
