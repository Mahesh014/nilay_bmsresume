package com.itech.bmsresume.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.EducationalcollaborationActionForm;
import com.itech.bmsresume.actionform.ResearchWorkActioinForm;

public class ResearchworkDao {

	/*public Object details(ResearchWorkActioinForm details, DataSource dataSource, String userid) {
		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;

		try {
			System.out.println("inside mystrength dao");
			con = dataSource.getConnection();
			//String qry = "insert into userdetails (researchwork) values(?) where email=?";
			String qry = "update userdetails set researchwork=? where email=?";
			ps = con.prepareStatement(qry);
			// ps.setString(1, formdetails.getFname());
			
			ps.setString(1, details.getResearchwork());
			ps.setString(2, userid);

			update = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if (update > 0) {
			return "success";

		} else {
			return "failure";
		}

	}
	
	
	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
        JSONObject obj=null;
        try {
        	//System.out.println("inside detailsappend dao");
			con=dataSource.getConnection();      
			String sql="select * from userdetails where email=?";
			pstmt=con.prepareStatement(sql);
			//System.out.println(userid);
			pstmt.setString(1,userid);
            //System.out.println("sql for data append is......."+sql);
            
			
			rs=pstmt.executeQuery();	
			while(rs.next())
			{
				obj=new JSONObject();
				//System.out.println("summmmmmmmmmmmmmmm"+rs.getString("subtotal"));

			obj.put("researchwork",rs.getString("researchwork"));
			
			jArray.add(obj);
			
		} }catch (Exception e) {
			e.printStackTrace();
		}finally{
			try{
				rs.close();
				pstmt.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return jArray;

	}*/


	public Object add(ResearchWorkActioinForm details, DataSource dataSource,
			String userid, String str, String heading) {
		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;
		
		try {
			System.out.println("inside contribution as principal dao");
			con = dataSource.getConnection();
			//String qry = "insert into userdetails (awardyear,award) values(?,?) where email=?";
			String qry = "insert into research (heading,research_work,userid) values(?,?,?)";
			ps = con.prepareStatement(qry);
			// ps.setString(1, formdetails.getFname());
			ps.setString(1, heading);
			ps.setString(2, str);
			ps.setString(3, userid);

			update = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if (update > 0) {
			return "success";

		} else {
			return "failure";
		}

	}

	public List<ResearchWorkActioinForm> list(DataSource dataSource,
			String userid) throws SQLException {
		List<ResearchWorkActioinForm>  list= new ArrayList<ResearchWorkActioinForm>();
		ResearchWorkActioinForm form=null;
		java.sql.Connection con=null;
		java.sql.PreparedStatement pstmt=null;
		ResultSet rs=null;
		
		
		try {
			con= dataSource.getConnection();
			String qry="select heading,research_work,active,userid,id from research where userid=?";
			pstmt=con.prepareStatement(qry);
			pstmt.setString(1, userid);
			rs=pstmt.executeQuery();	
			while(rs.next()){
				form= new ResearchWorkActioinForm();
				form.setHeading(rs.getString("heading"));
				form.setResearchwork(rs.getString("research_work"));
				form.setActive(rs.getBoolean("active"));
				form.setUserid(rs.getString("userid"));
				form.setId(rs.getInt("id"));
			    list.add(form);
			    
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		finally
		{
			if(con!=null)
			con.close();
			if(pstmt!=null)
			pstmt.close();
			if(rs!=null)
			rs.close();	
		}
		return list;
	
	}

	public Object update(ResearchWorkActioinForm details,
			DataSource dataSource, String userid, String str, int id) {
		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;
		
		try {
			System.out.println("inside contribution as principal dao");
			con = dataSource.getConnection();
			//String qry = "insert into userdetails (awardyear,award) values(?,?) where email=?";
			String qry = "update research set research_work=?,heading=? where userid=? and id=?";
			ps = con.prepareStatement(qry);
			// ps.setString(1, formdetails.getFname());
			ps.setString(2, details.getHeading());
			ps.setString(1, str);
			ps.setString(3, userid);
			ps.setInt(4, id);

			update = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if (update > 0) {
			return "success";

		} else {
			return "failure";
		}

	}
	
	
	public String changeStatus(ResearchWorkActioinForm educationalForm, DataSource dataSource)
	{
		
      
		Connection con = null;
		PreparedStatement pstmt = null;
		try	{
			con=dataSource.getConnection();
			String sql="update research set active = ? where userid=? and id=?";
			pstmt=con.prepareStatement(sql);
			pstmt.setBoolean(1, educationalForm.isActive());			
			pstmt.setString(2, educationalForm.getUserid());
			pstmt.setInt(3, educationalForm.getId());
			pstmt.executeUpdate();
			System.out.println("++++++++++++"+educationalForm.isActive());
			System.out.println("++++++++++++"+educationalForm.getUserid());

		}
		catch (Exception e){
			System.out.println(e);
		} 
		return null;
	}
}

		

