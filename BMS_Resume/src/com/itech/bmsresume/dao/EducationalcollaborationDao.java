package com.itech.bmsresume.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.EducationalcollaborationActionForm;

public class EducationalcollaborationDao {

	/*public Object details(EducationalcollaborationActionForm details,
			DataSource dataSource, String filepath, String filename1, String userid) {
		
		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;
		try {
			System.out.println("in banner dao");
			con = dataSource.getConnection();
			//String qry = "insert into userdetails(photo_educollaboration,educationalcollaboration) values(?,?) where email=?";
			String qry = "update userdetails set photo_educollaboration=?,educationalcollaboration=? where email=?";
			ps = con.prepareStatement(qry);
			System.out.println(filename1);
			ps.setString(1, filename1);
			ps.setString(2, details.getEducationalcollaboration());
			ps.setString(3, userid);
			update = ps.executeUpdate();			
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		if(update>0)
		{
			return "success";
		}
		else
		{
			return "failure";
		}
	}

	
	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		JSONArray jArray= new JSONArray();
        JSONObject obj=null;
        try {
        	//System.out.println("inside detailsappend dao");
			con=dataSource.getConnection();      
			String sql="select photo_educollaboration,educationalcollaboration from userdetails where email=?";
			pstmt=con.prepareStatement(sql);
			//System.out.println(userid);
			pstmt.setString(1,userid);
            //System.out.println("sql for data append is......."+sql);
            
			
			rs=pstmt.executeQuery();	
			while(rs.next())
			{
				obj=new JSONObject();
				//System.out.println("summmmmmmmmmmmmmmm"+rs.getString("subtotal"));

			obj.put("photo_educollaboration",rs.getString("photo_educollaboration"));
			obj.put("educationalcollaboration",rs.getString("educationalcollaboration"));
			
			
			jArray.add(obj);
			
		} }catch (Exception e) {
			e.printStackTrace();
		}finally{
			try{
				rs.close();
				pstmt.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return jArray;

	}*/
	
	public Object add(String heading, String textarea, DataSource dataSource,
			String userid,String fileName,String filePath) {
		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;
		
		try {
			System.out.println("inside Educational dao");
			System.out.println("**********"+textarea);
			con = dataSource.getConnection();
			//String qry = "insert into userdetails (awardyear,award) values(?,?) where email=?";
			String qry = "insert into educational(userid,heading,name,active,userphoto) values(?,?,?,?,?)";

			
			ps = con.prepareStatement(qry);
			// ps.setString(1, formdetails.getFname());
			ps.setString(1, userid);
			ps.setString(2, heading);
			ps.setString(3, textarea);
			ps.setBoolean(4, true);
			ps.setString(5,fileName);
			
			System.out.println("&&&&&&&&"+heading);
			update = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if (update > 0) {
			return "success";

		} else {
			return "failure";
		}
	}
	
	public List<EducationalcollaborationActionForm> list(DataSource dataSource,
			String userid) throws SQLException {
		List<EducationalcollaborationActionForm>  list= new ArrayList<EducationalcollaborationActionForm>();
		EducationalcollaborationActionForm form=null;
		java.sql.Connection con=null;
		java.sql.PreparedStatement pstmt=null;
		ResultSet rs=null;
		
		
		try {
			con= dataSource.getConnection();
			String qry="select heading,name,active,userid,userphoto,id from educational where userid='"+userid+"'";
			pstmt=con.prepareStatement(qry);
			rs=pstmt.executeQuery();	
			while(rs.next()){
				form= new EducationalcollaborationActionForm();
				form.setId(rs.getInt("id"));
				form.setHeading(rs.getString("heading"));
				form.setTextarea(rs.getString("name"));
				form.setFilename(rs.getString("userphoto"));
				form.setActive(rs.getBoolean("active"));


			    list.add(form);
			    
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		finally
		{
			if(con!=null)
			con.close();
			if(pstmt!=null)
			pstmt.close();
			if(rs!=null)
			rs.close();	
		}
		return list;
	
	}
	
	
	
	public String changeStatus(EducationalcollaborationActionForm educationalForm, DataSource dataSource)
	{
		
      
		Connection con = null;
		PreparedStatement pstmt = null;
		try	{
			con=dataSource.getConnection();
			String sql="update educational set active = ? where id=?";
			pstmt=con.prepareStatement(sql);
			pstmt.setBoolean(1, educationalForm.isActive());			
			pstmt.setInt(2, educationalForm.getId());
			pstmt.executeUpdate();
			System.out.println("++++++++++++"+educationalForm.isActive());
			System.out.println("++++++++++++"+educationalForm.getUserid());

		}
		catch (Exception e){
			System.out.println(e);
		} 
		return null;
	}
	
	public String update(EducationalcollaborationActionForm educationalForm, DataSource dataSource,String heading, String textarea, String filepath, String filename1) 
	{
		
		Connection con = null;
		PreparedStatement pstmt = null;
		try{
			con=dataSource.getConnection();
			String sql;
		
		
					sql="UPDATE educational SET heading=?,name=?,userphoto=? WHERE id=?";
					System.out.println(sql);
					pstmt=con.prepareStatement(sql);
					pstmt.setString(1, heading);
					System.out.println("***************"+heading);
					pstmt.setString(2, textarea);
					pstmt.setString(3, filename1);
					pstmt.setInt(4, educationalForm.getId());

					pstmt.executeUpdate();			
	
					}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
		
}
