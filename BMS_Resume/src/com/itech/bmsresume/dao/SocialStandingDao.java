package com.itech.bmsresume.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.SocialstandingActionForm;

public class SocialStandingDao {

	public Object add(SocialstandingActionForm details, String title, DataSource dataSource,
			String userid, String str) {
		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;
		
		try {
			System.out.println("inside social standing dao");
			con = dataSource.getConnection();
			//String qry = "insert into userdetails (awardyear,award) values(?,?) where email=?";
			String qry = "insert into socialstanding (title,socialstanding,userid,active) values(?,?,?,?)";
			ps = con.prepareStatement(qry);
			// ps.setString(1, formdetails.getFname());
			ps.setString(1, title);
			ps.setString(2, str);
			ps.setString(3, userid);
			ps.setBoolean(4, true);
			update = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if (update > 0) {
			return "success";

		} else {
			return "failure";
		}

	}

	public Object update(SocialstandingActionForm details,
			DataSource dataSource, String userid, String str) {


		Connection con = null;
		PreparedStatement ps = null;
		int update = 0;
		
		try {
			System.out.println("inside Awards dao");
			con = dataSource.getConnection();
			//String qry = "insert into userdetails (awardyear,award) values(?,?) where email=?";
			String qry = "update socialstanding set socialstanding=?,title=? where userid=?";
			
			ps = con.prepareStatement(qry);
			// ps.setString(1, formdetails.getFname());
			ps.setString(1, details.getSocialstanding());
			ps.setString(2 ,details.getTitle());
			ps.setString(3, userid);
			
			System.out.println(qry);
			update = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null)
					con.close();
				if (ps != null)
					ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if (update > 0) {
			return "success";

		} else {
			return "failure";
		}

	}

	public List<SocialstandingActionForm> list(DataSource dataSource,
			String userid) throws SQLException {
		List<SocialstandingActionForm>  list= new ArrayList<SocialstandingActionForm>();
		SocialstandingActionForm form=null;
		java.sql.Connection con=null;
		java.sql.PreparedStatement pstmt=null;
		ResultSet rs=null;
		
		
		try {
			con= dataSource.getConnection();
			String qry="select title,socialstanding,userid,active,id from socialstanding where userid=?";
			pstmt=con.prepareStatement(qry);
			pstmt.setString(1, userid);
			rs=pstmt.executeQuery();	
			while(rs.next()){
				form= new SocialstandingActionForm();
				form.setTitle(rs.getString("title"));
				form.setSocialstanding(rs.getString("socialstanding"));
				form.setUserid(rs.getString("userid"));
				form.setActive(rs.getBoolean("active"));
				System.out.println(rs.getInt("id"));
				form.setId(rs.getInt("id"));
				
			    list.add(form);
			    
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		finally
		{
			if(con!=null)
			con.close();
			if(pstmt!=null)
			pstmt.close();
			if(rs!=null)
			rs.close();	
		}
		return list;
	
	}

	public Object changeStatus(SocialstandingActionForm details,
			DataSource dataSource) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try	{
			con=dataSource.getConnection();
			String sql="update socialstanding set active = ? where userid=? and id=?";
			pstmt=con.prepareStatement(sql);
			pstmt.setBoolean(1, details.isActive());			
			pstmt.setString(2, details.getUserid());
			pstmt.setInt(3, details.getId());
			
			
			
			pstmt.executeUpdate();
			System.out.println("++++++++++++"+details.isActive());
			System.out.println("++++++++++++"+details.getUserid());

		}
		catch (Exception e){
			System.out.println(e);
		} 
		return null;
	}



}
