package com.itech.bmsresume.action;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.itech.bmsresume.actionform.ResearchWorkActioinForm;
import com.itech.bmsresume.handler.ResearchWorkHandler;

public class ResearchworkAction extends DispatchAction {
	
	ResearchWorkHandler handler =  new ResearchWorkHandler();
	
	
	/*public ActionForward details (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception
			{
		
		ResearchWorkActioinForm details =  (ResearchWorkActioinForm) form;
		
		HttpSession session = request.getSession();
		String userid = (String) session.getAttribute("user");
		System.out.println(userid+"<>");
		
		request.setAttribute("details", handler.details(details, getDataSource(request),userid));
				return mapping.findForward("success");
		
			}
	
	public ActionForward detailsAppend(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	{
			try {

				JSONArray obj;
				HttpSession session = request.getSession();
				String userid = (String) session.getAttribute("user");

				// this mathod is for append data to basicDetails jsp.
				//String productid=request.getParameter("removeid");
				//System.out.println("____________________________"+productid);
				//System.out.print(" ppppppppppppppppppppppppppppppppppppppp"+request.getParameter("buttonid"));
					 obj = handler.detailsAppend(getDataSource(request),request,response,userid);
					 response.getOutputStream().write(obj.toString().getBytes());
					} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		
	}*/

	
	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SQLException
	{
		ResearchWorkActioinForm details =  (ResearchWorkActioinForm) form;
		HttpSession session = request.getSession();
		String userid = (String) session.getAttribute("user");
		System.out.println(userid+"<>");
		
		String str = "";
		String strength[] = request.getParameterValues("researchwork");
		String heading = request.getParameter("heading");
		
		System.out.println("+++++++++++++++++++"+heading);

		for (int i = 0; i < strength.length; i++) {
			str = str + "~@~" + strength[i];

		}
		str = str.replaceFirst("~@~" , "");
		System.out.println(str);
		
		request.setAttribute("info", handler.add(details,heading,getDataSource(request),userid,str));
		return list(mapping, form, request, response);
		
		
	}


	public ActionForward list(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SQLException {

		try{



			HttpSession session = request.getSession();
			String userid = (String) session.getAttribute("user");
			System.out.println(userid + "<>");

			List<ResearchWorkActioinForm> detailsList = handler.list(getDataSource(request),userid);
			request.setAttribute("roleList", detailsList);
		}
		catch (Exception e) {
			e.printStackTrace();		}

		return mapping.findForward("success1");
	}
	
	public ActionForward update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SQLException
	{
		HttpSession session = request.getSession();
		ResearchWorkActioinForm details =  (ResearchWorkActioinForm) form;
		String userid = (String) session.getAttribute("user");
		System.out.println(userid+"<>");
		
		String str = "";
		String strength[] = request.getParameterValues("researchwork");
		for (int i = 0; i < strength.length; i++) {
			str = str + "~@~" + strength[i];

		}
		str = str.replaceFirst("~@~" , "");
		System.out.println(str);
		int id=Integer.parseInt(request.getParameter("id"));
		
		request.setAttribute("info", handler.update(details,getDataSource(request),userid,str,id));
		return list(mapping, form, request, response);
		
		
	}
	
	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception{
		try {
			
			ResearchWorkActioinForm educationalForm = (ResearchWorkActioinForm)form;
			educationalForm.setUserid(request.getParameter("userid"));
			educationalForm.setId(Integer.parseInt(request.getParameter("id")));
			System.out.println("%%%%%%%%%%%"+request.getParameter("userid"));
			educationalForm.setActive(Boolean.parseBoolean(request.getParameter("active")));
			System.out.println("*******"+Boolean.parseBoolean(request.getParameter("active")));
		    request.setAttribute("status", handler.changeStatus(educationalForm, getDataSource(request)));
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		    return list(mapping, form, request, response);
	}
	
}
