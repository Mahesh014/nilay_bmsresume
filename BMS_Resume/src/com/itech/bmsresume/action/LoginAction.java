package com.itech.bmsresume.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.itech.bmsresume.actionform.RegisterActionForm;
import com.itech.bmsresume.handler.LoginHandler;

public class LoginAction extends DispatchAction {

	String status="PLEASE CHECK YOUR EMAIL_ID AND PASSWORD";
	String status1="PLEASE CHECK YOUR OLD PASSWORD";
	String status2="PASSWORD CHANGE SUCCESSFULLY";
	LoginHandler handler = new LoginHandler();

	public ActionForward login(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("inside lllllllllllllllooooooooooooooooooogigggggn");

		RegisterActionForm formdetails = (RegisterActionForm) form;
		System.out.println("inside login action \n");

		List<RegisterActionForm> loginCredential = handler.login(formdetails,getDataSource(request));
		HttpSession session = request.getSession(true);
		
		if (loginCredential.size() == 1) {
			System.out.println("inside if");
			session.setAttribute("user", loginCredential.iterator().next().getEmailid());
			// session.setAttribute("user",
			// loginCredential.iterator().next().getName());
			System.out.println(loginCredential.iterator().next().getEmailid()+ "''''''''''''''''");

			// session.setAttribute("loginCredential", loginCredential);

			// check with contains mathod
			// System.out.println(loginCredential.contains(loginCredential.listIterator().next().getUserid()));
			// System.out.println(session.getAttribute("username"));

			return mapping.findForward("success");
		} else {
			request.setAttribute("status",status);
			return mapping.findForward("failure");

		}

	}

	
	public ActionForward changepassword(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {

		System.out.println("inside LoginAction changepassword");
		RegisterActionForm formdetails = (RegisterActionForm) form;

		HttpSession session = request.getSession(true);
		String usersessiondata =(String) session.getAttribute("user");

		if(usersessiondata != null){
			String returnvalue = handler.changepassword(formdetails,getDataSource(request));

			if (returnvalue != null) {
				System.out.println("inside if LoginAction changepassword");

				request.setAttribute("status",status2);
				return mapping.findForward(returnvalue);
			} else {
				request.setAttribute("status",status1);
				return mapping.findForward("failure");

			}
		}else {
			return mapping.findForward("sessionfailure");

		}

	}
}
