package com.itech.bmsresume.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class LogoutAction extends DispatchAction {

	public ActionForward AdminLogoutAction(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		try {
			HttpSession session=request.getSession();
			session.invalidate();
			request.setAttribute("status","Logout Successfully!!");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("success");
		
	}
}
