package com.itech.bmsresume.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.upload.FormFile;
import org.json.simple.JSONArray;

import com.itech.bmsresume.actionform.BannerActionFrom;
import com.itech.bmsresume.handler.BannerHandler;

public class BannerAction extends DispatchAction {

	BannerHandler handler = new BannerHandler();

	public ActionForward details(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try{

		BannerActionFrom details = (BannerActionFrom) form;

		HttpSession session = request.getSession();
		String userid = (String) session.getAttribute("user");
		System.out.println(userid + "<>");

		String filepath = "";
		String filename1 = "";
		System.out.println("Inside banner action");
		if (!details.getPhoto1().equals("")) {
			FormFile myfile = details.getPhoto1();
			System.out.println("..................");
			filepath = getServlet().getServletContext().getRealPath("/")+ "Employeeupload";
			// System.out.println(filePath+"filepaaaaaaaaath issssss");
			// we concate for store multiple file
			filename1 = myfile.getFileName();

			FileUploadDownloadUtil.fileUploader(myfile, filepath, filename1);
		}
		request.setAttribute("details", handler.details(details,
				getDataSource(request), filepath, filename1,userid));
		}
		catch (Exception e) {
e.printStackTrace();		}
		return mapping.findForward("success");

	}

	
	
	public ActionForward detailsAppend(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	{
			try {

				System.out.println("inside banner action details");
				JSONArray obj;
				HttpSession session = request.getSession();
				String userid = (String) session.getAttribute("user");

				// this mathod is for append data to basicDetails jsp.
				//String productid=request.getParameter("removeid");
				//System.out.println("____________________________"+productid);
				//System.out.print(" ppppppppppppppppppppppppppppppppppppppp"+request.getParameter("buttonid"));
					 obj = handler.detailsAppend(getDataSource(request),request,response,userid);
					 response.getOutputStream().write(obj.toString().getBytes());
					} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		
	}
}
