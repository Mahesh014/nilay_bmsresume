package com.itech.bmsresume.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.json.simple.JSONArray;

import com.itech.bmsresume.actionform.AboutmeActionForm;
import com.itech.bmsresume.handler.AboutmeHandler;

public class AboutmeAction extends DispatchAction {
	
	
	AboutmeHandler handler = new AboutmeHandler(); 
	public ActionForward details (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception
			{
		
		AboutmeActionForm details = (AboutmeActionForm) form;
		
		HttpSession session = request.getSession();
		String userid = (String) session.getAttribute("user");
		System.out.println(userid+"<>");
		
		request.setAttribute("details", handler.details(details, getDataSource(request),userid));
				return mapping.findForward("success");
		
			}
	

public ActionForward detailsAppend(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
{
		try {

			JSONArray obj;
			HttpSession session = request.getSession();
			String userid = (String) session.getAttribute("user");

			// this mathod is for append data to basicDetails jsp.
			//String productid=request.getParameter("removeid");
			//System.out.println("____________________________"+productid);
			//System.out.print(" ppppppppppppppppppppppppppppppppppppppp"+request.getParameter("buttonid"));
				 obj = handler.detailsAppend(getDataSource(request),request,response,userid);
				 response.getOutputStream().write(obj.toString().getBytes());
				} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	
}
}
