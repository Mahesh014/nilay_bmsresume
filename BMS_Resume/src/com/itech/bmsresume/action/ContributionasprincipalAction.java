package com.itech.bmsresume.action;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.itech.bmsresume.actionform.ContributionasprincipalActionFrom;
import com.itech.bmsresume.handler.ContributionasprincipalHandler;

public class ContributionasprincipalAction extends DispatchAction {

	ContributionasprincipalActionFrom details = new ContributionasprincipalActionFrom();
	ContributionasprincipalHandler handler = new ContributionasprincipalHandler();
	
	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SQLException
	{
		HttpSession session = request.getSession();
		String userid = (String) session.getAttribute("user");
		System.out.println(userid+"<>");
		
		String str = "";
		String strength[] = request.getParameterValues("contributionasprincipal");
		String title = request.getParameter("title");
		
		System.out.println("+++++++++++++++++++"+title);

		for (int i = 0; i < strength.length; i++) {
			str = str + "~@~" + strength[i];

		}
		str = str.replaceFirst("~@~" , "");
		System.out.println(str);
		
		request.setAttribute("info", handler.add(details,title,getDataSource(request),userid,str));
		return list(mapping, form, request, response);
		
		
	}

	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SQLException {
					HttpSession session = request.getSession();
					String userid = (String) session.getAttribute("user");
					System.out.println(userid + "<>");
			
					List<ContributionasprincipalActionFrom> detailsList = handler.list(getDataSource(request),userid);
					request.setAttribute("roleList", detailsList);

			return mapping.findForward("success");
		}
	
	public ActionForward update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SQLException
	{
		ContributionasprincipalActionFrom details = new ContributionasprincipalActionFrom();
		HttpSession session = request.getSession();
		String userid = (String) session.getAttribute("user");
		
		String titleVal = request.getParameter("title");
		System.out.println("AAAAAAAAAAAAA"+titleVal);
		System.out.println(userid+"<>");
		
		String str = "";
		String strength[] = request.getParameterValues("contributionasprincipal");
		for (int i = 0; i < strength.length; i++) {
			str = str + "~@~" + strength[i];

		}
		str = str.replaceFirst("~@~" , "");
		System.out.println(str);
		
		int id=Integer.parseInt(request.getParameter("id"));	
		System.out.println(id+"ididididi");
		request.setAttribute("info", handler.update(details,getDataSource(request),userid,str,id,titleVal));
		return list(mapping, form, request, response);
		
		
	}
	
	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception{
		try {
			
			ContributionasprincipalActionFrom details = (ContributionasprincipalActionFrom)form;
			
			
			System.out.println(request.getParameter("userid")+"....");
			details.setUserid(request.getParameter("userid"));
			details.setId(Integer.parseInt(request.getParameter("id")));
			System.out.println("%%%%%%%%%%%"+request.getParameter("userid"));
			details.setActive(Boolean.parseBoolean(request.getParameter("active")));
			System.out.println("*******"+Boolean.parseBoolean(request.getParameter("active")));
		    request.setAttribute("status", handler.changeStatus(details, getDataSource(request)));
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		    return list(mapping, form, request, response);
	}
}
