package com.itech.bmsresume.action;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.json.simple.JSONArray;

import com.itech.bmsresume.actionform.HomePageActionForm;
import com.itech.bmsresume.handler.HomePageHandler;

public class HomePageAction extends DispatchAction {
	
	
	HomePageHandler handler= new HomePageHandler();
	
	
	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SQLException {
					/*HttpSession session = request.getSession();
					String userid = (String) session.getAttribute("user");
					System.out.println(userid + "<>");*/
			
					List<HomePageActionForm> detailsList = handler.list(getDataSource(request));
					request.setAttribute("roleList", detailsList);
					
					
					
			return mapping.findForward("success");
		}
	
	public ActionForward list1(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SQLException {
					/*HttpSession session = request.getSession();
					String userid = (String) session.getAttribute("user");
					System.out.println(userid + "<>");*/
			
				
					
					
					List<?> corecompetenices = handler.list1(getDataSource(request));
					request.setAttribute("corecompetenices", corecompetenices);
			return mapping.findForward("success");

}
	

	public ActionForward detailsAppend(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	{
			try {

				JSONArray obj;
//				HttpSession session = request.getSession();
//				String userid = (String) session.getAttribute("user");

				// this mathod is for append data to basicDetails jsp.
				//String productid=request.getParameter("removeid");
				//System.out.println("____________________________"+productid);
				//System.out.print(" ppppppppppppppppppppppppppppppppppppppp"+request.getParameter("buttonid"));
					 obj = handler.detailsAppend(getDataSource(request),request,response);
					 response.getOutputStream().write(obj.toString().getBytes());
					} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		
	}




}
