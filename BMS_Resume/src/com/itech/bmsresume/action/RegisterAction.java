package com.itech.bmsresume.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.itech.bmsresume.actionform.RegisterActionForm;
import com.itech.bmsresume.handler.RegisterHandler;

public class RegisterAction extends DispatchAction {

	RegisterHandler handler = new RegisterHandler();
	
	
	
	public ActionForward register (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception
			{
		RegisterActionForm formdetails = (RegisterActionForm) form;
		
		
		
		request.setAttribute("formdetails", handler.register(formdetails, getDataSource(request)));
				return mapping.findForward("success");
		
			}
}
