package com.itech.bmsresume.action;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.itech.bmsresume.actionform.SocialstandingActionForm;
import com.itech.bmsresume.handler.SocialstandingHandler;

public class SocialstandingAction extends DispatchAction {
	
	SocialstandingActionForm details = new SocialstandingActionForm();
	SocialstandingHandler handler = new SocialstandingHandler();
	
	
	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SQLException
	{
		HttpSession session = request.getSession();
		String userid = (String) session.getAttribute("user");
		System.out.println(userid+"<>");
		
		String str = "";
		
		String strength[] = request.getParameterValues("socialstanding");
		String title = request.getParameter("title");
		
		System.out.println("+++++++++++++++++++"+title);

		for (int i = 0; i < strength.length; i++) {
			str = str + "~@~" + strength[i];

		}
		str = str.replaceFirst("~@~" , "");
		System.out.println(str);
		
		request.setAttribute("info", handler.add(details,title,getDataSource(request),userid,str));
		return list(mapping, form, request, response);
		
		
	}
	
	public ActionForward update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SQLException
	{
		HttpSession session = request.getSession();
		String userid = (String) session.getAttribute("user");
		System.out.println(userid+"<>");
		String title=request.getParameter("title");
		
		String str = "";
		String strength[] = request.getParameterValues("socialstanding");
		for (int i = 0; i < strength.length; i++) {
			str = str + "~@~" + strength[i];

		}
		str = str.replaceFirst("~@~" , "");
		System.out.println(str);
		
		request.setAttribute("info", handler.update(details,getDataSource(request),userid,str));
		return list(mapping, form, request, response);
		
		
	}
	
	public ActionForward list(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) throws SQLException {
				HttpSession session = request.getSession();
				String userid = (String) session.getAttribute("user");
				System.out.println(userid + "<>");
		
				List<SocialstandingActionForm> detailsList = handler.list(getDataSource(request),userid);
				request.setAttribute("roleList", detailsList);

		return mapping.findForward("success");
	}
	
	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception{
		try {
			
			SocialstandingActionForm details = (SocialstandingActionForm)form;
			
	        String str  = request.getParameter("id");
	        System.out.println("aaaaaaaaaaaaaaaaaaaaa"+str);
	        
			details.setUserid(request.getParameter("userid"));
			details.setId(Integer.parseInt(request.getParameter("id")));
			System.out.println("%%%%%%%%%%%"+request.getParameter("userid")+"..."+request.getParameter("id"));
			details.setActive(Boolean.parseBoolean(request.getParameter("active")));
			System.out.println("*******"+Boolean.parseBoolean(request.getParameter("active")));
		    request.setAttribute("status", handler.changeStatus(details, getDataSource(request)));
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		    return list(mapping, form, request, response);
	}

}
