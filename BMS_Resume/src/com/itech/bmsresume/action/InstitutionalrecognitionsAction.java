package com.itech.bmsresume.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.json.simple.JSONArray;

import com.itech.bmsresume.actionform.InstitutionalrecognitionsActionForm;
import com.itech.bmsresume.handler.InstitutionalrecognitionsHandler;

public class InstitutionalrecognitionsAction extends DispatchAction {
	
	InstitutionalrecognitionsHandler handler = new InstitutionalrecognitionsHandler();
	public ActionForward details (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception
			{
		
		InstitutionalrecognitionsActionForm details =  (InstitutionalrecognitionsActionForm) form;
		
		HttpSession session = request.getSession();
		String userid = (String) session.getAttribute("user");
		System.out.println(userid+"<>");
		
		String str="";
		String skills[] =request.getParameterValues("institutionalrecognitionyear");
		for(int i=0; i<skills.length; i++){
			str = str+"~@~"+skills[i];
			
		}
		str= str.replaceFirst("~@~" , "");
		System.out.println(str);
		//----------------------------------------------------------------------------------
		String str1="";
		String skills1[] =request.getParameterValues("Institutional_recognition");
		for(int i=0; i<skills1.length; i++){
			str1 = str1+"~@~"+skills1[i];
			
		}
		str1= str1.replaceFirst("~@~" , "");
		System.out.println(str1);
		
		request.setAttribute("details", handler.details(details, getDataSource(request),userid, str,str1));
				return mapping.findForward("success");
		
			}
	
	
	public ActionForward detailsAppend(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	{
			try {

				JSONArray obj;
				HttpSession session = request.getSession();
				String userid = (String) session.getAttribute("user");

				// this mathod is for append data to basicDetails jsp.
				//String productid=request.getParameter("removeid");
				//System.out.println("____________________________"+productid);
				//System.out.print(" ppppppppppppppppppppppppppppppppppppppp"+request.getParameter("buttonid"));
					 obj = handler.detailsAppend(getDataSource(request),request,response,userid);
					 response.getOutputStream().write(obj.toString().getBytes());
					} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		
	}
}
