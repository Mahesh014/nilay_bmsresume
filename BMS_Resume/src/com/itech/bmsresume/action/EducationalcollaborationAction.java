package com.itech.bmsresume.action;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.upload.FormFile;

import com.itech.bmsresume.actionform.EducationalcollaborationActionForm;
import com.itech.bmsresume.handler.EducationalcollaborationHandler;

public class EducationalcollaborationAction extends DispatchAction {

	EducationalcollaborationHandler handler = new EducationalcollaborationHandler();
	/*public ActionForward details (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception
			{
		
		EducationalcollaborationActionForm details =  (EducationalcollaborationActionForm) form;
		
		HttpSession session = request.getSession();
		String userid = (String) session.getAttribute("user");
		System.out.println(userid+"<>");
		
		String filepath = "";
		String filename1 = "";
		System.out.println("Inside banner action");
		if (!details.getPhoto1().equals("")) {
			FormFile myfile = details.getPhoto1();
			System.out.println("..................");
			filepath = getServlet().getServletContext().getRealPath("/")
					+ "Employeeupload";
			// System.out.println(filePath+"filepaaaaaaaaath issssss");
			// we concate for store multiple file
			filename1 = myfile.getFileName();

			FileUploadDownloadUtil.fileUploader(myfile, filepath, filename1);
		}
		request.setAttribute("details", handler.details(details, getDataSource(request),filepath, filename1,userid));
				return mapping.findForward("success");
		
			}
	public ActionForward detailsAppend(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	{
			try {

				JSONArray obj;
				HttpSession session = request.getSession();
				String userid = (String) session.getAttribute("user");

				// this mathod is for append data to basicDetails jsp.
				//String productid=request.getParameter("removeid");
				//System.out.println("____________________________"+productid);
				//System.out.print(" ppppppppppppppppppppppppppppppppppppppp"+request.getParameter("buttonid"));
					 obj = handler.detailsAppend(getDataSource(request),request,response,userid);
					 response.getOutputStream().write(obj.toString().getBytes());
					} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		
	}*/

	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		HttpSession session = request.getSession();
		String userid = (String) session.getAttribute("user");
		System.out.println(userid+"<*********************>");
		EducationalcollaborationActionForm educationalForm= (EducationalcollaborationActionForm) form;
		
		String heading = request.getParameter("heading");
		String textarea = request.getParameter("textarea");
		String filepath = "";
		String filename1 = "";
		 
		
		 if (!educationalForm.getPhoto1().equals("")) {
				FormFile myfile = educationalForm.getPhoto1();
				System.out.println("..................");
				filepath = getServlet().getServletContext().getRealPath("/")+ "Employeeupload";
				// System.out.println(filePath+"filepaaaaaaaaath issssss");
				// we concate for store multiple file
				filename1 = myfile.getFileName();

				FileUploadDownloadUtil.fileUploader(myfile, filepath, filename1);
			}
	
				
		System.out.println("+++++++++++++++++++"+heading);
	
		request.setAttribute("info", handler.add(heading,textarea,getDataSource(request),userid, filename1, filepath));
		return list(mapping, form, request, response);
		//return null;

		
	}

public ActionForward list(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) throws SQLException {
				HttpSession session = request.getSession();
				String userid = (String) session.getAttribute("user");
				System.out.println(userid + "<>");
		
				List<EducationalcollaborationActionForm> detailsList = handler.list(getDataSource(request),userid);
				request.setAttribute("roleList", detailsList);

		return mapping.findForward("success");
	}


public ActionForward changestatus(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) throws Exception{
	try {
		
		EducationalcollaborationActionForm educationalForm = (EducationalcollaborationActionForm)form;
		educationalForm.setId(Integer.parseInt(request.getParameter("id")));

		educationalForm.setActive(Boolean.parseBoolean(request.getParameter("active")));
		System.out.println("*******"+Boolean.parseBoolean(request.getParameter("active")));
	    request.setAttribute("status", handler.changeStatus(educationalForm, getDataSource(request)));
	
	} catch (Exception e) {
		e.printStackTrace();
	}
	
	    return list(mapping, form, request, response);
}

public ActionForward update(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
  {
	 
	  try{
		  
			
			String heading = request.getParameter("heading");
			String textarea = request.getParameter("textarea");
			
				EducationalcollaborationActionForm educationalForm = (EducationalcollaborationActionForm)form;
				String filepath = "";
				String filename1 = "";
				 
				
				 if (!educationalForm.getPhoto1().equals("")) {
						FormFile myfile = educationalForm.getPhoto1();
						System.out.println("..................");
						filepath = getServlet().getServletContext().getRealPath("/")+ "Employeeupload";
					// we concate for store multiple file
						filename1 = myfile.getFileName();

						FileUploadDownloadUtil.fileUploader(myfile, filepath, filename1);
					}
				  
			      request.setAttribute("status", handler.update(educationalForm,getDataSource(request),heading,textarea,filepath,filename1));

			
			
			
			}catch (Exception e) {
				e.printStackTrace();
			}
	  
				return list(mapping, form, request, response);
  }
}


