package com.itech.bmsresume.action;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.upload.FormFile;

import com.itech.bmsresume.actionform.ActivitiesActionForm;
import com.itech.bmsresume.actionform.EducationalcollaborationActionForm;
import com.itech.bmsresume.handler.ActivitiesHandler;

public class ActivitiesAction extends DispatchAction {
	
	ActivitiesHandler handler = new ActivitiesHandler();
	
	/*public ActionForward details (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception
			{
		
		ActivitiesActionForm details =  (ActivitiesActionForm) form;
		
		HttpSession session = request.getSession();
		String userid = (String) session.getAttribute("user");
		System.out.println(userid+"<>");
		
		String filepath = "";
		String filename1 = "";
		System.out.println("Inside banner action");
		if (!details.getPhoto1().equals("")) {
			FormFile myfile = details.getPhoto1();
			System.out.println("..................");
			filepath = getServlet().getServletContext().getRealPath("/")
					+ "Employeeupload";
			// System.out.println(filePath+"filepaaaaaaaaath issssss");
			// we concate for store multiple file
			filename1 = myfile.getFileName();

			FileUploadDownloadUtil.fileUploader(myfile, filepath, filename1);
		}
		request.setAttribute("details", handler.details(details, getDataSource(request),filepath, filename1,userid));
				return mapping.findForward("success");
		
			}

	
	public ActionForward detailsAppend(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	{
			try {

				JSONArray obj;
				HttpSession session = request.getSession();
				String userid = (String) session.getAttribute("user");

				// this mathod is for append data to basicDetails jsp.
				//String productid=request.getParameter("removeid");
				//System.out.println("____________________________"+productid);
				//System.out.print(" ppppppppppppppppppppppppppppppppppppppp"+request.getParameter("buttonid"));
					 obj = handler.detailsAppend(getDataSource(request),request,response,userid);
					 response.getOutputStream().write(obj.toString().getBytes());
					} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		
	}*/
	
	
	ActivitiesActionForm details = new ActivitiesActionForm();
	
	public ActionForward add(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		try
		{
		HttpSession session = request.getSession();
		String userid = (String) session.getAttribute("user");
		System.out.println(userid+"<>");
		details =(ActivitiesActionForm) form;
		
		String filepath = "";
		String filename1 = "";
		System.out.println("Inside activity action"+ details.getPhoto1());
		if (!details.getPhoto1().equals("")) {
			FormFile myfile = details.getPhoto1();
			System.out.println("..................");
			filepath = getServlet().getServletContext().getRealPath("/")+ "Employeeupload";
			// System.out.println(filePath+"filepaaaaaaaaath issssss");
			// we concate for store multiple file
			filename1 = myfile.getFileName();

			FileUploadDownloadUtil.fileUploader(myfile, filepath, filename1);
		}
		
		
		request.setAttribute("info", handler.add(details,getDataSource(request),userid ,filename1, filepath));
		
		}catch (Exception e) {
			e.printStackTrace();		}
		
		return list(mapping, form, request, response);
		
		
	}

	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SQLException {
					HttpSession session = request.getSession();
					String userid = (String) session.getAttribute("user");
					System.out.println(userid + "<>");
			
					List<ActivitiesActionForm> detailsList = handler.list(getDataSource(request),userid);
					request.setAttribute("roleList", detailsList);

			return mapping.findForward("success");
		}
	
	public ActionForward update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		try
		{
		
		
			String heading = request.getParameter("heading");
			String activity = request.getParameter("activity");
			
			ActivitiesActionForm educationalForm = (ActivitiesActionForm)form;
				String filepath = "";
				String filename1 = "";
				 
				
				 if (!educationalForm.getPhoto1().equals("")) {
						FormFile myfile = educationalForm.getPhoto1();
						System.out.println("..................");
						filepath = getServlet().getServletContext().getRealPath("/")+ "Employeeupload";
					// we concate for store multiple file
						filename1 = myfile.getFileName();

						FileUploadDownloadUtil.fileUploader(myfile, filepath, filename1);
					}
				  
			      request.setAttribute("status", handler.update(educationalForm,getDataSource(request),heading,activity,filepath,filename1));

		
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return list(mapping, form, request,response);
		
		
	}
	
	
	
	public ActionForward changestatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception{
		try {
			
			ActivitiesActionForm educationalForm = (ActivitiesActionForm)form;
			educationalForm.setId(Integer.parseInt(request.getParameter("id")));
			educationalForm.setActive(Boolean.parseBoolean(request.getParameter("active")));
			System.out.println("*******"+Boolean.parseBoolean(request.getParameter("active")));
		    request.setAttribute("status", handler.changeStatus(educationalForm, getDataSource(request)));
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		    return list(mapping, form, request, response);
	}



}
