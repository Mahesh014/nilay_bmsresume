package com.itech.bmsresume.action;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.itech.bmsresume.actionform.TeqipUIActionForm;
import com.itech.bmsresume.handler.TeqipUIHandler;

public class TeqipUIAction extends DispatchAction {
	
	TeqipUIHandler handler = new TeqipUIHandler();
	
	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SQLException {
					/*HttpSession session = request.getSession();
					String userid = (String) session.getAttribute("user");
					System.out.println(userid + "<>");*/
			
					List<TeqipUIActionForm> detailsList = handler.list(getDataSource(request));
					request.setAttribute("roleList", detailsList);
					
					
					
			return mapping.findForward("success");
		}

}
