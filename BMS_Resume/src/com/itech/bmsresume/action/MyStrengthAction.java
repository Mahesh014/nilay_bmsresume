package com.itech.bmsresume.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.json.simple.JSONArray;

import com.itech.bmsresume.actionform.MyStrengthActionForm;
import com.itech.bmsresume.handler.MystrengthHandler;

public class MyStrengthAction extends DispatchAction {
	
	MystrengthHandler handler = new MystrengthHandler();
	public ActionForward details (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception
			{
		
		MyStrengthActionForm details =  (MyStrengthActionForm) form;
		
		HttpSession session = request.getSession();
		String userid = (String) session.getAttribute("user");
		System.out.println(userid+"<>");
		
		String str = "";
		String strength[] = request.getParameterValues("mystrength");
		for (int i = 0; i < strength.length; i++) {
			str = str + "~@~" + strength[i];

		}
		str = str.replaceFirst("~@~" , "");
		System.out.println(str);
		
		request.setAttribute("details", handler.details(details, getDataSource(request),userid,str));
				return mapping.findForward("success");
		
			}
	
	
	public ActionForward detailsAppend(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	{
			try {

				JSONArray obj;
				HttpSession session = request.getSession();
				String userid = (String) session.getAttribute("user");

				//this mathod is for append data to basicDetails jsp.
				//String productid=request.getParameter("removeid");
				//System.out.println("____________________________"+productid);
				//System.out.print(" ppppppppppppppppppppppppppppppppppppppp"+request.getParameter("buttonid"));
					 obj = handler.detailsAppend(getDataSource(request),request,response,userid);
					 response.getOutputStream().write(obj.toString().getBytes());
					} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		
	}
}
