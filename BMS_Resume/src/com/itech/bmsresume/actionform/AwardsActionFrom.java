package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class AwardsActionFrom extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String award;
	private int awardyear;
	public String getAward() {
		return award;
	}
	public void setAward(String award) {
		this.award = award;
	}
	public int getAwardyear() {
		return awardyear;
	}
	public void setAwardyear(int awardyear) {
		this.awardyear = awardyear;
	} 

}
