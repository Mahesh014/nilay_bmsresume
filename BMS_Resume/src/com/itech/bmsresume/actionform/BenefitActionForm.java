package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class BenefitActionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String benefits;

	public void setBenefits(String benefits) {
		this.benefits = benefits;
	}

	public String getBenefits() {
		return benefits;
	}
}
