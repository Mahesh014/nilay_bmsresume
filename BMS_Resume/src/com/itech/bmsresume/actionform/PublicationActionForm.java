package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class PublicationActionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String paperpublished;
	public void setPaperpublished(String paperpublished) {
		this.paperpublished = paperpublished;
	}
	public String getPaperpublished() {
		return paperpublished;
	}

}
