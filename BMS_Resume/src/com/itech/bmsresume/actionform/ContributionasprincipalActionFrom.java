package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class ContributionasprincipalActionFrom extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	 private String title, contributionasprincipal,userid;
	 private boolean active;
	 private int id;
	 
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContributionasprincipal() {
		return contributionasprincipal;
	}
	public void setContributionasprincipal(String contributionasprincipal) {
		this.contributionasprincipal = contributionasprincipal;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public boolean isActive() {
		return active;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUserid() {
		return userid;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	
}
