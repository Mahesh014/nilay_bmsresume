package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class AcademicInnovationActionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String academic_innovation;
	public void setAcademic_innovation(String academic_innovation) {
		this.academic_innovation = academic_innovation;
	}
	public String getAcademic_innovation() {
		return academic_innovation;
	}

}
