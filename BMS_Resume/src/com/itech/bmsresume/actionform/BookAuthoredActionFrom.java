package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class BookAuthoredActionFrom extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String booktitle, publisher;
	private int publisheryear;
	public String getBooktitle() {
		return booktitle;
	}
	public void setBooktitle(String booktitle) {
		this.booktitle = booktitle;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public int getPublisheryear() {
		return publisheryear;
	}
	public void setPublisheryear(int publisheryear) {
		this.publisheryear = publisheryear;
	}

}
