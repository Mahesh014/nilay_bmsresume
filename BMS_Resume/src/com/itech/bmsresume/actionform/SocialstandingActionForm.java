package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class SocialstandingActionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		
	private String title,socialstanding,userid;
	private boolean active;
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSocialstanding() {
		return socialstanding;
	}

	public void setSocialstanding(String socialstanding) {
		this.socialstanding = socialstanding;
	}

}
