package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class SocialmediaActionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String flink,tlink,ylink,glink;
	public String getFlink() {
		return flink;
	}
	public void setFlink(String flink) {
		this.flink = flink;
	}
	public String getTlink() {
		return tlink;
	}
	public void setTlink(String tlink) {
		this.tlink = tlink;
	}
	public String getYlink() {
		return ylink;
	}
	public void setYlink(String ylink) {
		this.ylink = ylink;
	}
	public String getGlink() {
		return glink;
	}
	public void setGlink(String glink) {
		this.glink = glink;
	}

}
