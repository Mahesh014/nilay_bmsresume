package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class CorecompetenciesActionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String corecompetenices;
	public void setCorecompetenices(String corecompetenices) {
		this.corecompetenices = corecompetenices;
	}
	public String getCorecompetenices() {
		return corecompetenices;
	}

}
