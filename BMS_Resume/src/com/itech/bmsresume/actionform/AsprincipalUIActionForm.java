package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class AsprincipalUIActionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String title,contributionasprincipal,userid,active,contributionasprincipals;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContributionasprincipal() {
		return contributionasprincipal;
	}

	public void setContributionasprincipal(String contributionasprincipal) {
		this.contributionasprincipal = contributionasprincipal;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public void setContributionasprincipals(String contributionasprincipals) {
		this.contributionasprincipals = contributionasprincipals;
	}

	public String getContributionasprincipals() {
		return contributionasprincipals;
	}
 
}
