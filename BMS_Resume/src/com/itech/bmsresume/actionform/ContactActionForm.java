package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class ContactActionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String name,designationcompany,address,citypincode,emailid;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCitypincode() {
		return citypincode;
	}

	public void setCitypincode(String citypincode) {
		this.citypincode = citypincode;
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public void setDesignationcompany(String designationcompany) {
		this.designationcompany = designationcompany;
	}

	public String getDesignationcompany() {
		return designationcompany;
	}

}
