package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class ActivitiesActionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String getHeading() {
		return heading;
	}
	public void setHeading(String heading) {
		this.heading = heading;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public FormFile getPhoto1() {
		return photo1;
	}
	public void setPhoto1(FormFile photo1) {
		this.photo1 = photo1;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getFilename() {
		return filename;
	}
	private String heading,photo,activity,userid,filename;
	private FormFile photo1;
	private boolean active;
	private int id;
}
