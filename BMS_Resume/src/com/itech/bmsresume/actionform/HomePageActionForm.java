package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class HomePageActionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	
	private String aboutme,title,socialstanding;
	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public String getSocialstanding() {
		return socialstanding;
	}



	public void setSocialstanding(String socialstanding) {
		this.socialstanding = socialstanding;
	}



	private String corecompetenices[];


	public String getAboutme() {
		return aboutme;
	}



	public void setAboutme(String aboutme) {
		this.aboutme = aboutme;
	}



	public void setCorecompetenices(String[] corecompetenices) {
		this.corecompetenices = corecompetenices;
	}



	public String[] getCorecompetenices() {
		return corecompetenices;
	}

}
