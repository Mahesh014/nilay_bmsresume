package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class ImportantinstitutionalRecognitionsActionform extends ActionForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String important_institutional_recognition;

	public void setImportant_institutional_recognition(
			String important_institutional_recognition) {
		this.important_institutional_recognition = important_institutional_recognition;
	}

	public String getImportant_institutional_recognition() {
		return important_institutional_recognition;
	}
	
}
