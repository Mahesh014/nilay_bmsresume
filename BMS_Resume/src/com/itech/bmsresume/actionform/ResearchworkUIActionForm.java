package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class ResearchworkUIActionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String getResearch_work() {
		return research_work;
	}
	public void setResearch_work(String researchWork) {
		research_work = researchWork;
	}
	public String getHeading() {
		return heading;
	}
	public void setHeading(String heading) {
		this.heading = heading;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	private String research_work,heading,userid;
	private boolean active;

}
