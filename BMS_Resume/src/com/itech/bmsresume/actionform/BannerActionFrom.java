package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class BannerActionFrom extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name, designation, worksat,photo;
	private FormFile photo1;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getWorksat() {
		return worksat;
	}
	public void setWorksat(String worksat) {
		this.worksat = worksat;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public FormFile getPhoto1() {
		return photo1;
	}
	public void setPhoto1(FormFile photo1) {
		this.photo1 = photo1;
	}
	
	
}
