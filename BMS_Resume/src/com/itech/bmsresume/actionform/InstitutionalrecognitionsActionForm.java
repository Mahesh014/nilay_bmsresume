package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class InstitutionalrecognitionsActionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String Institutional_recognition,institutionalrecognitionyear;

	public String getInstitutionalrecognitionyear() {
		return institutionalrecognitionyear;
	}

	public void setInstitutionalrecognitionyear(String institutionalrecognitionyear) {
		this.institutionalrecognitionyear = institutionalrecognitionyear;
	}

	public void setInstitutional_recognition(String institutional_recognition) {
		Institutional_recognition = institutional_recognition;
	}

	public String getInstitutional_recognition() {
		return Institutional_recognition;
	}
	

}
