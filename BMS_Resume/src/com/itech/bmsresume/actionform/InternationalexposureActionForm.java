package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class InternationalexposureActionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String internationalexposure;
	public void setInternationalexposure(String internationalexposure) {
		this.internationalexposure = internationalexposure;
	}
	public String getInternationalexposure() {
		return internationalexposure;
	}
	

}
