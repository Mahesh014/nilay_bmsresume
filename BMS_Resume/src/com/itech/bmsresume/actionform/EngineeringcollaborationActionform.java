package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class EngineeringcollaborationActionform extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String userphoto;
	private FormFile photo1;
	private String heading,name,userid;
	private boolean active;
	private int id;
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserphoto() {
		return userphoto;
	}
	public void setUserphoto(String userphoto) {
		this.userphoto = userphoto;
	}
	public FormFile getPhoto1() {
		return photo1;
	}
	public void setPhoto1(FormFile photo1) {
		this.photo1 = photo1;
	}
	public String getHeading() {
		return heading;
	}
	public void setHeading(String heading) {
		this.heading = heading;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public boolean isActive() {
		return active;
	}
	
}
