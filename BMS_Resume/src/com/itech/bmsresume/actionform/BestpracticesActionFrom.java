package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class BestpracticesActionFrom extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String bestpracticesatbms;

	public void setBestpracticesatbms(String bestpracticesatbms) {
		this.bestpracticesatbms = bestpracticesatbms;
	}

	public String getBestpracticesatbms() {
		return bestpracticesatbms;
	}

}
