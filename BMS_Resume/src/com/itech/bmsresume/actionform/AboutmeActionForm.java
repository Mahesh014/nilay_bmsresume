package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class AboutmeActionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String aboutme;

	public void setAboutme(String aboutme) {
		this.aboutme = aboutme;
	}

	public String getAboutme() {
		return aboutme;
	}

}
