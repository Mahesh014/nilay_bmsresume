package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class ResearchWorkActioinForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String researchwork,heading,userid;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	private boolean active;

	private int id;
	
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public void setResearchwork(String researchwork) {
		this.researchwork = researchwork;
	}
	public String getResearchwork() {
		return researchwork;
	}
	public void setHeading(String heading) {
		this.heading = heading;
	}
	public String getHeading() {
		return heading;
	}

}
