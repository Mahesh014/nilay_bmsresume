package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class MyStrengthActionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String mystrength;
	public void setMystrength(String mystrength) {
		this.mystrength = mystrength;
	}
	public String getMystrength() {
		return mystrength;
	}

}
