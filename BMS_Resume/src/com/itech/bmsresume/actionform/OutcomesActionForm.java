package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class OutcomesActionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String outcome;
	private int outcomeyear;
	public String getOutcome() {
		return outcome;
	}
	public void setOutcome(String outcome) {
		this.outcome = outcome;
	}
	public int getOutcomeyear() {
		return outcomeyear;
	}
	public void setOutcomeyear(int outcomeyear) {
		this.outcomeyear = outcomeyear;
	}

}
