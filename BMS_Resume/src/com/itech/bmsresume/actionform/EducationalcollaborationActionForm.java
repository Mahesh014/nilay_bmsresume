/**
 * 
 */
package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

/**
 * @author nilay
 *
 */
public class EducationalcollaborationActionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private FormFile photo1;
	
	private String heading,textarea,userid;
	private String filename;
	private int id;
	
	
	

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	
	
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	private boolean active;
		
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getHeading() {
		return heading;
	}
	public void setHeading(String heading) {
		this.heading = heading;
	}

	public FormFile getPhoto1() {
		return photo1;
	}
	public void setPhoto1(FormFile photo1) {
		this.photo1 = photo1;
	}
	public String getEducationalcollaboration() {
		return educationalcollaboration;
	}
	public void setEducationalcollaboration(String educationalcollaboration) {
		this.educationalcollaboration = educationalcollaboration;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getFilename() {
		return filename;
	}
	public void setTextarea(String textarea) {
		this.textarea = textarea;
	}
	public String getTextarea() {
		return textarea;
	}
	private String educationalcollaboration, photo;

}
