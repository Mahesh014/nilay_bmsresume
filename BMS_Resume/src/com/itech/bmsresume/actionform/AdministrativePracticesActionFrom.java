package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;

public class AdministrativePracticesActionFrom extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String administrative_practices;
	public void setAdministrative_practices(String administrative_practices) {
		this.administrative_practices = administrative_practices;
	}
	public String getAdministrative_practices() {
		return administrative_practices;
	}

}
