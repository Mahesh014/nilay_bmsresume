package com.itech.bmsresume.actionform;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class ResumeuploadActionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private FormFile doc,pdf;

	public FormFile getPdf() {
		return pdf;
	}

	public void setPdf(FormFile pdf) {
		this.pdf = pdf;
	}

	public void setDoc(FormFile doc) {
		this.doc = doc;
	}

	public FormFile getDoc() {
		return doc;
	}

	

}
