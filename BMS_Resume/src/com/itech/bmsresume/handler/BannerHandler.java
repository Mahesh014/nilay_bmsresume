package com.itech.bmsresume.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.bmsresume.actionform.BannerActionFrom;
import com.itech.bmsresume.dao.BannerDao;

public class BannerHandler {

	BannerDao dao = new BannerDao();

	public Object details(BannerActionFrom details, DataSource dataSource,
			String filepath, String filename1, String userid) {
		// TODO Auto-generated method stub
		return dao.details(details, dataSource, filename1, filepath,userid);
	}

	
	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		System.out.println("Inside the banner handler");
		return dao.detailsAppend(dataSource,request,response,userid);
	}
}
