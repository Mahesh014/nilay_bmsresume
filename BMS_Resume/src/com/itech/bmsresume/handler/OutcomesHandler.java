package com.itech.bmsresume.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.bmsresume.actionform.OutcomesActionForm;
import com.itech.bmsresume.dao.OutcomesDao;

public class OutcomesHandler {
	
	OutcomesDao dao =  new OutcomesDao();

	public Object details(OutcomesActionForm details, DataSource dataSource, String userid, String str, String str1) {
		// TODO Auto-generated method stub
		return dao.details(details, dataSource,userid,str,str1);
	}

	
	
	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		// TODO Auto-generated method stub
		return dao.detailsAppend(dataSource,request,response,userid);
	}
}
