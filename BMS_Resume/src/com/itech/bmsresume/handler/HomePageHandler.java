package com.itech.bmsresume.handler;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.bmsresume.actionform.HomePageActionForm;
import com.itech.bmsresume.dao.HomePageDao;

public class HomePageHandler {
	
	HomePageDao dao = new HomePageDao();

	public List<HomePageActionForm> list(DataSource dataSource) throws SQLException {
		// TODO Auto-generated method stub
		return dao.list(dataSource);
	}

	public List<?> list1(DataSource dataSource) throws SQLException {
		// TODO Auto-generated method stub
		return dao.list1(dataSource);
	}
	
	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		return dao.detailsAppend(dataSource,request,response);
	}

}
