package com.itech.bmsresume.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.bmsresume.actionform.MyStrengthActionForm;
import com.itech.bmsresume.dao.MystrengthDao;

public class MystrengthHandler {
	
	MystrengthDao dao = new MystrengthDao();

	public Object details(MyStrengthActionForm details, DataSource dataSource, String userid, String str) {
		// TODO Auto-generated method stub
		return dao.details(details,dataSource,userid,str);
	}

	
	
	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		// TODO Auto-generated method stub
		return dao.detailsAppend(dataSource,request,response,userid);
	}
}
