package com.itech.bmsresume.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.bmsresume.actionform.AcademicInnovationActionForm;
import com.itech.bmsresume.dao.AcademicInnovationDao;

public class AcademicInnovationHandler {
	
	AcademicInnovationDao dao = new AcademicInnovationDao();
	public Object details(AcademicInnovationActionForm details, DataSource dataSource, String userid, String str) {
		// TODO Auto-generated method stub
		return dao.details(details,dataSource,userid,str);
	}
	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		// TODO Auto-generated method stub
		return dao.detailsAppend(dataSource,request,response,userid);
	}

}
