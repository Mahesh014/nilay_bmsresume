package com.itech.bmsresume.handler;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.EngineeringorganizationUIActionForm;
import com.itech.bmsresume.dao.EngineeringorganizationUIDao;

public class EngineeringorganizationUIHandler {

	EngineeringorganizationUIDao dao = new EngineeringorganizationUIDao();
	
	public List<EngineeringorganizationUIActionForm> list(DataSource dataSource) throws SQLException {
		// TODO Auto-generated method stub
		return dao.list(dataSource);
	}
	
}