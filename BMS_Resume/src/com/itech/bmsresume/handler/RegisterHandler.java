package com.itech.bmsresume.handler;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.RegisterActionForm;
import com.itech.bmsresume.dao.RegisterDao;

public class RegisterHandler {
	
	RegisterDao dao = new RegisterDao();

	public Object register(RegisterActionForm formdetails, DataSource dataSource) {
		// TODO Auto-generated method stub
		return dao.register(formdetails,dataSource);
	}
	
}