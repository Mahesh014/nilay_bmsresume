package com.itech.bmsresume.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.bmsresume.actionform.AdministrativePracticesActionFrom;
import com.itech.bmsresume.dao.AdministrativePracticesDao;

public class AdministrativePracticesHandler {

	AdministrativePracticesDao dao = new AdministrativePracticesDao();
	public Object details(AdministrativePracticesActionFrom details,
			DataSource dataSource, String userid, String str) {
		// TODO Auto-generated method stub
		return dao.details(details, dataSource,userid,str);
	}

	
	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		// TODO Auto-generated method stub
		return dao.detailsAppend(dataSource,request,response,userid);
	}
}
