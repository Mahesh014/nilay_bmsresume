package com.itech.bmsresume.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.bmsresume.actionform.SocialmediaActionForm;
import com.itech.bmsresume.dao.SocialmediaDao;

public class SocialmediaHandler {
	
	SocialmediaDao dao= new SocialmediaDao();
	
	public Object details(SocialmediaActionForm details, DataSource dataSource,
			String userid) {
		// TODO Auto-generated method stub
		return dao.details(details,dataSource,userid);
	}

	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		// TODO Auto-generated method stub
		return dao.detailsAppend(dataSource,request,response,userid);
	}
	public Object update(SocialmediaActionForm details, DataSource dataSource,
			String userid) {
		// TODO Auto-generated method stub
		return dao.update(details,dataSource,userid);
	}

}
