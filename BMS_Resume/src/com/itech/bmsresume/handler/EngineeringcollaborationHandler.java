package com.itech.bmsresume.handler;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.EducationalcollaborationActionForm;
import com.itech.bmsresume.actionform.EngineeringcollaborationActionform;
import com.itech.bmsresume.dao.EngineeringcollaborationDao;

public class EngineeringcollaborationHandler {

	EngineeringcollaborationDao dao= new EngineeringcollaborationDao();

	/*public Object details(EngineeringcollaborationActionform details,
			DataSource dataSource, String filepath, String filename1, String userid) {
		// TODO Auto-generated method stub
		return dao.details(details,dataSource,filepath, filename1,userid);
	}
	
	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		// TODO Auto-generated method stub
		return dao.detailsAppend(dataSource,request,response,userid);
	}*/

	public Object add(String heading, String textarea, DataSource dataSource,
			String userid, String fileName1, String filePath) {
		// TODO Auto-generated method stub
		return dao.add(heading,textarea, dataSource,userid,fileName1,filePath);
	}
	
	public List<EngineeringcollaborationActionform> list(DataSource dataSource, String userid) throws SQLException {
		return dao.list(dataSource,userid);
	}

	public String update(EngineeringcollaborationActionform details, DataSource dataSource, String userid, String heading, String textarea, String filename1, String filepath) {

		return dao.update(details, dataSource, userid, heading, textarea,filename1, filepath);
	}
	
	public Object changeStatus(EngineeringcollaborationActionform details, DataSource dataSource) {
		// TODO Auto-generated method stub
		return dao.changeStatus(details, dataSource);
	}
}
