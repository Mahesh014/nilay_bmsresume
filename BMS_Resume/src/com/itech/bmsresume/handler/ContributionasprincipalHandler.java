package com.itech.bmsresume.handler;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.ContributionasprincipalActionFrom;
import com.itech.bmsresume.dao.ContributionasprincipalDao;

public class ContributionasprincipalHandler {

	ContributionasprincipalDao dao= new ContributionasprincipalDao();

	public Object add(ContributionasprincipalActionFrom details, String title,
			DataSource dataSource, String userid, String str) {
		return dao.add(details,title,dataSource,userid,str);
	}
	
	public Object update(ContributionasprincipalActionFrom details, DataSource dataSource, String userid, String str, int id, String titleVal) {
		return dao.update(details, dataSource, userid, str,id,titleVal);
	}

	public List<ContributionasprincipalActionFrom> list(DataSource dataSource,String userid) throws SQLException {
		
		return dao.list(dataSource,userid);
	}
	
	public Object changeStatus(ContributionasprincipalActionFrom details,
			DataSource dataSource) {
		
		return dao.changeStatus(details,dataSource);
	}
}
