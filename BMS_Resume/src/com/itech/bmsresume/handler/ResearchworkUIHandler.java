package com.itech.bmsresume.handler;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.ResearchworkUIActionForm;
import com.itech.bmsresume.dao.ResearchWorkUIDao;

public class ResearchworkUIHandler {
	
	ResearchWorkUIDao dao = new ResearchWorkUIDao();

	
	public List<ResearchworkUIActionForm> list(DataSource dataSource) throws SQLException {
		// TODO Auto-generated method stub
		return dao.list(dataSource);
	}
}
