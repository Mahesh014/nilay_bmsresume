package com.itech.bmsresume.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.bmsresume.actionform.BookAuthoredActionFrom;
import com.itech.bmsresume.dao.BookAuthoredDao;

public class BookAuthoredHandler {

	BookAuthoredDao dao = new BookAuthoredDao();
	public Object details(BookAuthoredActionFrom details, DataSource dataSource, String userid, String str, String str1, String str2) {
		
		return dao.details(details, dataSource,userid,str,str1,str2);
	}

	
	
	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		// TODO Auto-generated method stub
		return dao.detailsAppend(dataSource,request,response,userid);
	}



	public Object update(BookAuthoredActionFrom details, DataSource dataSource,
			String userid, String str, String str1, String str2) {
		// TODO Auto-generated method stub
		return dao.update(details, dataSource,userid,str,str1,str2);
	}
}
