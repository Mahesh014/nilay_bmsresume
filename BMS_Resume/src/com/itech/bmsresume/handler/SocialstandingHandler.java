package com.itech.bmsresume.handler;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.SocialstandingActionForm;
import com.itech.bmsresume.dao.SocialStandingDao;

public class SocialstandingHandler {
	
	SocialStandingDao dao =  new SocialStandingDao();

	public Object add(SocialstandingActionForm details,String title, DataSource dataSource,
			String userid, String str) {
		
		System.out.println("inside social stading insert dao");
		return dao.add(details,title, dataSource,userid, str);
	}

	public Object update(SocialstandingActionForm details,
			DataSource dataSource, String userid, String str) {
		System.out.println("inside social stading update dao");
		return dao.update(details, dataSource,userid, str);
	}

	public List<SocialstandingActionForm> list(DataSource dataSource, String userid) throws SQLException {
		return dao.list(dataSource,userid);
	}

	public Object changeStatus(SocialstandingActionForm details,
			DataSource dataSource) {
		
		return dao.changeStatus(details,dataSource);
	}

}
