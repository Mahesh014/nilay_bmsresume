package com.itech.bmsresume.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.bmsresume.actionform.ResumeuploadActionForm;
import com.itech.bmsresume.dao.ResumeuploadDao;

public class ResumeuploadHandler {

	ResumeuploadDao dao =new ResumeuploadDao();

	public Object details(ResumeuploadActionForm details,
			DataSource dataSource, String filepath, String filename1, String filepath1,String filename11,String userid) {
		// TODO Auto-generated method stub
		return dao.details(details,dataSource,filepath,filename1,filepath1,filename11,userid);
	}

	public Object update(ResumeuploadActionForm details, DataSource dataSource,
			String filepath, String filename1, String filepath1,
			String filename11, String userid) {
		// TODO Auto-generated method stub
		return dao.update(details,dataSource,filepath,filename1,filepath1,filename11,userid);
	}
	
	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		System.out.println("Inside the banner handler");
		return dao.detailsAppend(dataSource,request,response,userid);
	}
}
