package com.itech.bmsresume.handler;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.TeqipUIActionForm;
import com.itech.bmsresume.dao.TeqipUIDao;

public class TeqipUIHandler {

	TeqipUIDao dao = new TeqipUIDao();
	
	public List<TeqipUIActionForm> list(DataSource dataSource) throws SQLException {
		// TODO Auto-generated method stub
		return dao.list(dataSource);
	}
}
