package com.itech.bmsresume.handler;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.AsprincipalUIActionForm;
import com.itech.bmsresume.dao.AsprincipalUIDao;

public class AsprincipalUIHandler {
	
	AsprincipalUIDao dao = new AsprincipalUIDao();
	
	public List<AsprincipalUIActionForm> list(DataSource dataSource) throws SQLException {
		// TODO Auto-generated method stub
		return dao.list(dataSource);
	}

}
