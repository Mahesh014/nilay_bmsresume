package com.itech.bmsresume.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.bmsresume.actionform.ImportantinstitutionalRecognitionsActionform;
import com.itech.bmsresume.dao.ImportantinstitutionalRecognitionDao;

public class ImportantinstitutionalRecognitionsHandler {
	
	ImportantinstitutionalRecognitionDao dao = new ImportantinstitutionalRecognitionDao();

	public Object details(ImportantinstitutionalRecognitionsActionform details,
			DataSource dataSource, String userid, String str) {
		
		return dao.details(details, dataSource,userid,str);
	}

	
	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		// TODO Auto-generated method stub
		return dao.detailsAppend(dataSource,request,response,userid);
	}
}
