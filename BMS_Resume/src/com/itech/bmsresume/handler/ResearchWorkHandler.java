package com.itech.bmsresume.handler;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.EducationalcollaborationActionForm;
import com.itech.bmsresume.actionform.ResearchWorkActioinForm;
import com.itech.bmsresume.dao.ResearchworkDao;

public class ResearchWorkHandler {
	
	ResearchworkDao dao = new ResearchworkDao();

	/*public Object details(ResearchWorkActioinForm details, DataSource dataSource, String userid) {
		// TODO Auto-generated method stub
		return dao.details(details,dataSource,userid);
	}
	
	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		// TODO Auto-generated method stub
		return dao.detailsAppend(dataSource,request,response,userid);
	}*/

	public Object add(ResearchWorkActioinForm details, String heading,
			DataSource dataSource, String userid, String str) {
		
		return dao.add(details,dataSource,userid,str,heading);
	}

	public List<ResearchWorkActioinForm> list(DataSource dataSource,
			String userid) throws SQLException {
		return dao.list(dataSource,userid);
	}

	public Object update(ResearchWorkActioinForm details,
			DataSource dataSource, String userid, String str, int id) {
		
		return dao.update(details,dataSource,userid,str,id);
	}
	
	public Object changeStatus(ResearchWorkActioinForm educationalForm, DataSource dataSource) {
		// TODO Auto-generated method stub
		return dao.changeStatus(educationalForm, dataSource);
	}

}
