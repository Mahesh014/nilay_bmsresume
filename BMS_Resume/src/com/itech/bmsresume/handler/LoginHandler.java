package com.itech.bmsresume.handler;

import java.util.List;

import javax.sql.DataSource;
import com.itech.bmsresume.actionform.RegisterActionForm;
import com.itech.bmsresume.dao.LoginDao;

public class LoginHandler {
	
	LoginDao dao = new LoginDao();

	public List<RegisterActionForm> login(RegisterActionForm formdetails,DataSource dataSource) {
		// TODO Auto-generated method stub
		System.out.println("inside LoginHandler Login");
		return dao.login(formdetails, dataSource);
	}

	public String changepassword(
			RegisterActionForm formdetails, DataSource dataSource) {
		
		System.out.println("inside LoginHandler changepassword");
		return dao.changepassword(formdetails, dataSource);
	}


}
