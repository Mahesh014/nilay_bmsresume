package com.itech.bmsresume.handler;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.EducationalcollaborationActionForm;
import com.itech.bmsresume.dao.EducationalcollaborationDao;

public class EducationalcollaborationHandler {

	EducationalcollaborationDao dao= new EducationalcollaborationDao();

	/*public Object details(EducationalcollaborationActionForm details,
			DataSource dataSource, String filepath, String filename1, String userid) {
		// TODO Auto-generated method stub
		return dao.details(details, dataSource,filepath, filename1,userid);
	}
	
	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		// TODO Auto-generated method stub
		return dao.detailsAppend(dataSource,request,response,userid);
	}*/
	
	public Object add(String heading, String textarea, DataSource dataSource,
			String userid, String fileName1, String filePath) {
		// TODO Auto-generated method stub
		return dao.add(heading,textarea, dataSource,userid,fileName1,filePath);
	}
	
	public List<EducationalcollaborationActionForm> list(DataSource dataSource, String userid) throws SQLException {
		return dao.list(dataSource,userid);
	}

	public String update(EducationalcollaborationActionForm educationalForm, DataSource dataSource, String heading, String textarea,String filepath,String filename1) {

		return dao.update(educationalForm, dataSource, heading, textarea,filepath,filename1);
	}
	
	public Object changeStatus(EducationalcollaborationActionForm educationalForm, DataSource dataSource) {
		// TODO Auto-generated method stub
		return dao.changeStatus(educationalForm, dataSource);
	}
}
