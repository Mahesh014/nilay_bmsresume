package com.itech.bmsresume.handler;

import java.util.List;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.ActivitiesActionForm;
import com.itech.bmsresume.actionform.ContributionasprincipalActionFrom;
import com.itech.bmsresume.actionform.EducationalcollaborationActionForm;
import com.itech.bmsresume.dao.ActivitiesDao;

public class ActivitiesHandler {

	ActivitiesDao dao = new ActivitiesDao();

	/*public Object details(ActivitiesActionForm details, DataSource dataSource,
			String filepath, String filename1, String userid) {
		// TODO Auto-generated method stub
		return dao.details(details,dataSource, filepath,filename1,userid);
	}

	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		// TODO Auto-generated method stub
		return dao.detailsAppend(dataSource,request,response,userid);
	*/

	public Object add(ActivitiesActionForm details, DataSource dataSource,
			String userid, String filename1, String filepath) {
		// TODO Auto-generated method stub
		return dao.add(details,dataSource,userid,filename1, filepath);
	}

	public List<ActivitiesActionForm> list(DataSource dataSource,
			String userid) {
		// TODO Auto-generated method stub
		return dao.list(dataSource,userid);
	}

	
	
	
	public Object changeStatus(ActivitiesActionForm educationalForm, DataSource dataSource) {
		// TODO Auto-generated method stub
		return dao.changeStatus(educationalForm, dataSource);
	}

	public Object update(ActivitiesActionForm educationalForm,
			DataSource dataSource, String heading, String activity,
			String filepath, String filename1) {
		// TODO Auto-generated method stub
		return dao.update(educationalForm, dataSource, heading, activity,filepath,filename1);
	}
}
