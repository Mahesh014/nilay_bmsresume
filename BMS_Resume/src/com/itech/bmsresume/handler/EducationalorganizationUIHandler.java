package com.itech.bmsresume.handler;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import com.itech.bmsresume.actionform.EducationalorganizationUIActionForm;
import com.itech.bmsresume.dao.EducationalorganizationalUIDao;

public class EducationalorganizationUIHandler {

	EducationalorganizationalUIDao dao = new EducationalorganizationalUIDao();
	
	public List<EducationalorganizationUIActionForm> list(DataSource dataSource) throws SQLException {
		// TODO Auto-generated method stub
		return dao.list(dataSource);
	}
}
