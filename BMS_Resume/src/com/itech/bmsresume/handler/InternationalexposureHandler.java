package com.itech.bmsresume.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONArray;

import com.itech.bmsresume.actionform.BookAuthoredActionFrom;
import com.itech.bmsresume.actionform.InternationalexposureActionForm;
import com.itech.bmsresume.dao.InternationalexposureDao;

public class InternationalexposureHandler {
	
	InternationalexposureDao dao = new InternationalexposureDao();

	public Object details(InternationalexposureActionForm details,
			DataSource dataSource, String userid, String str) {
		
		return dao.details(details, dataSource,userid,str);
	}
	

	
	public JSONArray detailsAppend(DataSource dataSource,
			HttpServletRequest request, HttpServletResponse response,
			String userid) {
		
		return dao.detailsAppend(dataSource,request,response,userid);
	}



	public Object update(BookAuthoredActionFrom details, DataSource dataSource,
			String userid, String str) {
		// TODO Auto-generated method stub
		return dao.update(details, dataSource,userid,str);
	}
}
